# Lugmety Oauth Service
### Base Url for staging : http://api.stagingapp.io/auth/v1
### Base Url for live : https://api.lugmety.com/auth/v1
## Notice 
1. Run GET BASE_URL/query/driver/availability?hash=$2y$12$H0Gck5GuZWISN6iUNJffbON.cTrZlHBg6RlpBPh8lhLLeiAonpUgi to insert driver aviavility. 
2. **customer_priority** value `10` means **VIP** , `20` means **new user** and `30` means **normal user**