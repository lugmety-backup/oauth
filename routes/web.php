<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

// change vendor/zizaco/entrust/src/entrust/middleware/EntrustPermission inside handle method to
//if ($this->auth->guest() || !$request->user()->can(explode('|', $permissions))) {
//    return response()->json([
//        'status'=>'403',
//        "message"=>"Permission Denied"],403);
//}
//
//return $next($request);



$app->post("verify/email","UserController@verifyEmail");
$app->post('user/login','AuthController@login');
$app->post("user/confirmation","AuthController@confirmCode");
$app->get("notification/info","UserController@adminAndUserInfo");
$app->post("notification/tokens/{role}", "UserController@getTokensByRole");
$app->post("forgot/password","UserController@forgetPassword");
$app->post("reset/password","AuthController@newPassword");

$app->post("device/token","AuthController@storeNonLoggedInDeviceToken");

$app->get("device/token","AuthController@getNonLoggedInUserToken");

$app->get("/driver/{id:[0-9]+}/info","UserController@getDriverInfo");

$app->post("resend/code","UserController@resendPinCode");

$app->post("send/code/email","UserController@resendPinCodeInEmail");
$app->post("review/users/list",'UserController@getUserBasedForReview');
$app->post("check/captain/driver",'UserController@checkDrivers');//checks user has role drivers and driver-captain

$app->get("user/notification-info/{id}", "UserController@userNotificationInfo");

//Registration of Customer and driver route`
$app->post('user/create/{role}','UserController@register');
//$app->post('user/refresh','AuthController@refreshToken');

$app->get("/machine/user/{userId:[0-9]+}/detail",[
    "uses" => "UserController@getUserDetailForMachineRequest"
]);



$app->group(['middleware' => 'auth'], function () use ($app) {
    // user routes
    $app->group(['user group'], function () use ($app) {
//custom pagination for user 


        $app->get('user/pagination', [
            'middleware' => 'permission:user-view',
            'uses' => 'UserController@indexV2']);

        /*
             * displays logged in user permission list
             * */
        $app->get("user/permission",[
            "uses" => "UserController@displayPermissionList"
        ]);
        /**
         * logout user
         */
        $app->post('user/logout','AuthController@logout');

        $app->get('unassigned/drivers',[
            'middleware' => 'permission:view-driver',
            "uses" => "UserController@getAllUnAssignedDriver"
        ]);

        $app->get("driver/list",[
            'middleware' => 'permission:view-driver',
            "uses" =>   "UserController@listDriverBasedOnRole"
        ]);

        $app->get('user/details',["uses" => "UserController@getUserDetailForPublic"]);

        $app->get('user/{id:[0-9]+}/{role}',[//added to check role of passed user id
            'middleware' => 'permission:user-view',
            'uses' => 'UserController@userRoleCheck'
        ]);

        $app->post("driver/country",["uses" => "UserController@updateCountryIdForDriver"]);

        $app->get('user/details/{id:[0-9]+}',["uses" => "UserController@getUserDetail"]);

        $app->get('user/restaurant/{id:[0-9]+}', [
            'uses' => 'UserController@restaurantUserView']);

        $app->get('user/restaurant/{id:[0-9]+}/branch/{branchId:[0-9]+}', [
            'uses' => 'UserController@restaurantBranchUserView']);

        $app->get('user', ['middleware' => 'permission:user-view',
            'uses' => 'UserController@index']);



        $app->get('user/{id:[0-9]+}', ['middleware' => 'permission:user-view',
            'uses' => 'UserController@show']);

        $app->get('branch/user/{id:[0-9]+}', ['middleware' => 'permission:branch-user-view',
            'uses' => 'UserController@show']);

        $app->get('restaurant/user/{id:[0-9]+}', ['middleware' => 'permission:restaurant-user-view',
            'uses' => 'UserController@show']);

        $app->post('user/', [
            'middleware' => 'permission:user-create',
            'uses' => 'UserController@store']);

        $app->post('user/create', ['middleware' => 'permission:user-create',
            'uses' => 'UserController@storeWithRole']);

        $app->post('user/create/restaurant/customer', ['middleware' => 'permission:user-create',
            'uses' => 'UserController@storeWithRoleRestaurantCustomer']);

        $app->post('restaurant/user/create', ['middleware' => 'permission:restaurant-user-create',
            'uses' => 'UserController@storeWithRole']);//restaurant user create

        $app->post('branch/user/create', ['middleware' => 'permission:branch-user-create',
            'uses' => 'UserController@storeWithRole']);//branch user create

        $app->post('user/delete', ['middleware' => 'permission:user-delete',
            'uses' => 'UserController@destroyUserFromRemoteCall']);

        $app->put('user/{id:[0-9]+}', ['middleware' => 'permission:user-edit',
            'uses' => 'UserController@update']);

        $app->put('restaurant/user/{id:[0-9]+}', ['middleware' => 'permission:restaurant-user-edit',
            'uses' => 'UserController@update']);//restaurant user update

        $app->put('branch/user/{id:[0-9]+}', ['middleware' => 'permission:branch-user-edit',
            'uses' => 'UserController@update']);//branch user update

        $app->put('user/update/{id}',["uses" => "UserController@updateUserDetail"]);

        $app->put('user/password',[
            "uses" => "UserController@changePassword"
        ]);

        $app->delete('user/{id:[0-9]+}', ['middleware' => 'permission:user-delete',
            'uses' => 'UserController@destroy']);

        $app->delete('restaurant/user/{id:[0-9]+}', ['middleware' => 'permission:restaurant-user-delete',
            'uses' => 'UserController@destroy']);//restaurant user delete

        $app->delete('branch/user/{id:[0-9]+}', ['middleware' => 'permission:branch-user-delete',
            'uses' => 'UserController@destroy']);//branch user delete

        $app->put('user/update/{user_id}/meta',['middleware' => 'permission:user-meta-update',
            "uses" => "UserMetaController@update"]);

        $app->get('user/update/{user_id}/meta',['middleware' => 'permission:user-meta-update',
            "uses" => "UserMetaController@index"]);

        $app->put('user/me/update/meta',['middleware' => 'permission:user-meta-update',
            "uses" => "UserMetaController@selfUpdate"]);



    });
//driver availability route
    $app->group(['driver availability group'], function () use ($app) {

        $app->get("driver/availability",[
            "uses" => "DriverAvialibilityController@getDriverAviability"
        ]);

        $app->post("driver/availability",[
            "uses" => "DriverAvialibilityController@storeDriverAvialibility"
        ]);

        $app->get("query/driver/availability",[
            "uses" => "DriverAvialibilityController@queryDriverAviability"
        ]);




    });

    //role routes
    $app->group(['role group'], function () use ($app) {

        $app->get('role', ['middleware' => 'permission:role-view',
            'uses' => 'RoleController@index' ]);

        $app->post('role', ['middleware' => 'permission:role-create',
            'uses' => 'RoleController@store' ]);

        $app->get('role/{id:[0-9]+}', ['middleware' => 'permission:role-view',
            'uses' => 'RoleController@show' ]);

        $app->put('role/{id:[0-9]+}',['middleware' => 'permission:role-edit',
            'uses' => 'RoleController@update' ]);

        $app->get('child/role', [
            'uses' => 'RoleController@getChildRole']);

        $app->delete('role/{id:[0-9]+}',['middleware' => 'permission:role-delete',
            'uses' => 'RoleController@destroy' ]);
    });

    //permission routes
    $app->group(['permission group'], function () use ($app) {

        $app->get('permission', ['middleware' => 'permission:permission-view',
            'uses' => 'PermissionController@index' ]);

        $app->post('permission', ['middleware' => 'permission:permission-create',
            'uses' => 'PermissionController@store' ]);

        $app->get('permission/{id:[0-9]+}', ['middleware' => 'permission:permission-view',
            'uses' => 'PermissionController@show' ]);

        $app->put('permission/{id:[0-9]+}',['middleware' => 'permission:permission-edit',
            'uses' => 'PermissionController@update' ]);

        $app->delete('permission/{id:[0-9]+}',['middleware' => 'permission:permission-delete',
            'uses' => 'PermissionController@destroy' ]);
    });

    //permission role routes
    $app->group(['role permission  group operation based on role'], function () use ($app) {

        $app->get('role/{id:[0-9]+}/permission', ['middleware' => ['permission:role-view|permission-view'],
            'uses' => 'RolePermissionController@index' ]);

        $app->post('role/{id:[0-9]+}/permission', ['middleware' => ['permission:permission-create'],
            'uses' => 'RolePermissionController@store' ]);

        $app->delete('role/{id:[0-9]+}/permission', ['middleware' => ['permission:permission-delete'],
            'uses' => 'RolePermissionController@destroy' ]);
    });

    //user role routes
    $app->group(['user role  group operation based on user'], function () use ($app) {

        $app->get('user/{id:[0-9]+}/role', ['middleware' => ['permission:user-view|role-view'],
            'uses' => 'UserRoleController@index' ]);

        $app->post('user/{id:[0-9]+}/role', ['middleware' => ['permission:role-create'],
            'uses' => 'UserRoleController@store' ]);

        $app->delete('user/{id:[0-9]+}/role', ['middleware' => ['permission:role-delete'],
            'uses' => 'UserRoleController@destroy' ]);
    });

    //permission role route
    $app->group(['permission role route operation based on permission '], function () use ($app) {

        $app->get('permission/{id:[0-9]+}/role', ['middleware' => ['permission:role-view|permission-view'],
            'uses' => 'PermissionRoleController@index' ]);
    });
//$app->post('/oauth/introspect', 'IntrospectionController@introspectToken');
    $app->group( ['oauth_introspection'] , function () use ( $app ) {
        $app->post('/oauth/introspect', 'IntrospectionController@introspectToken');
    });

});


$app->get('export-user', 'ExportController@exportUser'
);
$app->get('export-admin', 'ExportController@exportAdmin'
);

$app->post('export-driver', 'ExportController@exportDriver');

$app->get('test/error/message', 'ExampleController@index');

$app->group(['Report '], function () use ($app) {

    $app->get('user/gender', [
        'middleware' => ['permission:view-age-grp-report'],
        'uses' => 'ReportController@userDemoReportGender']);
    $app->get('user/age-group', [
        'middleware' => ['permission:view-gender-report'],
        'uses' => 'ReportController@userDemoReportAgeGroup']);

    $app->get('user/role-group', [
        'middleware' => ['permission:view-age-grp-report'],
        'uses' => 'ReportController@getUsersCountByRole']);
    $app->get('user-demography/{id:[0-9]+}', [
        // 'middleware' => ['permission:view-age-grp-report'],
        'uses' => 'ReportController@userDemography']);

    $app->get('dashboard-user-count/{id:[0-9]+}', [
        // 'middleware' => ['permission:view-age-grp-report'],
        'uses' => 'ReportController@getCustomerCountForDashboard']);

});