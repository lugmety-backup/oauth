<?php
/**
 * Created by PhpStorm.
 * User: ashish
 * Date: 9/21/2017
 * Time: 10:14 AM
 */

namespace App;


use Illuminate\Database\Eloquent\Model;

class Tokens extends Model
{
        protected $table = "tokens";
        protected $fillable = ["user_id","token","device_type"];

        public function user(){
            return $this->belongsTo('App\User','user_id');
        }
}