<?php
/**
 * Created by PhpStorm.
 * User: ashish
 * Date: 8/17/2017
 * Time: 4:00 PM
 */

namespace App\Events;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Queue\SerializesModels;

class UserEvent
{
    use SerializesModels;

    public $message;
    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct($message)
    {
        $this->message = $message;
    }
}