<?php
namespace App\Events;
class ExampleEvent extends Event
{
    /**
     * Create a new event instance.
     *
     * @return void
     */
    public $array;
    public function __construct($array)
    {
        $this->array = $array;
    }
}