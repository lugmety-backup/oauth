<?php
/**
 * Created by PhpStorm.
 * User: ashish
 * Date: 9/3/2017
 * Time: 10:58 AM
 */

namespace App\Events;


use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Log;

class AdminEvent
{
    use SerializesModels;

    public $message;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct($message)
    {
        $this->message = $message;

    }

}