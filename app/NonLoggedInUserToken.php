<?php
/**
 * Created by PhpStorm.
 * User: Own
 * Date: 5/8/2018
 * Time: 10:36 AM
 */

namespace App;


use Illuminate\Database\Eloquent\Model;

class NonLoggedInUserToken extends Model
{
    protected $table = 'non_logged_tokens';
    protected $fillable = ['user_type','device_type','token','country_id'];

}