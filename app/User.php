<?php

namespace App;
use Carbon\Carbon;
use Illuminate\Support\Facades\Config;
use Laravel\Passport\HasApiTokens;
use Zizaco\Entrust\Traits\EntrustUserTrait;
use Illuminate\Auth\Authenticatable;
use Laravel\Lumen\Auth\Authorizable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;
class User extends Model implements AuthenticatableContract, AuthorizableContract
{

    use HasApiTokens;
    use Authenticatable;
    //use Authorizable;
    use EntrustUserTrait;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable=['first_name',"country_id","type","confirmation_code","ip_address","country_code",'middle_name','last_name','status','gender','email','password','gender','mobile','birthday','facebook_id','google_id','avatar','remember_token','country','city','address_line_1','address_line_2','customer_priority','is_normal'];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [
        'password','pivot','facebook_id','google_id','remember_token'
    ];
    public function roles()
    {
        return $this->belongsToMany('App\Role', Config::get('entrust::assigned_roles_table'));
    }

    public function oauthAccessToken(){
        return $this->hasMany('\App\OauthAccessToken','user_id',"id");
    }

    public function userMetas(){
        return $this->hasMany('App\UserMeta');
    }

    public function setPasswordAttribute($password)
    {
        $this->attributes['password'] = app('hash')->make($password);
    }

    public function setCountryCodeAttribute($code)
    {
        $this->attributes['country_code'] = strtolower($code);
    }


    public function getAvatarAttribute($value)
    {
        if(empty($value)){
            return $value;
        }
        return Config::get("config.image_service_base_url_cdn").$value;
    }

    public function getFirstNameAttribute($value)
    {
        return ucfirst($value);
    }

    public function getMiddleNameAttribute($value)
    {
        return ucfirst($value);
    }

    public function getLastNameAttribute($value)
    {
        return ucfirst($value);
    }

    public function getCountryAttribute($value){
        return ucwords($value);
    }

    public function getCityAttribute($value){
        return ucwords($value);
    }

    public function userMeta()
    {
        return $this->hasMany('App\UserMeta','user_id','id');
    }

    public function restaurantUserMeta(){
        return $this->hasOne('App\RestaurantUserMeta');
    }

    public function userToken(){
        return $this->hasMany('App\Tokens');
    }

    public function getCreatedAtAttribute($value)
    {
        $date = Carbon::parse($value)->tz('Asia/Riyadh')->format("d M Y h:i A");
        return $date;
    }

    public function driverAvailability(){
        return $this->hasOne(DriverAviability::Class,'user_id','id');
    }
}
