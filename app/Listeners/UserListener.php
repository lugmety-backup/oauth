<?php
/**
 * Created by PhpStorm.
 * User: ashish
 * Date: 8/17/2017
 * Time: 4:05 PM
 */

namespace App\Listeners;


use App\Events\UserEvent;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Support\Facades\Log;
use Amqp;
class UserListener implements ShouldQueue
{
    public $event;
    public $tries = 10;
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  ExampleEvent  $event
     * @return void
     */
    public function handle(UserEvent $event)
    {
        $this->event = $event;
        \Log::info( serialize($this->event->message));

        Amqp::publish('service1', serialize($this->event->message) , [
            'queue' => 'notice',
            'exchange_type' => 'direct',
            'exchange' => 'amq.direct',
        ]);
    }

}