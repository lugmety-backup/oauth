<?php
namespace App\Listeners;

use App\Events\ExampleEvent;
use Bschmitt\Amqp\Amqp;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Facades\Log;

class ExampleListener implements ShouldQueue
{
    use InteractsWithQueue;
    /**
     * $tries holds no. of tries queue make before assigning a job as failed job
     * @var int
     */
    public $tries = 10;
    /**
     * Create the event listener.
     *
     * @return void
     */
    public $event;
    public function __construct()
    {
        //
    }
    /**
     * Handle the event.
     *
     * @param  ExampleEvent  $event
     * @return void
     */
    public function handle(ExampleEvent $event)
    {
        $this->event = $event;
        Log::info(serialize($this->event->array));
//        \Amqp::publish('service1', serialize($this->event->array) , [
//            'queue' => 'notice',
//            'exchange_type' => 'direct',
//            'exchange' => 'amq.direct',
//        ]);
    }
    public function failed(\Exception $ex)
    {
    }
}