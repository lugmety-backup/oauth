<?php
/**
 * Created by PhpStorm.
 * User: ashish
 * Date: 8/27/2017
 * Time: 5:06 PM
 */

namespace App;


use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class PasswordReset extends Model
{
    public $timestamps = false;
    public $primaryKey = "token";

    protected $table = "password_resets";
    protected $fillable =["email","token","created_at"];



}