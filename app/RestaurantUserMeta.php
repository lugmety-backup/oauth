<?php
/**
 * Created by PhpStorm.
 * User: ashish
 * Date: 5/22/2017
 * Time: 1:30 PM
 */

namespace App;


use Illuminate\Database\Eloquent\Model;

class RestaurantUserMeta extends Model
{
    protected $table="restaurant_user_meta";
    protected $fillable=['user_id',"restaurant_id","branch_id","is_restaurant_admin"];
    public $timestamps = false;
    public function user(){
        return $this->belongsTo('App\User','user_id');
    }
}