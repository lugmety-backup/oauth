<?php
/**
 * Created by PhpStorm.
 * User: ashish
 * Date: 3/30/2017
 * Time: 1:05 PM
 */

namespace App;

use Illuminate\Support\Facades\Config;
use Zizaco\Entrust\EntrustRole;
use Illuminate\Database\Eloquent\Model;

class Role extends EntrustRole
{
protected $fillable=['name' , 'display_name' , 'description' , 'parent_role' ];
    protected $hidden = [
        'pivot'
    ];
    public function permissions()
    {
        return $this->belongsToMany('App\Permission', Config::get('entrust::permission_role_table'));
    }
}