<?php
/**
 * Created by PhpStorm.
 * User: ashish
 * Date: 5/19/2017
 * Time: 4:00 PM
 */

namespace App;

use Illuminate\Database\Eloquent\Model;


class UserMeta extends Model
{
    protected $table = 'user_meta';
    protected $fillable=['user_id','meta_key','meta_value'];
    public $timestamps = false;
    public function user(){
        return $this->belongsTo('App\User','user_id')->first();
    }

}
