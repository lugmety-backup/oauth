<?php
/**
 * Created by PhpStorm.
 * User: ashish
 * Date: 3/30/2017
 * Time: 1:07 PM
 */

namespace App;

use Zizaco\Entrust\EntrustPermission;
use Illuminate\Database\Eloquent\Model;

class Permission extends EntrustPermission
{
    protected $guarded=['id'];

    protected $hidden = [
        'pivot',"created_at","updated_at"
    ];

}