<?php
/**
 * Created by PhpStorm.
 * User: Own
 * Date: 5/21/2018
 * Time: 1:11 PM
 */

namespace App;


use Illuminate\Database\Eloquent\Model;

class DriverAviability extends Model
{
    /**
     * the table name
     * @var string
     */
    protected $table = 'driver_availability';

    /**
     * the fillable column values
     * @var array
     */
    protected $fillable = ['user_id','availability'];

    public $timestamps = false;

    public function user(){
        return $this->belongsTo('App\User','user_id');
    }

}