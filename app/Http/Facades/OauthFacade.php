<?php
/**
 * Created by PhpStorm.
 * User: ashish
 * Date: 4/25/2017
 * Time: 10:50 PM
 */

namespace App\Http\Facades;


use Illuminate\Support\Facades\Facade;

class OauthFacade extends Facade
{
protected static function getFacadeAccessor()
{
  return "oauth";
}
}