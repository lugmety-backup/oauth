<?php
/**
 * Created by PhpStorm.
 * User: ashish
 * Date: 6/28/2017
 * Time: 4:39 PM
 */

namespace App\Http\Facades;


use Illuminate\Support\Facades\Facade;

class RemoteCallFacade extends Facade
{
    protected static function getFacadeAccessor()
    {
        return "remoteCall";
    }
}