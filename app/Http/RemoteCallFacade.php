<?php
/**
 * Created by PhpStorm.
 * User: ashish
 * Date: 6/28/2017
 * Time: 4:32 PM
 */

namespace App\Http;
use GuzzleHttp\Exception\RequestException;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\ConnectException;
use Illuminate\Support\Facades\Validator;

class RemoteCallFacade
{
    private $guzzle;

    /**
     * RemoteCallFacade constructor.
     * @param Client $guzzle
     */
    public function __construct(Client $guzzle)
    {
        $this->guzzle = $guzzle;
    }
    public function check($url, array $request){
        try{
            $result =  $this->guzzle->post($url,[
                'headers' => [
                    'Accept' => 'application/json'
                ],
                'form_params' => $request
            ]);
            $user=json_decode((string) $result->getBody(), true);
            return ["message"=>$user,"status"=>$result->getStatusCode()];
        }
        catch(ConnectException $ex){
            $res= ["message"=>"Error Connecting to Restaurant  Service","status"=>503];
            return $res;
        }
        catch (RequestException $e)
        {
            $res= ["message"=>json_decode($e->getResponse()->getBody()->getContents(),true),"status"=>$e->getResponse()->getStatusCode()];
            return $res;
        }
    }

    /**
     * @param $url
     * @return array
     */
    public function getSpecificWithoutToken($url){
        try{
            $result = $this->guzzle->get($url,[
                'headers' => [
                    'Accept' => 'application/json',
                ]
            ]);
            $user=json_decode((string) $result->getBody(), true);

            return ["message"=>$user,"status"=>$result->getStatusCode()];
        }
        catch(ConnectException $ex){
            $res= ["message"=>"Error Connecting to Service","status"=>503];
            return $res;
        }
        catch (RequestException $e)
        {
            $res= ["message"=>json_decode($e->getResponse()->getBody()->getContents(),true),"status"=>$e->getResponse()->getStatusCode()];
            return $res;
        }

    }
}