<?php
/**
 * Created by PhpStorm.
 * User: hazesoft_one
 * Date: 12/18/17
 * Time: 2:29 PM
 */

namespace App\Http\Controllers;


use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\DB;
use Mockery\CountValidator\Exception;

class ReportController extends Controller
{
    public function __construct()
    {
    }

    public function userDemoReportGender()
    {
        try {
            $users = DB::table('users')->join("role_user", "users.id", "=", "role_user.user_id")->where('role_user.role_id', 4)
                ->select('gender', DB::raw('count(gender) as count'))
                ->where('gender', "<>", "")
                ->groupBy('gender')
                ->get();
            return $users;
        } catch (\Exception $ex) {
            return response()->json([
                "status" => "400",
                "message" => $ex->getMessage()
            ], 400);
        }
    }

    public function userDemoReportAgeGroup()
    {
        try {
            $users = DB::select(DB::raw("SELECT
    SUM(IF(age < 20,1,0)) as 'Under 20',
    SUM(IF(age BETWEEN 20 and 29,1,0)) as '20 - 29',
    SUM(IF(age BETWEEN 30 and 39,1,0)) as '30 - 39',
    SUM(IF(age BETWEEN 40 and 49,1,0)) as '40 - 49',
    SUM(IF(age BETWEEN 50 and 59,1,0)) as '50 - 59',
    SUM(IF(age BETWEEN 60 and 69,1,0)) as '60 - 69',
    SUM(IF(age BETWEEN 70 and 79,1,0)) as '70 - 79',
    SUM(IF(age >=80, 1, 0)) as 'Over 80'
   
    
    

FROM (SELECT TIMESTAMPDIFF(YEAR, birthday, CURDATE()) AS age FROM users JOIN role_user ON users.id=role_user.user_id WHERE role_user.role_id=4) as derived"));

            return $users;
        } catch (\Exception $ex) {
            return response()->json([
                "status" => "400",
                "message" => $ex->getMessage()
            ], 400);
        }

    }

    public function getUsersCountByRole()
    {
        try {
            $users = DB::table('users')
                ->join("role_user", "users.id", "=", "role_user.user_id")
                ->join("roles", "roles.id", "=", "role_user.role_id")
                ->select(DB::raw('count(*) as count, roles.display_name ,country_id'))
                ->groupBy(['users.country_id', 'roles.name'])
                ->get();

            return response()->json([
                "status" => "200",
                "data" => $users
            ], 200);
        } catch (\Exception $ex) {
            return response()->json([
                "status" => "400",
                "message" => $ex->getMessage()
            ], 400);
        }
    }


    public function userDemography($id,Request $request)
    {
        try {
            $this->validate($request, [
                "start_date" => "required|date_format:Y-m-d",
                "end_date" => "required|date_format:Y-m-d|after:start_date",
                "limit" => "sometimes|integer"


            ]);
        } catch (\Exception $ex) {
            return response()->json([
                "status" => "422",
                "message" => $ex->response->original
            ], 422);
        }

        $startDate = Carbon::parse($request['start_date'])->startOfDay();  //2016-09-29 00:00:00.000000
        //return $startDate;

        $endDate = Carbon::parse($request['end_date'])->endOfDay(); //2016-09-29 23:59:59.000000
        $limit = $request->has('limit')? $request['limit']: 5;


        try {
            $user1 = DB::table('users')->join("role_user", "users.id", "=", "role_user.user_id")->where('role_user.role_id', 4)
                ->select('gender', DB::raw('count(gender) as count'))
                ->whereBetween('users.created_at', [$startDate, $endDate])
                ->where('gender', "<>", "")
                ->where('country_id', $id)
                ->groupBy('gender')
                ->get();

            $user2 = DB::select(DB::raw("SELECT
    SUM(IF(age < 20,1,0)) as 'Under 20',
    SUM(IF(age BETWEEN 20 and 29,1,0)) as '20 - 29',
    SUM(IF(age BETWEEN 30 and 39,1,0)) as '30 - 39',
    SUM(IF(age BETWEEN 40 and 49,1,0)) as '40 - 49',
    SUM(IF(age BETWEEN 50 and 59,1,0)) as '50 - 59',
    SUM(IF(age BETWEEN 60 and 69,1,0)) as '60 - 69',
    SUM(IF(age BETWEEN 70 and 79,1,0)) as '70 - 79',
    SUM(IF(age >=80, 1, 0)) as 'Over 80'
   
FROM (SELECT TIMESTAMPDIFF(YEAR, birthday, CURDATE()) AS age FROM users JOIN role_user ON users.id=role_user.user_id WHERE (users.created_at BETWEEN '$startDate' AND '$endDate') AND  users.country_id='$id') as derived"));

            // $user2 = json_decode(json_encode($user2[0]), True);
            $users['gender'] = $user1;

            $users['age_group'] = $user2;

            $fullUrl = Config::get('config.location_base_url') . "/user/delivery-district/".$id."?start_date=".$startDate."&end_date=".$endDate."&limit=".$limit;
            //return $fullUrl;

            $userData = \RemoteCall::getSpecificWithoutToken($fullUrl);


            if ($userData["status"] != "200") {
                return response()->json([
                    "status" => $userData["status"],
                    "message" => $userData["message"]["message"],
                ], $userData["status"]);
            }

            $users['delivery_districts'] = $userData['message']['data'];

            return response()->json([
                "status" => "200",
                "data" => $users
            ], 200);

        } catch (\Exception $ex) {
            return response()->json([
                "status" => "400",
                "message" => $ex->getMessage()
            ], 400);
        }
    }

    public function getCustomerCountForDashboard($id)
    {
        try {
            $day = Carbon::now();

            $month = Carbon::parse($day)->format('Y-m');

            $yesterday_date = Carbon::parse($day)->subDay(1)->format('Y-m-d');
            $check = DB::table('users')
                ->join("role_user", "users.id", "=", "role_user.user_id")
                ->join("roles", "roles.id", "=", "role_user.role_id")
                ->select(DB::raw('count(*) as count,roles.name as role'))
                ->where([
                    ['country_id', $id],
                    ['users.status', 1]
                ])
                ->where(function ($query) {
                    $query->where('roles.name','customer')
                        ->orWhere('roles.name', 'driver');
                });

            $ld_users = $check->groupBy('roles.name')->pluck('count','role');
            $md_users = $check->where('users.created_at','like',$month.'%')->groupBy('roles.name')->pluck('count','role');
            $yesterday_users = $check->where('users.created_at','like',$yesterday_date.'%')->groupBy('roles.name')->pluck('count','role');
            return response()->json([
                "status" => "200",
                "data" => [
                    'ld_users' => $ld_users,
                    'md_users' => $md_users,
                    'yesterday_users' => $yesterday_users
                ],
            ], 200);
        } catch (\Exception $ex) {
            return response()->json([
                "status" => "400",
                "message" => $ex->getMessage()
            ], 400);
        }
    }


}