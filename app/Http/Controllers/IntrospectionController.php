<?php
/**
 * Created by PhpStorm.
 * User: bijayluitel
 * Date: 4/9/17
 * Time: 3:35 PM
 */

namespace App\Http\Controllers;

use App\DriverAviability;
use App\RestaurantUserMeta;
use App\Tokens;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Laravel\Passport\Bridge\AccessTokenRepository;
use Laravel\Passport\Passport;
use Lcobucci\JWT\Parser;
use Lcobucci\JWT\Token;
use Lcobucci\JWT\ValidationData;
use League\OAuth2\Server\Exception\OAuthServerException;
use League\OAuth2\Server\ResourceServer;
use Mockery\Exception;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psy\Exception\RuntimeException;
use Zend\Diactoros\Response as Psr7Response;
use OauthFacade;

class IntrospectionController
{
    /**
     * @var \Lcobucci\JWT\Parser
     */
    private $jwt;

    /**
     * @var \League\OAuth2\Server\ResourceServer
     */
    private $resourceServer;

    /**
     * @var \Laravel\Passport\Bridge\AccessTokenRepository
     */
    private $accessTokenRepository;

    /**
     * constructing IntrospectionController
     *
     * @param \Lcobucci\JWT\Parser $jwt
     * @param \League\OAuth2\Server\ResourceServer $resourceServer
     * @param \Laravel\Passport\Bridge\AccessTokenRepository $accessTokenRepository
     */
    public function __construct(
        Parser $jwt,
        ResourceServer $resourceServer,
        AccessTokenRepository $accessTokenRepository
    ) {
        $this->jwt = $jwt;
        $this->resourceServer = $resourceServer;
        $this->accessTokenRepository = $accessTokenRepository;
    }

    /**
     * Authorize a client to access the user's account.
     *
     * @param  ServerRequestInterface $request
     *
     * @return JsonResponse|ResponseInterface
     */
    public function introspectToken(ServerRequestInterface $request)
    {
        try {
            $this->resourceServer->validateAuthenticatedRequest($request);
            if (array_get($request->getParsedBody(), 'token_type_hint', 'access_token') !== 'access_token') {
                //  unsupported introspection
                return $this->notActiveResponse();
            }

            $accessToken = array_get($request->getParsedBody(), 'token');
            if ($accessToken === null) {
                return $this->notActiveResponse();
            }
            $token = $this->jwt->parse($accessToken);

            if ( !OauthFacade::verifyToken($token)) {

                return $this->errorResponse([
                    'status'=>'401',
                    'message'=>"Invalid Token"
                ],401);
                //error if given token is invalid
            }

            /**
             * @var string $userModel
             */
            $userModel = config('auth.providers.users.model');
            $user = (new $userModel)->findOrFail($token->getClaim('sub'));
            /*
             * check if the user status is 1 if not display user as unauthorized so that user cannot logged in
             * in case where the user is disabled
             * */
            try{
                if($user->status!==1){
                    throw new \Exception();

                }
            }
            catch(\Exception $ex){
                return response()->json([
                        'status'=> '401',
                        "message"=> "Unauthorized" ]
                    ,401 );
            }

            if ( array_get( $request->getParsedBody(), 'permission', '') !== '' ) {
                $permission = array_get($request->getParsedBody(), 'permission');


                try{
                    if(!$user->can($permission)){
                        throw new Exception();
                    }
                }
                catch (Exception $ex){
                    return response()->json([
                            'status'=> '403',
                            "message"=> "Forbidden" ]
                        ,403 );
                }


            }

            $userMeta=$user->restaurantUserMeta;
            $is_restaurant_admin=$restaurant_id=$branch_id=null;
            if($userMeta != null){
                $is_restaurant_admin=$userMeta->is_restaurant_admin;
                $restaurant_id=$userMeta->restaurant_id;
                $branch_id=$userMeta->branch_id;
            }


            // get user roles
            $roles = array();
            $parentRole=null;
            if( count( $user->roles ) ) {
                foreach ($user->roles as $role) {
                    $roles[] = $role->name;
                    if(!is_null($role->parent_role)) $parentRole=$role->parent_role;
                }
            }
            /**
             * If the user role is restaurant-admin or parent_role is restaurant-admin
             * displays its associated restaurant branches
             * else null
             * @return array|null
             */
            if($roles[0]=="restaurant-admin" || $parentRole == "restaurant-admin"){
                $branchs=RestaurantUserMeta::select("branch_id")->distinct()->where([
                        ["restaurant_id",$restaurant_id],
                        ["branch_id","!=",null]
                    ]
                )->get();
                if($branchs->count()!=0) {
                    foreach ($branchs as $branch) {
                        $branches_id[] = $branch["branch_id"];
                    }
                }
                else $branches_id=$branch_id;
            }
            else $branches_id=$branch_id;

            $is_availables = 1;
            if($user->hasRole('driver') || $user->hasRole('driver-captain') ){
                $is_availables = 0;
                $getDriverAviability = DriverAviability::where('user_id',$user->id)->first();
                if($getDriverAviability){
                    $is_availables = $getDriverAviability->availability;
                }

            }

            $client_id = intval($token->getClaim('aud'));
            /**
             * fetching device_type based on client id
             */
            $clientInfo = DB::table("oauth_clients")->where("id",$client_id)->first();
            $device_type = ($clientInfo)?$clientInfo->name:"";
            return $this->jsonResponse([
                'status'        =>'200',
                'active'        => true,
                'scope'         => trim(implode(' ', (array)$token->getClaim('scopes', []))),
                'client_id'     => $client_id,
                'user_id'       => $user->id,
                'username'      => $user->email,
                "firstname"     => $user->first_name,
                "middlename"    => $user->middle_name,
                "lastname"      => $user->last_name,
                "avatar"        => $user->avatar,
                "device_type" => $device_type,
                'token_type'    => 'access_token',
                'exp'           => intval($token->getClaim('exp')),
                'roles'         => $roles,
                'parentRole' => $parentRole,
                'is_normal' => $user->is_normal,
                'is_restaurant_admin' => $is_restaurant_admin,
                'restaurant_id' => $restaurant_id,
                'branch_id' => $branches_id,
                'customer_priority' => $user->customer_priority,
                'is_available' => $is_availables

//                'iat' => intval($token->getClaim('iat')),
//                'nbf' => intval($token->getClaim('nbf')),
//                'sub' => intval($token->getClaim('sub')),
//                'aud' => intval($token->getClaim('aud')),
//                'jti' => $token->getClaim('jti'),
            ], 200 );

        }
        catch (OAuthServerException $oAuthServerException) {

            return $oAuthServerException->generateHttpResponse(new Psr7Response);
        }
        catch ( ModelNotFoundException $ex){

            return response()->json([
                'status'=>'404',
                'message'=>"Requested User could not be found"
            ],404);

        }
        catch (\Exception $exception) {
            //this error occurs when the  decoder could not decode token
            return response()->json([
                'status'=>'401',
                'message'=>"Invalid Token"
            ],401);
        }
    }

    /**
     * returns inactive token message
     *
     * @return \Illuminate\Http\JsonResponse
     */
    private function notActiveResponse(): JsonResponse
    {
        return $this->jsonResponse([
            'status' => '422',
            'message'=>'token field is required'
        ],422);
    }

    /**
     * @param array|mixed $data
     * @param int $status
     *
     * @return \Illuminate\Http\JsonResponse
     */
    private function jsonResponse($data, $status = 200): JsonResponse
    {
        return new JsonResponse($data, $status);
    }

//    private function verifyToken(Token $token): bool
//    {
//        $signer = new \Lcobucci\JWT\Signer\Rsa\Sha256();
//        $publicKey = 'file://' . Passport::keyPath('oauth-public.key');
//
//        try {
//            if ( ! $token->verify($signer, $publicKey)) {
//                return false;
//            }
//
//            $data = new ValidationData();
//            $data->setCurrentTime(time());
//
//            if ( ! $token->validate($data)) {
//                return false;
//            }
//
//            //  is token revoked?
//            if ($this->accessTokenRepository->isAccessTokenRevoked($token->getClaim('jti'))) {
//                return false;
//            }
//
//            return true;
//        } catch (\Exception $exception) {
//        }
//
//        return false;
//    }

    /**
     * @param array $data
     * @param int $status
     *
     * @return \Illuminate\Http\JsonResponse
     */
    private function errorResponse($data, $status = 400): JsonResponse
    {
        return $this->jsonResponse($data, $status);
    }

    /**
     * returns an error
     *
     * @param \Exception $exception
     * @param int $status
     *
     * @return \Illuminate\Http\JsonResponse
     */
    private function exceptionResponse(\Exception $exception, $status = 500): JsonResponse
    {
        return $this->errorResponse([
            'error' => [
                'id' => str_slug(get_class($exception) . ' ' . $status),
                'status' => $status,
                'title' => $exception->getMessage(),
                'detail' => $exception->getTraceAsString()
            ],
        ], $status);
    }
}