<?php
/**
 * Created by PhpStorm.
 * User: ashish
 * Date: 3/31/2017
 * Time: 4:30 PM
 */

namespace App\Http\Controllers;

use App\Permission;
use App\Role;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Database\QueryException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Mockery\Exception;
use Symfony\Component\HttpKernel\Exception\MethodNotAllowedHttpException;

class RolePermissionController extends Controller
{
    private $permission;
    private $role;

    /**
     * RolePermissionController constructor.
     * @param $permission
     */
    public function __construct(Permission $permission,Role $role)
    {
        $this->role=$role;
        $this->permission = $permission;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($id)
    {
        try {
            $role = $this->role->findOrFail($id);//Select * from role table based on id
            //return $permission;
            $permission = $role->permissions;
            if($permission->count()==0){
                throw new \Exception();
            }
            return response()->json([
                'status' => '200',
                'data' => $permission
            ],200);
        }
        catch (QueryException $ex){
            return response()->json([
                'status'=>'404',
                'message'=>"Permission could not be found"
            ],404);
        }
        catch (ModelNotFoundException $ex){
            return response()->json([
                'status'=>'404',
                'message'=>"Requested Role  could not be found"
            ],404);
        }
        catch (\Exception $ex){
            return response()->json([
                'status'=>'404',
                'message'=>"Requested Permission Roles could not be found"
            ],404);
        }
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request,$id)
    {
        try{
            $role = $this->role->findOrFail($id);//Select * from permission table based on id
            try{
                try{
                    if(count($request['permission'])==0)
                    {
                        throw new Exception();
                    }
                }
                catch (\Exception $ex){
                    return response()->json([
                        'status' => '404',
                        'message' => "Submitted Permission Name could not be found"
                    ], 404);
                }
                for($i=0;$i<count($request['permission']);$i++) {
                    $permission = $this->permission->where('name', strtolower($request['permission'][$i]))->get();
                    if ($permission->count() == 0) {
                        throw new \Exception();
                    }
                    $role->attachPermission($permission[0]);
                }
                return response()->json([
                    'status' => '200',
                    'message' => 'Permissions are successfully assigned to Role'
                ],200);
                /**
                 * since role in return in array so base [0] is used to get object inside index 0
                 **/
            }

            catch (QueryException $ex){
                return response()->json([
                    'status' => '409',
                    'message' => "Permission could not be attached to role due to duplicate entry"
                ], 409);
            }
            catch (ModelNotFoundException $ex) {
                return response()->json([
                    'status' => '404',
                    'message' => "Requested Role  could not be found"
                ], 404);
            }
            catch (\Exception $ex){
                return response()->json([
                    'status' => '404',
                    'message' => "Requested Permission could not be found"
                ], 404);
            }
        }
        catch (ModelNotFoundException $ex) {
            return response()->json([
                'status' => '404',
                'message' => "Requested Role  could not be found"
            ], 404);
        }
        catch (\Exception $ex){
            return response()->json([
                'status'=>'422',
                'message'=>$ex->response->original
            ],422);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id, Request $request)
    {
        DB::beginTransaction();
        try{
            $role = $this->role->findOrFail($id);//Select * from role table based on id
            try{
                try{
                    if(count($request['permission'])==0)
                    {
                        throw new Exception();
                    }
                }
                catch (\Exception $ex){
                    DB::rollBack();
                    return response()->json([
                        'status' => '404',
                        'message' => "Submitted Permission Name could not be found"
                    ], 404);
                }
                foreach($request['permission'] as $permi) {
                    $permission = $this->permission->where('name', strtolower($permi))->get();
                    if ($permission->count() == 0) {
                        throw new \Exception();
                    }
                    $rolesofpermission=$permission[0]->roles;
                    try{
                        if(count($rolesofpermission)==0)
                        {
                            throw new Exception();
                        }
                        $rolecount=0;
                        foreach($rolesofpermission as $rp){
                            if($role->id == $rp->id){
                                $rolecount++;
                            }
                        }
                        if($rolecount==0){
                            throw new Exception();
                        }
                    }
                    catch (\Exception $ex){
                        DB::rollBack();
                        return response()->json([
                            'status' => '404',
                            'message' => "Detached Unsuccessful! Some of permissions have already detached"
                        ], 404);
                    }
                    $role->detachPermission($permission[0]);
                }
                DB::commit();
                return response()->json([
                    'status' => '200',
                    'message' => 'Permissions are successfully detached from Role'
                ],200);
                /**
                 * since role in return in array so base [0] is used to get object inside index 0
                 **/
            }
            catch (\Exception $ex){
                DB::rollBack();
                return response()->json([
                    'status' => '404',
                    'message' => "Submitted Permission Name could not be found"
                ], 404);
            }
        }
        catch (ModelNotFoundException $ex) {
            DB::rollBack();
            return response()->json([
                'status' => '404',
                'message' => "Requested Role  could not be found"
            ], 404);
        }
        catch (\Exception $ex){
            DB::rollBack();
            return response()->json([
                'status'=>'422',
                'message'=>$ex->response->original
            ],422);
        }
    }

}