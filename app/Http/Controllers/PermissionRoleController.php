<?php
/**
 * Created by PhpStorm.
 * User: ashish
 * Date: 4/4/2017
 * Time: 7:51 AM
 */

namespace App\Http\Controllers;

use App\Permission;
use App\Role;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Database\QueryException;
use Illuminate\Http\Request;
use Symfony\Component\HttpKernel\Exception\MethodNotAllowedHttpException;

class PermissionRoleController extends Controller
{
    private $permission;
    private $role;

    /**
     * PermissionRoleController constructor.
     * @param $permission
     */
    public function __construct(Permission $permission,Role $role)
    {
        $this->role=$role;
        $this->permission = $permission;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($id)
    {
        try {
            $permission = $this->permission->findOrFail($id);//Select * from permission table based on id
            $role = $permission->roles;
            if($role->count()==0){
                throw new \Exception();
            }
            return response()->json([
                'status' => '200',
                'data' => $role
            ],200);
        }
        catch (QueryException $ex){
            return response()->json([
                'status'=>'404',
                'message'=>"Role could not be found"
            ],404);
        }
        catch (ModelNotFoundException $ex){
            return response()->json([
                'status'=>'404',
                'message'=>"Requested Permission  could not be found"
            ],404);
        }
        catch (\Exception $ex){
            return response()->json([
                'status'=>'404',
                'message'=>"Requested Permission Roles could not be found"
            ],404);
        }
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request,$id)
    {

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

}