<?php

namespace App\Http\Controllers;
use App\Permission;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Database\QueryException;
use Illuminate\Http\Request;
use Cocur\Slugify\Slugify;
use Symfony\Component\HttpKernel\Exception\HttpException;

class PermissionController extends Controller
{
    private $permission;
    private $slugify;
    /**
     * PermissionController constructor.
     * @param $permission
     */
    public function __construct(Permission $permission,Slugify $slugify)
    {
        $this->permission=$permission;
        $this->slugify=$slugify;
    }

    /**
     * Display all the permissions present in database table named permission
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        try{
            $permissions= $this->permission->all();//Select id, name from permission table
            return \Datatables::of($permissions)->make(true);
//            return response()->json([
//                'status'=>'200',
//                'data'=>$permissions
//            ],200);
        }
        catch (QueryException $e){

            return response()->json([
                'status'=>'404',
            ],404);
        }
        catch (\Exception $e){
            return response()->json([
                'status'=>'404',
            ],404);
        }

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store the permission into database table named permission
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try{
            $this->validate($request, [
                'name' => 'required',

            ]);
            $data['name']=$this->slugify->slugify($request['name'],['regexp' => '/([^A-Za-z]|-)+/']);
            if(empty($data['name'])){
                return response()->json([
                    "status"=>"422",
                    "message"=>["name"=>"The name may not only contain numbers,special characters."]
                ],422);
            }
            $data['display_name']=$request->input('display_name',null);
            $data['description']=$request->input('description',null);
            $permission= $this->permission->create($data);
            return response()->json([
                'status'=>'200',
                'message'=>'Permission created successfully',
            ],200);

        }
        catch (QueryException $ex){
            return response()->json([
                'status'=>'409',
                'message'=>"Permission could not be created due to duplicate entry"
            ],409);
        }
        catch (\Exception $e){
            return response()->json([
                'status'=>'422',
                'message'=>$e->response->original
            ],422);
        }
    }

    /**
     * Display the specified permission
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        try{
            $permission= $this->permission->findOrFail($id);
            return response()->json([
                'status'=>'200',
                'data'=>$permission
            ]);
        }
        catch (QueryException $ex){
            return response()->json([
                'status'=>'404',
                'message'=>"Requested Permission could not be found"
            ],404);
        }
        catch (ModelNotFoundException  $ex){
            return response()->json([
                'status'=>'404',
                'message'=>"Requested Permission could not be found"
            ],404);
        }
        catch (\Exception $ex){
            return response()->json([
                'status'=>'404',
                'message'=>"Requested Permission could not be found"
            ],404);
        }

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified permission in permission table.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        try{
            $this->validate($request, [
                'name' => 'required',

            ]);
            $data['name']=$this->slugify->slugify($request['name'],['regexp' => '/([^A-Za-z]|-)+/']);
            if(empty($data['name'])){
                return response()->json([
                    "status"=>"422",
                    "message"=>["name"=>"The name may not only contain numbers,special characters."]
                ],422);
            }
            $data['display_name']=$request->input('display_name',null);
            $data['description']=$request->input('description',null);
            $permission= $this->permission->findOrFail($id)->fill($data);
            $permission=$permission->update()?$permission:false;

            return response()->json([
                'status'=>'200',
                'message'=>'Permission Updated successfully'
            ]);

        }
        catch (QueryException $ex){
            return response()->json([
                'status'=>'409',
                'message'=>"Permission could not be created due to duplicate entry"
            ],409);
        }

        catch (ModelNotFoundException  $ex){
            return response()->json([
                'status'=>'404',
                'message'=>"Requested Permission could not be found"
            ],404);
        }
        catch (\Exception $e){
            return response()->json([
                'status'=>'422',
                'message'=>$e->response->original
            ],422);
        }
    }

    /**
     * Remove the specified permission from permission table.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try{
            $permission= $this->permission->findOrFail($id)->delete();
            return response()->json([
                'status'=>'200',
                'message'=>"Permission deleted successfully"
            ]);
        }
        catch (QueryException $ex){
            return response()->json([
                'status'=>'404',
                'message'=>"Requested Permission could not be found"
            ],404);
        }
        catch (ModelNotFoundException  $ex){
            return response()->json([
                'status'=>'404',
                'message'=>"Requested Permission could not be found"
            ],404);
        }
        catch (\Exception $ex){
            return response()->json([
                'status'=>'404',
                'message'=>"Requested Permission could not be found"
            ],404);
        }
    }
}
