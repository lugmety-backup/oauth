<?php
/**
 * Created by PhpStorm.
 * User: ashish
 * Date: 3/31/2017
 * Time: 4:30 PM
 */

namespace App\Http\Controllers;

use App\DriverAviability;
use App\Events\AdminEvent;
use App\PasswordReset;
use App\Events\Event;
use Carbon\Carbon;
use Cocur\Slugify\Slugify;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Hash;
use App\Permission;
use App\RestaurantUserMeta;
use App\Role;
use App\User;
use App\UserMeta;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Database\QueryException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Schema;
use Mockery\Exception;
use Redis;
use Illuminate\Support\Facades\URL;
use Image;
use Illuminate\Support\Facades\Mail;
use Validator;
use Illuminate\Support\Facades\Storage;
use ImageUploader;
use Amqp;
use App\Events\UserEvent;
use Settings;
use RemoteCall;

class UserController extends Controller
{
    /**
     * UserController constructor.
     */
    private $user;
    private $slugify;

    public function __construct(User $user, Slugify $slugify)
    {
        $this->user = $user;
        $this->slugify = $slugify;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    private function getAllUsersWithParam($type, $status)
    {
        switch ($status) {
            case null:
                {
                    if (!is_null($type)) {
                        return User::whereHas("roles", function ($query) use ($type) {
                            $query->where("name", $type);
                        })->get();
                    } else {
                        return User::all();

                    }
                    break;
                }
            case !null:
                {
                    if (!is_null($type)) {

                        $users = User::where("status", $status)->whereHas("roles", function ($query) use ($type) {
                            $query->where("name", $type);
                        })->get();
                        return $users;
                    } else {
                        return User::where("status", $status)->get();

                    }
                    break;
                }
        }
    }

    /**
     * gets user info for admin
     * @param $id
     */
    public function getDriverInfo($id){
        try{
            $userDetail = User::findOrFail($id);
            $userDetail['role'] = $userDetail->roles->first()->name;
            return response()->json([
                'status' => '200',
                'data' => $userDetail
            ]);
        }
        catch (ModelNotFoundException $ex){
            return response()->json([
                'status' => '404',
                'data' => 'User could not be found.'
            ],404);
        }
        catch (\Exception $ex){
            return response()->json([
                'status' => '500',
                'data' => 'Error getting user info.'
            ],500);
        }
    }

    /**
     * get the list of user , restaurant,branch,admin and super-admin details
     * @param $userId
     * @param $restaurantId
     * @return \Illuminate\Http\JsonResponse
     */
    public function adminAndUserInfo(Request $request)
    {

        $userId = $request->get("user_id");
        $restaurantId = $request->get("restaurant_id");
        $driverId = $request->get("driver_id");
        $parentId = $request->get("parent_id");
        $countryId = $request->get('country_id', null);
        $customer = [];
        $driverList = [];
        if (!is_null($countryId)) {
            $driverUserDriverOnly = Role::with('users')->where('name', "driver")->get();
            $driverUsercaptain = Role::with('users')->where('name', "driver-captain")->get();

            if (count($driverUserDriverOnly) != 0 && count($driverUsercaptain) != 0){
                $driverUser = collect($driverUserDriverOnly[0]["users"])->merge(collect($driverUsercaptain[0]['users']))->all();
            }

            else if(count($driverUserDriverOnly) == 0 && count($driverUsercaptain) != 0){
                $driverUser = $driverUsercaptain[0]['users'];
            }
            elseif (count($driverUsercaptain) == 0 && count($driverUserDriverOnly) != 0){
                $driverUser = $driverUserDriverOnly[0]["users"];
            }
            else{
                goto getUsers;
            }
//            return $driverUser;
            if (count($driverUser) == 0) goto getUsers;
            $drivers = $driverUser;
            $drivers = collect($drivers)->where("status", 1)->where("country_id", $countryId)->all();
            $driversId = collect($drivers)->pluck('id')->values()->all();//gets drivers id
            $getAvialableDriversOnly = DriverAviability::whereIn('user_id',$driversId)->where('availability',1)->pluck('user_id')->values()->all();//compare with the availability table to only get records of driver who is avialable
            $drivers = collect($drivers)->whereIn('id',$getAvialableDriversOnly)->values()->all();
            foreach ($drivers as $driver) {
                $tokens = $driver->userToken()->pluck("token");
                if (count($tokens) == 0) continue;
                foreach ($tokens as $token) {
                    $driverList[] = $token;
                }

            }


        }

        if (is_null($userId)) goto mainRestaurantAdmin;
        getUsers:
        try {
            /**
             * get customer details
             */
            $customer = User::where("status", 1)->findOrFail($userId);
            if (!($customer->hasRole("customer") || $customer->hasRole("restaurant-customer"))) throw new Exception();
            $username = implode(" ", array($customer['first_name'], $customer['middle_name'], $customer['last_name']));
            $customer->makeHidden(["first_name", "middle_name", "last_name"]);
            $customer["name"] = $username;
            $customer["tokens"] = $customer->userToken()->pluck("token");
        } catch (\Exception $ex) {
            $customer = [];
        }

        mainRestaurantAdmin:
        $restaurantAdmin = [];

        if (is_null($parentId)) goto restaurantUser;
        $restaurantUsers = RestaurantUserMeta::select("user_id")->where([
            ["restaurant_id", $parentId]
        ])->get();
        try {
            if ($restaurantUsers->count() == 0) {
                throw new \Exception();
            }
        } catch (\Exception $ex) {
            $restaurantAdmin = [];
        }
        foreach ($restaurantUsers as $restaurantUser) {
            $restUser = $restaurantUser->user()->select("id", "email", "mobile")->where("status", 1)->get();
            if ($restUser->count() != 0) {
                $userRole = $restUser[0]->roles()->first();
                if (!$userRole) continue;
                if (($userRole->name == "restaurant-admin")) {
                    $restUser[0]['role'] = $userRole->name;
                    $restaurantAdmin = $restUser[0];
                }
            }
        }

        /*
         * getting restaurant branch users
         */

        restaurantUser:
        $branchOperator = [];
        $users = [];
        if (is_null($restaurantId)) goto driverUser;
        $restaurantUsers = RestaurantUserMeta::select("user_id")->where([
            ["branch_id", $restaurantId]
        ])->get();
        try {
            if ($restaurantUsers->count() == 0) {
                throw new \Exception();
            }
        } catch (\Exception $ex) {
            $users = [];
        }

        foreach ($restaurantUsers as $restaurantUser) {
            $restUser = $restaurantUser->user()->select("id", "email", "mobile")->where("status", 1)->get();
            if ($restUser->count() != 0) {
                $userRole = $restUser[0]->roles()->first();
                if (!$userRole) continue;
                if (($userRole->name == "branch-admin")) {
                    $restUser[0]['role'] = $userRole->name;
                    $users = $restUser[0];
                }
                if (($userRole->name == "branch-operator")) {
                    $restUser[0]['role'] = $userRole->name;
                    $operator = $restUser[0];
                    $operator['tokens'] = $restUser[0]->userToken()->pluck("token");
                    $branchOperator[] = $operator;

                }
            }
        }
        /**
         * get driver detail based on driver id
         */

        driverUser:

        $driverUserToSent = [];
        if (is_null($driverId)) goto adminUsers;

        try {
            $driverUser = User::where("status", 1)->findOrFail($driverId);
            $getDriverAviability = DriverAviability::where('user_id',$driverId)->where('availability',1)->first();
            if(!$getDriverAviability){
                throw new \Exception();
            }
            if (!($driverUser->hasRole("driver") || $driverUser->hasRole("driver-captain"))) throw new \Exception();
            $username = implode(" ", array($driverUser['first_name'], $driverUser['middle_name'], $driverUser['last_name']));
            $driverUserToSent["id"] = $driverUser["id"];
            $driverUserToSent["email"] = $driverUser["email"];
            $driverUserToSent["name"] = $username;
            $driverUserToSent["mobile"] = $driverUser["mobile"];
            $driverUserToSent["tokens"] = $driverUser->userToken()->pluck("token");
        } catch (\Exception $ex) {
            $driverUserToSent = [];
        }


        /*
         * getting user with role admin and super-admin
         */

        adminUsers:

        $adminUser = $admin = Role::with('users')->where('name', "admin")->get();
        $superAdminUser = $superAdmin = Role::with('users')->where('name',"super-admin")->get();
        if(count($admin) != 0) $adminUser = $admin[0]->users;
        if(count($superAdmin) != 0) $superAdminUser =  $superAdmin[0]->users;


        $combileUsers = collect($adminUser->merge($superAdminUser))->where('status',1)->all();


        $admins = null;
        foreach ($combileUsers as $admin) {
            $admins[] = ["email" => $admin["email"], "mobile" => $admin["mobile"]];
        }

        $userList = [
            "customer" => $customer,
            "restaurant_admin" => $restaurantAdmin,
            "branch_admin" => $users,
            "driver" => $driverUserToSent,
            "admin" => $admins,
            "driver_list" => $driverList,
            "branch_operator" => $branchOperator,
        ];
        if (is_null($userList)) {
            return response()->json([
                "status" => "404",
                "message" => "Users could not be found for requested restaurant id"
            ], 404);
        }
        return response()->json([
            "status" => "200",
            "data" => $userList
        ], 200);
    }


    /**
     * Returns  restaurant users with status 1 only or  all associated restaurant user with status 1 i.e restaurant user
     * plus its branch users if restaurant_all url param is 1
     *
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function restaurantUserView($id)
    {
        $type = $types['restaurant_all'] = Input::get("restaurant_all");
        $userId = Auth::id();
        if (!is_null($type)) {
            $validator = Validator::make($types, [
                "restaurant_all" => "integer|min:0|max:1"
            ]);
            if ($validator->fails()) {
                return response()->json([
                    'status' => '422',
                    "message" => $validator->errors()
                ], 422);
            }
        }
        if ($type == 0 || is_null($type)) {
            $restaurantUsers = RestaurantUserMeta::select("user_id")->where([
                ["restaurant_id", $id],
                ["branch_id", null]
            ])->get();
        } else {
            $restaurantUsers = RestaurantUserMeta::select("user_id")->where([
                ["restaurant_id", $id]
            ])->get();
        }
        $users = [];

        foreach ($restaurantUsers as $restaurantUser) {
            $restUser = $restaurantUser->user()->where("id", "!=", $userId)->get();
            if ($restUser->count() != 0) {
                $userRole = $restUser[0]->roles()->first();
                if (count($userRole) == 0) continue;
                $restUser[0]['role'] = $userRole->name;
                $users[] = $restUser[0];
            }
        }
        return response()->json([
            "status" => "200",
            "data" => $users
        ], 200);
    }

    /**
     * checks if user has role based on @param $role
     * @param $id
     * @param $role
     * @return \Illuminate\Http\JsonResponse
     */
    public function userRoleCheck($id, $role)
    {
        try {
            $user = User::where([
                ["status", 1],
                ["id", $id]
            ])->firstOrFail();
        } catch (ModelNotFoundException $ex) {
            return response()->json([
                "status" => "404",
                "message" => "User could not be found. It may be due to user is inactive or user is not registered"
            ], 404);
        }
        if($role == 'driver'){
            if (!($user->hasRole($role) || $user->hasRole('driver-captain')))  {
                return response()->json([
                    "status" => "404",
                    "message" => "Role $role or driver captain is not assigned to given user"
                ], 404);
            }
            return response()->json([
                "status" => "200",
                "data" => true,
                "country_id" => $user->country_id
            ]);
        }
        elseif (!$user->hasRole($role)) {
            return response()->json([
                "status" => "404",
                "message" => "Role $role is not assigned to given user"
            ], 404);
        } else {
            return response()->json([
                "status" => "200",
                "data" => true,
                "country_id" => $user->country_id
            ]);
        }
    }

    /**
     * verify email of passed email
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function verifyEmail(Request $request)
    {
        try {
            $message = [
                "domain_validation" => "The mail server for domain does not exist."
            ];
            $this->validate($request, [
                'email' => 'required|email|domain_validation|unique:users|max:255',
            ], $message);
        } catch (\Exception $ex) {
            return response()->json([
                "status" => "422",
                "message" => $ex->response->original
            ], 422);
        }
        return response()->json([
            "status" => "200",
            "message" => "This email address can be used"
        ], 200);

    }

    /**
     * gets all unassigned drivers or driver captain
     * @return \Illuminate\Http\JsonResponse
     */
    public function getAllUnAssignedDriver(){
        try{
            $orderFullUrl = Config::get("config.order_base_url")."/assigned/drivers";
            $getAssignedDriverList = \RemoteCall::getSpecificWithoutToken($orderFullUrl);
            if($getAssignedDriverList["status"] != "200"){
                if($getAssignedDriverList["status"] == "503"){
                    return response()->json([
                        "status" => "503",
                        "message" => $getAssignedDriverList["message"]
                    ],503);
                }
                else{
                    return response()->json($getAssignedDriverList["message"],$getAssignedDriverList["status"]);
                }
            }
            $data = $getAssignedDriverList['message']['data'];
            $drivers =  User::whereNotIn("users.id", $data)->join('driver_availability','users.id','=','driver_availability.user_id')->where('driver_availability.availability',1)->where('status',1)->select('users.id','first_name','middle_name','last_name','email')->orderBy('first_name','desc')->whereHas("roles", function ($query)  {
                $query->where("name", 'driver')
                    ->orWhere('name','driver-captain');
            })->get();

            return response()->json([
                'status' => '200',
                'data' => $drivers
            ]);
        }
        catch (\Exception $ex){
            Log::error('error listing unassigned drivers',[
                'message' => $ex->getMessage()
            ]);
            return response()->json([
                'status' => '500',
                'message' => 'Error Listing unassigned drivers '
            ]);
        }
    }

    /*
    * get list of users based on passed array of user id
    */
    public function getUserBasedForReview(Request $request)
    {
        $userDetails = [];
        try {
            $userDetails = User::select("id", "avatar", "first_name", "last_name", "middle_name", "email", "mobile", "country_code")->find($request->users);
            if ($request->has("error") && $request->error == "true") {
                if (count($userDetails) == 0) {
                    return response()->json([
                        "status" => "404",
                        "message" => "User(s) could not be found"
                    ], 404);
                }
            }

            return response()->json([
                "status" => "200",
                "data" => $userDetails
            ]);
        }
        catch (\Exception  $ex){
            Log::error($ex->getMessage());
            return response()->json([
                "status" => "500",
                "data" => $ex->getMessage()
            ]);
        }

    }

    /**
     * checks if the passed users have role driver-captain and driver
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function checkDrivers(Request $request)
    {
        try {
            $this->validate($request, [
                "captain_id" => "required|integer|min:0",
                "drivers_id" => "sometimes|array",
                "drivers_id.*" => "present|integer|min:1"
            ]);
        } catch (\Exception $ex) {
            return response()->json([
                "status" => "422",
                "message" => $ex->response->original
            ], 422);
        }
        try {
            if ($request->has("captain_id") && ($request->captain_id != 0)) {
                $getCaptainInfo = User::where("id", $request->captain_id)->whereHas("roles", function ($query) {
                    $query->where("name", "driver-captain");
                })->first();
                if(!$getCaptainInfo){
                    return response()->json([
                        "status" => "403",
                        "message" => "Requested captain_id does not have role of captain driver."
                    ],403);
                }
            }
            if(count($request["drivers_id"]) != 0){
                $getAllDriversInfo = User::whereIn("id", $request->drivers_id)->whereHas("roles", function ($query) {
                    $query->where("name", "driver");
                })->get();
                Log::info("check drivers",[
                    "request" => $request->drivers_id,
                    "drivers" => $getAllDriversInfo->toArray()
                ]);
                if(count($getAllDriversInfo) != count($request->drivers_id)){
                    return response()->json([
                        "status" => "403",
                        "message" => "One or more user(s) does not have role driver."
                    ],403);
                }
            }

            return response()->json([
                "status" => "200",
                "message" => "Successfully verified user(s)"
            ],200);
        }
        catch (\Exception $ex){

        }
    }

    /**
     * get the list of unassigned or assigned drivers and/or driver-captain
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function listDriverBasedOnRole(Request $request){
        try{
            $message = [
                "status.in" => "The status field value must be either assigned or unassigned.",
                "role.in" => "The role field value must be either driver or driver-captain."
            ];
            $this->validate($request,[
                "status" => "required|in:assigned,unassigned",
                "role" => "required|in:driver,driver-captain"

            ],$message);
        }
        catch (\Exception $ex){
            return response()->json([
                "status" => "422",
                "message" => $ex->response->original
            ],422);
        }
        try{
            $securedPassword = app('hash')->make("Alice123$");
            $orderFullUrl = Config::get("config.order_base_url")."/assigned/team/members?code=$securedPassword";
            $getTeamDeatails = \RemoteCall::getSpecificWithoutToken($orderFullUrl);
            if($getTeamDeatails["status"] != "200"){
                if($getTeamDeatails["status"] == "503"){
                    return response()->json([
                        "status" => "503",
                        "message" => $getTeamDeatails["message"]
                    ],503);
                }
                else{
                    return response()->json($getTeamDeatails["message"],$getTeamDeatails["status"]);
                }
            }
            if($request->status == "assigned"){
                if($request->role == "driver"){
                    $users =  User::whereIn("id", $getTeamDeatails["message"]["data"]["members"])->whereHas("roles", function ($query)  {
                        $query->where("name", "driver");
                    })->get();
                }
                else{
                    $users =  User::whereIn("id", $getTeamDeatails["message"]["data"]["captains"])->whereHas("roles", function ($query)  {
                        $query->where("name", "driver-captain");
                    })->get();
                }

            }
            else{
                if($request->role == "driver"){
                    $users =  User::whereNotIn("id", $getTeamDeatails["message"]["data"]["members"])->whereHas("roles", function ($query)  {
                        $query->where("name", "driver");
                    })->get();
                }
                else{
                    $users =  User::whereNotIn("id", $getTeamDeatails["message"]["data"]["captains"])->whereHas("roles", function ($query)  {
                        $query->where("name", "driver-captain");
                    })->get();
                }
            }
            return response()->json([
                "data" => $users,
                "status" => "200"
            ]);
        }
        catch (\Exception $ex){
            Log::error("error listing drivers",[
                "request" => $request->all(),
                "message" => $ex->getMessage()
            ]);
            return response()->json([
                "status" => "500",
                "message" => "Error listing drivers "
            ],500);
        }
    }

    /**
     * Returns requested restaurant branch users only whose user status is 1
     * @param $id
     * @param $branchId
     * @return \Illuminate\Http\JsonResponse
     */
    public function restaurantBranchUserView($id, $branchId)
    {
        $userId = Auth::id();
        $restaurantUsers = RestaurantUserMeta::select("user_id")->where([
            ["is_restaurant_admin", 0],
            ["restaurant_id", $id],
            ["branch_id", $branchId]
        ])->get();
        $users = [];
        foreach ($restaurantUsers as $restaurantUser) {
            $restUser = $restaurantUser->user()->where("id","!=",$userId)->get();
            if ($restUser->count() != 0) {
                $userRole = $restUser[0]->roles()->first();
                if (count($userRole) == 0) continue;
                $restUser[0]['role'] = $userRole->name;
                $users[] = $restUser[0];
            }
        }
        return response()->json([
            "status" => "200",
            "data" => $users
        ], 200);
    }

    public function resendPinCodeInEmail(Request $request){
        try{
            $this->validate($request,[
                "email" => "required|email"
            ]);
        }
        catch (\Exception $ex){
            return response()->json([
                "status" => "422",
                "message" => $ex->getMessage()
            ]);
        }
        try {
            DB::beginTransaction();
            $getUserDetail = User::where("email", $request->email)->firstOrFail();
            if($getUserDetail->status == 1) {
                return response()->json([
                    "status" => "403",
                    "message" => "User is already activated"
                ],403);
            }
            /**
             * calling an event to passs the code
             */

            if(empty($getUserDetail->confirmation_code)){
                $code = mt_rand(1000, 9999);
                $getUserDetail->update(["confirmation_code" => $code]);

            }
            else{
                $code = $getUserDetail->confirmation_code;
            }
            $userData = $getUserDetail->toArray();

            $array = [
                'service' => 'oauth service',
                'message' => 'sendcodeinemail',
                'data' => [
                    "user_details" => $userData,
                    "confirmation_url" => $code
                ]
            ];
            event(new UserEvent($array));
            DB::commit();
            return response()->json(
                [
                    "status" => "200",
                    "message" => "Please check your email to get the code."
                ]
            );
        }
        catch (ModelNotFoundException $ex){
            return response()->json([
                "status" => "404",
                "message" => "User could not be found"
            ],404);
        }
        catch (\Exception $ex){
            return response()->json([
                "status" => "500",
                "message" => "Error sending code"
            ],500);
        }


    }

    public function resendPinCode(Request $request)
    {
        try {
            $this->validate($request, [
                "mobile" => "required"
            ]);
        } catch (\Exception $ex) {
            return response()->json([
                "status" => "422",
                "message" => $ex->response->original
            ], 422);
        }
        $lang = Input::get('lang', 'en');
        try {
            DB::beginTransaction();
            $getUserData = User::where([
                ["status", 0],
                ["mobile", $request->mobile],
                ["confirmation_code","!=", null]
            ])->firstOrFail();
            if(!$getUserData->hasRole("customer")){
                return \Settings::getErrorMessageOrDefaultMessage('oauth-you-have-to-be-customer-in-order-to-resend-confirmation-code',
                    "You have to be customer in order to resend confirmation code.", 403, $lang);
//                return response()->json([
//                    "status" => "403",
//                    "message" => "You have to be customer in order to resend confirmation code."
//                ],403);
            }
            $confirmationCode = mt_rand(1000, 9999);
            $getUserData->update(["confirmation_code" => $confirmationCode]);


            $userData= $getUserData->toArray();
            $array = [
                'service' => 'oauth service',
                'message' => 'customer create',
                'data' => [
                    "user_details" => $userData,
                    "confirmation_url" => $confirmationCode
                ]
            ];
            event(new UserEvent($array));
            DB::commit();
            return \Settings::getErrorMessageOrDefaultMessage('oauth-please-check-your-mobile-inbox-for-verification',
                "Please check your mobile inbox for verification.", 200, $lang);
//            return response()->json([
//                'status' => '200',
//                'message' => 'Please check your mobile inbox for verification.'
//            ], 200);


        }
        catch (ModelNotFoundException $ex) {
            return \Settings::getErrorMessageOrDefaultMessage('oauth-requested-mobile-user-has-been-activated-or-not-exist',
                "Requested mobile user has been already activated or does not exist.", 404, $lang);
//            return response()->json([
//                "status" => "404",
//                "message" => "Requested mobile user has been already activated or does not exist."
//            ], 404);
        }
        catch (\Exception $ex) {
            return response()->json([
                "status" => "500",
                "message" => $ex->getMessage()
            ], 500);
        }
    }


    public function index(Request $request)
    {
        $type =  $request->get("role");
        $exclude = $request->get('exclude');
        $status = $request->get("status");
        $userForResponse = [];

        try {
            try{
                $this->validate($request,[
                    "status" => "sometimes|integer|min:0|max:1"
                ]);
            }
            catch (\Exception $ex){
                return response()->json([
                    "status" => "422",
                    "message" => $ex->response->original
                ],422);
            }
//            if (!is_null($type)) {
//                $user = $this->getAllUsersWithParam($type, $status);
//                if(isset($user[0])) $userForResponse = $user[0]['users'];
//                else $userForResponse = $user;
//
//            }
//            else{
//                $user = $this->getAllUsersWithParam($type, $status);
//                $userForResponse = $user;
//            }
            $user = $this->getAllUsersWithParam($type, $status);
            $userForResponse = $user;
            foreach ($userForResponse as $user){
                $user->roles;
            }
            $userForResponse = collect($userForResponse)->sortByDesc("id")->values()->all();
            return \Datatables::of($userForResponse)->make(true);

        } catch (QueryException $ex) {
            return response()->json([
                'status' => '404',
                'message' => "Users could not be found"
            ], 404);
        } catch (ModelNotFoundException  $ex) {
            return response()->json([
                'status' => '404',
                'message' => "Users could not be found"
            ], 404);
        } catch (\Exception $ex) {
            return response()->json([
                'status' => '404',
                'message' => "Users could not be found"
            ], 404);
        }
    }

    public function indexV2(Request $request)
    {
        $lang = "en";
        $sortBy['sort_by'] = $request->get("sort_by", "desc");
        $limit = $request->get("limit",10);

        $type = $request->get("role");
        $exclude = $request->get('exclude');
        $status = $request->get("status");
        $userForResponse = [];

        try {
            $this->validate($request, [
                "search" => "sometimes|string",
                "sort_column" => "sometimes",
                "sort_by" => "sometimes",
                "status" => "sometimes|integer|min:0|max:1",
                'role' => 'sometimes|string',
                'email' => 'sometimes|string',
                'country' => 'sometimes|integer|min:1',
                'customer_type' => 'sometimes|in:10,30',
                "start_date" => "sometimes|date_format:Y-m-d",
                "end_date" => "sometimes|date_format:Y-m-d",
                "availability" => "sometimes|integer|min:0|max:1",
            ]);
        } catch (\Exception $ex) {
            return response()->json([
                "status" => "422",
                "message" => $ex->response->original
            ], 422);
        }
        try {
            $appends = [
                "sort_by" => $sortBy['sort_by'],
                "limit" => $limit,
                "sort_column" => ($request->has("sort_column")) ? $request->sort_column : null
            ];
            $filter = [
                "search" => ($request->has("search")) ? $request->search : null,
                "role" => ($request->has("role")) ? $request->role : null,
                'country' => ($request->has("country")) ? $request->country : null,
                'customer_type' => ($request->has("customer_type")) ? $request->customer_type : null,
                'email' => ($request->has("email")) ? $request->email : null,
                'start_date' => ($request->has("start_date")) ? $request->start_date : null,
                'end_date' =>  ($request->has("end_date")) ? $request->end_date : null,
                'status' =>  ($request->has("status")) ? $request->status : null,
                'availability' => ($request->has("availability")) ? $request->availability : null,


            ];
            $appends = array_merge($appends, $filter);



//gets all column fields
            $columnsList = Schema::getColumnListing("users");
            $order = "id";
            foreach ($columnsList as $columnName) {
                if (isset($appends["sort_column"])) {
                    if ($columnName == $appends["sort_column"]) {
                        $order = $columnName;
                        break;
                    }
                }
            }

//            DB::enableQueryLog();
            $users = $this->user;

            /**
             * filters
             */

            //filter by date
            if(is_null($appends['start_date']) && is_null($appends['end_date'])){
                $firstDayOfMonth = Carbon::now()->startOfMonth()->toDateTimeString();
                $users = $users->where("created_at",">", $firstDayOfMonth);

            }else if(!is_null($filter['start_date']) && !is_null($filter['end_date'])) {
                $filter['start_date'] = Carbon::createFromFormat('Y-m-d', $filter['start_date'])->startOfDay()->toDateTimeString();
                $filter['end_date'] = Carbon::createFromFormat('Y-m-d', $filter['end_date'])->addDay()->startOfDay()->toDateTimeString();
                $firstDayOfMonth = Carbon::now()->startOfMonth();
                $users = $users->where("created_at", ">", $filter['start_date'])
                    ->where("created_at", "<", $filter['end_date']);
            }elseif(!is_null($filter['start_date'])){
                $filter['start_date'] = Carbon::createFromFormat('Y-m-d', $filter['start_date'])->startOfDay()->toDateTimeString();
                $users = $users->where("created_at",">", $filter['start_date']);
            }elseif(!is_null($filter['end_date'])){
                $filter['end_date'] = Carbon::createFromFormat('Y-m-d', $filter['end_date'])->addDay()->startOfDay()->toDateTimeString();
                $users = $users->where("created_at","<", $filter['end_date']);
            }
//filter by role
            if (!is_null($appends["role"])) {
                $users = $users->whereHas("roles", function ($query) use ($appends) {
                    $query->where("name", $appends["role"]);

                });
                //filter by driver availability
                if(($appends["role"] ==='driver' || $appends["role"] ==='driver-captain') &&  (!is_null($appends["availability"]))){
                    $users = $users->whereHas("driverAvailability", function ($query) use ($appends) {
                        $query->where('availability',$appends['availability']);

                    });
                }

            }
//filter by email
            if (!is_null($appends["email"])) {

                $users = $users->where('email','like','%'.$appends["email"]. '%');

            }
//filter by customer type
            if (!is_null($appends["customer_type"])) {

                $users = $users->where('customer_priority',$appends["customer_type"]);

            }
//filter by country
            if (!is_null($appends["country"])) {

                $users = $users->where('country_id',$appends["country"]);

            }
//filter by status
            if (!is_null($appends["status"])) {

                $users = $users->where('status',$appends["status"]);

            }


            //search
            if (!is_null($appends["search"])) {
                $value = "%" . strtolower($appends["search"]) . "%";
                $slugedValue = "%" . $this->slugify->slugify($value) . "%";
                $users = $users->where(function ($query) use ($columnsList, $value) {
                    foreach ($columnsList as $key => $column) {
                        if($key === 0){
                            $query->where($column, "like", $value);
                        }
                        $query->orWhere($column, "like", $value);
                    }
                });
                $users = $users->WhereHas("roles", function ($query) use ($value,$slugedValue) {
                    $query->orWhere("name",'like', $value)
                        ->orWhere('name','like',$slugedValue)
                        ->orWhere('display_name','like',$slugedValue)
                        ->orWhere('display_name','like',$value);
                });

            }




            $users = $users->with('roles');


            $users = $users->orderBy($order, $sortBy['sort_by']);
//            $users = $users->get();
//            dd(DB::getQueryLog());
            $users = $users->paginate($limit);
            $users = $users->withPath('/user/pagination')->appends($appends);
            if($request->has('role') && ($request->role === 'driver' || $request->role === 'driver-captain' )){
                foreach ($users as $user){
                    $user["availability"] = $user->driverAvailability()->first()->availability;
                }
            }
            return $users;


        } catch (QueryException $ex) {
            return response()->json([
                'status' => '404',
                'message' => "Users could not be found"
            ], 404);
        } catch (ModelNotFoundException  $ex) {
            return response()->json([
                'status' => '404',
                'message' => "Users could not be found"
            ], 404);
        } catch (\Exception $ex) {
            Log::error('error listing user records',[
                'request' => $request->all(),
                'message' => $ex->getMessage()
            ]);
            return response()->json([
                'status' => '404',
                'message' => "error getting records"
            ], 500);
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        DB::beginTransaction();
        try {

            $message = [
                'customer_priority.in' => 'The customer_priority field must have value 10 for VIP and 30 for normal user.'
            ];
            $request['status'] = Input::get("status", 1);
            $rules = [
                'first_name' => 'required|max:50',
                'last_name' => 'required|max:50',
                'email' => 'required|email|unique:users|max:255',
                'password' => 'required|max:255',
                'gender' => 'required|in:male,female',
                'mobile' => 'required|digits_between:9,20',
                'status' => 'integer|min:0|max:1',
                "birthday" => "date_format:Y-m-d",
                'country' => 'sometimes|max:255',
                'city' => 'sometimes|max:255',
//                'customer_priority' => 'required|in:10,30',
//                "country_code" => "sometimes|",
                'address_line_1' => 'sometimes|max:255',
                'address_line_2' => 'sometimes|max:255',
                "meta" => "sometimes|array",
                'role_id' => 'required|integer|min:1',
            ];
            $this->validate($request, $rules);
            if($request->role_id == 4){
                $this->validate($request, [
                    'customer_priority' => 'required|in:10,30'
                ],$message);
            }
            $req = $request->all();
            if ($request->has('avatar')) {
                $image['avatar'] = $request["avatar"];
                $image["type"] = "avatar";
                $imageUpload = ImageUploader::upload($image);
                if ($imageUpload["status"] != 200) {
                    return response()->json(
                        $imageUpload["message"]
                        , $imageUpload["status"]);
                }
                $req['avatar'] = $imageUpload["message"]["data"];
            }
            $user = User::create($req);
            $req['user_id'] = $user->id;
            $hasIsSubscribeField = 0;
            $hasNotificationField = 0;
            if ($request->has('meta')) {
                $rules = [
                    'meta_key' => 'required',
                    'meta_value' => 'required',
                ];

                foreach ($request['meta'] as $key => $req) {
                    if(!is_array($req)){
                        return response()->json([
                            'status' => '403',
                            'message' => "Meta must contain the object with meta_key and meta_value"
                        ], 403);
                    }
                    try {

                        $validator = Validator::make($req, $rules);

                        if ($validator->fails()) {
                            $error = $validator->errors();
                            DB::rollBack();
                            return response()->json([
                                'status' => '422',
                                "message" => [$key => $error]
                            ]);
                        }
                        $req['user_id'] = $user->id;

                        $req["meta_key"] = strtolower($req["meta_key"]);
                        $userMeta = UserMeta::where([['user_id', $user->id], ['meta_key', $req["meta_key"]]])->first();
                        if($req['meta_key'] == "is_subscribed"){
                            if($req['meta_value'] != 0 && $req['meta_value'] != 1){
                                $req['meta_value']=0;
                            }
                            $hasIsSubscribeField = 1;
                        }
                        if($req['meta_key'] == "notification"){
                            if($req['meta_value'] != 0 && $req['meta_value'] != 1){
                                $req['meta_value']=0;
                            }
                            $hasNotificationField = 1;
                        }

                        try {
                            if ($userMeta) {

                                throw new \Exception();
                            }
                        } catch (\Exception $ex) {
                            DB::rollBack();
                            return response()->json([
                                'status' => '409',
                                'message' => "User Meta could not be created due to duplicate entry "
                            ], 409);
                        }
                        $countryMeta = UserMeta::create($req);
                    } catch (\Exception $e) {
                        DB::rollBack();
                        return response()->json([
                            'status' => '422',
                            'message' => $e->getMessage()
                        ], 422);
                    }
                }
            }

            /** create role of user
             * Check if User Already has role.
             */
            try {
                $roles = $user->roles;
                if (count($roles) != 0) {
                    throw new \Exception();
                }
            } catch (\Exception $ex) {
                return response()->json([
                    'status' => '403',
                    'message' => "User already has role"
                ], 403);
            }

            try {
                $role = Role::findOrFail($request['role_id']);
                $attach = $user->attachRole($role);
                //creates record in driver aviability
                if(($role->name == 'driver' || $role->name == 'driver-captain')){
                    DriverAviability::create([
                        'user_id' =>  $req['user_id'],
                        'availability' => 1
                    ]);
                }
                if($role->name ==="admin" || $role->name ==="super-admin"){
                    if(!$this->passAdminInfoToRedis()){
                        return response()->json([
                            "status" => "503",
                            "message" => "Redis server is not running"
                        ],503);
                    }
                }
            } catch (QueryException $ex) {
                DB::rollBack();
                return response()->json([
                    'status' => '409',
                    'message' => "Role could not be attached to user due to duplicate entry"
                ], 409);
            } catch (ModelNotFoundException $ex) {
                DB::rollBack();
                return response()->json([
                    'status' => '404',
                    'message' => "Submitted Role Name could not be found"
                ], 404);
            } catch (\Exception $ex) {
                DB::rollBack();
                return response()->json([
                    'status' => '404',
                    'message' => "Submitted Role Name could not be attached"
                ], 404);
            }

            /**
             *Creating is_subscribed and notification user meta for customer.
             * */
            if ($role->name == "customer") {
                $additional_meta['user_id'] = $user->id;
                if($hasIsSubscribeField == 0){
                    try {
                        $additional_meta['meta_key'] = "is_subscribed";
                        $additional_meta['meta_value'] = 0;
                        UserMeta::create($additional_meta);
                    } catch (\Exception $ex) {
                        DB::rollBack();
                        return response()->json([
                            'status' => '404',
                            'message' => "User Meta could not be created"
                        ], 404);
                    }
                }
                if($hasNotificationField == 0){
                    try {
                        $additional_meta['meta_key'] = "notification";
                        $additional_meta['meta_value'] = 0;
                        UserMeta::create($additional_meta);
                    } catch (\Exception $ex) {
                        DB::rollBack();
                        return response()->json([
                            'status' => '404',
                            'message' => "User Meta could not be created"
                        ], 404);
                    }
                }
            }


            //todo store user_meta
            DB::commit();
            return response()->json([
                'status' => '200',
                'message' => 'User created successfully'
            ]);
        } catch (QueryException $ex) {
            return response()->json([
                'status' => '409',
                'message' => "User could not be created due to duplicate entry"
            ], 409);
        } catch (\Exception $e) {
            return response()->json([
                'status' => '422',
                'message' => $e->response->original
            ], 422);
        }
    }

    public function displayPermissionList()
    {
        try {
            $user = User::findOrFail(Auth::id())->where("status", 1)->first();
            $role = $user->roles()->get();
            $permission = $role[0]->permissions()->select("name")->get();
            return response()->json([
                "status" => "200",
                "data" => $permission]);
        } catch (\Exception $ex) {
            return response()->json([
                "status" => "404",
                "message" => "User Not Found"
            ], 404);
        }
    }


    /**
     * Store the specified resource with Customer .. registration of customer
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */

    public function register($role, Request $request)
    {
        DB::beginTransaction();
        try {
            $role = strtolower($role);
            if ($role == "customer" || $role == "driver") {

            } else {
                throw new \Exception();
            }
        } catch (\Exception $ex) {
            return response(['status' => 404, 'message' => 'Requested parameter must be customer or driver'], 404);
            DB::rollBack();
        }

        try {
            $request['status'] = 0;
            $ageLimit = Config::get("config.age_limit");
            $message =[
                "domain_validation" => "The mail server for domain does not exist.",
                "birthday_validation" => "Your age must be at least $ageLimit or more."
            ];
            if ($role == "customer") {
                $rules = [
                    'first_name' => 'required|max:50',
                    'last_name' => 'required|max:50',
                    'email' => 'required|email|domain_validation|unique:users|max:255',
                    'password' => 'required|max:255',
                    'gender' => 'required|in:male,female',
                    'mobile' => 'required|digits_between:7,20|unique:users',
                    "status" => "integer|min:0|max:1",
                    "birthday" => "date_format:Y-m-d|birthday_validation",
                    'country' => 'required|max:255',
                    'city' => 'required|max:255',
                    "country_code" => "required|string",
                    'address_line_1' => 'required|max:255',
                    'address_line_2' => 'sometimes|max:255',
                    "meta" => "sometimes|array",
                ];
            }
            else {
                $rules = [
                    'first_name' => 'required|max:50',
                    'last_name' => 'required|max:50',
                    'email' => 'required|email|domain_validation|unique:users|max:255',
                    'password' => 'required|max:255',
                    'gender' => 'required|in:male,female',
                    'mobile' => 'required|digits_between:9,20|unique:users',
                    "status" => "integer|min:0|max:1",
                    "birthday" => "date_format:Y-m-d|birthday_validation",
                    'country' => 'required|max:255',
                    'city' => 'required|max:255',
                    "country_code" => "required|string",
                    "country_id" => "required|integer|min:1",
                    'address_line_1' => 'required|max:255',
                    'address_line_2' => 'sometimes|max:255',
                    "meta" => "sometimes|array",
                ];
            }

            $this->validate($request, $rules,$message);
        } catch (\Exception $e) {
            DB::rollBack();
            return response()->json([
                'status' => '422',
                'message' => $e->response->original
            ], 422);
        }
        $lang = Input::get('lang','en');
        $req = $request->all();
        if ($request->has('avatar')) {
            $image['avatar'] = $request["avatar"];
            $image["type"] = "avatar";
            $imageUpload = ImageUploader::upload($image);
            if ($imageUpload["status"] != 200) {
                return response()->json(
                    $imageUpload["message"]
                    , $imageUpload["status"]);
            }
            $req['avatar'] = $imageUpload["message"]["data"];
        }
        if ($role == "customer") {
            $confirmationCode = $req["confirmation_code"] =mt_rand(0,9).mt_rand(0,9).mt_rand(0,9).mt_rand(0,9);
        }

        $user = User::create($req);
        $req['user_id'] = $user->id;
        if(($role == 'driver')){
            DriverAviability::create([
                'user_id' =>  $req['user_id'],
                'availability' => 1
            ]);
        }
        $hasIsSubscribeField = 0;
        $hasNotificationField = 0;

        try {
            if ($request->has('meta')) {
                $rules = [
                    'meta_key' => 'required',
                    'meta_value' => 'required',
                ];


                foreach ($request['meta'] as $key => $req) {
                    if(!is_array($req)){
                        return response()->json([
                            'status' => '403',
                            'message' => "Meta must contain the object with meta_key and meta_value"
                        ], 403);
                    }
                    try {

                        $validator = Validator::make($req, $rules);

                        if ($validator->fails()) {
                            $error = $validator->errors();
                            DB::rollBack();
                            return response()->json([
                                'status' => '422',
                                "message" => [$key => $error]
                            ]);
                        }
                        $req['user_id'] = $user->id;
//                        creates driver aviability

                        $req["meta_key"] = strtolower($req["meta_key"]);
                        $userMeta = UserMeta::where([['user_id', $user->id], ['meta_key', $req["meta_key"]]])->first();

                        if($req['meta_key'] == "is_subscribed"){
                            if($req['meta_value'] != 0 && $req['meta_value'] != 1){
                                $req['meta_value']=0;
                            }
                            $hasIsSubscribeField = 1;
                        }
                        if($req['meta_key'] == "notification"){
                            if($req['meta_value'] != 0 && $req['meta_value'] != 1){
                                $req['meta_value']=0;
                            }
                            $hasNotificationField = 1;
                        }
                        try {
                            if ($userMeta) {
                                throw new \Exception();
                            }
                        } catch (\Exception $ex) {
                            DB::rollBack();
                            return response()->json([
                                'status' => '409',
                                'message' => "User Meta could not be created due to duplicate entry "
                            ], 409);
                        }
                        $countryMeta = UserMeta::create($req);
                    } catch (\Exception $e) {
                        DB::rollBack();
                        return response()->json([
                            'status' => '422',
                            'message' => $e->getMessage()
                        ], 422);
                    }
                }
            }
        } catch (QueryException $ex) {
            DB::rollBack();
            return response()->json([
                'status' => '409',
                'message' => "User could not be created due to duplicate entry"
            ], 409);
        }

        /**
         *Creating is_subscribed and notification user meta for customer.
         * */
        if ($role == "customer") {
            $additional_meta['user_id'] = $user->id;
            if($hasIsSubscribeField == 0){
                try {
                    $additional_meta['meta_key'] = "is_subscribed";
                    $additional_meta['meta_value'] = 0;
                    UserMeta::create($additional_meta);
                } catch (\Exception $ex) {
                    DB::rollBack();
                    return response()->json([
                        'status' => '404',
                        'message' => "User Meta could not be created"
                    ], 404);
                }
            }
            if($hasNotificationField == 0){
                try {
                    $additional_meta['meta_key'] = "notification";
                    $additional_meta['meta_value'] = 0;
                    UserMeta::create($additional_meta);
                } catch (\Exception $ex) {
                    DB::rollBack();
                    return response()->json([
                        'status' => '404',
                        'message' => "User Meta could not be created"
                    ], 404);
                }
            }
        }

        /*
         * Assign User with role customer
         * */
        try {
            $role = Role::where('name', $role)->first();
            if (is_null($role)) {
                throw new \Exception();
            }
            $user->attachRole($role);
            DB::commit();
            if($role->name == "customer"){
                $userData= $user->toArray();
                $array = [
                    'service' => 'oauth service',
                    'message' => 'customer create',
                    'data' => [
                        "user_details" => $userData,
                        "confirmation_url" => $confirmationCode
                    ]
                ];
                event(new UserEvent($array));
                return \Settings::getErrorMessageOrDefaultMessage('oauth-registration-successful-please-check-your-phone-for-sms',
                    "Registration Successful: Please check your phone for SMS verification code.", 200, $lang);
//                return response()->json([
//                    'status' => '200',
//                    'message' => 'Registration Successful: Please check your phone for SMS verification code.'
//                ], 200);

            }
            else{
                $userData= $user->toArray();

                $array = [
                    'service' => 'oauth service',
                    'message' => 'driver create',
                    'data' =>  [
                        "user_details" => $userData,
                        //"confirmation_url" =>  Config::get("config.oauth_base_url")."/user/confirmation/$confirmationCode"
                    ]
                ];
                event(new UserEvent($array));

            }
            return \Settings::getErrorMessageOrDefaultMessage('oauth-user-created-successfully',
                "User created successfully.", 200, $lang);
//            return response()->json([
//                'status' => '200',
//                'message' => 'User created successfully'
//            ], 200);
            /**
             * since role in return in array so base [0] is used to get object inside index 0
             **/
        } catch (QueryException $ex) {
            return response()->json([
                'status' => '409',
                'message' => "Role could not be attached to user due to duplicate entry"
            ], 409);
        } catch (\Exception $ex) {
            return response()->json([
                'status' => '404',
                'message' => "Submitted Role Name could not be found"
            ], 404);
        }
    }

    /*
     *  Create user with role branch-admin or restaurant admin
     * */
    public function storeWithRole(Request $request)
    {
        DB::beginTransaction();
        try {
            $request["status"] = Input::get("status", 1);
            $rules = [
                'first_name' => 'required|max:50',
                'last_name' => 'required|max:50',
                'email' => 'required|email|unique:users|max:255',
                'gender' => 'required|in:male,female',
                'mobile' => 'required|digits_between:7,20',
                "role" => "required",
                'status' => "integer|min:0|max:1",
                "birthday" => "date_format:Y-m-d",
                'country' => 'max:255',
                'city' => 'max:255',
                'address_line_1' => 'max:255',
                'address_line_2' => 'sometimes|max:255',
                "meta" => "required|array",
                "meta.is_restaurant_admin" => 'required|boolean',
                "meta.restaurant_id" => "required|numeric|min:1",
                "meta.branch_id" => 'numeric|min:1'
            ];

            $this->validate($request, $rules);
            try {
                /*
                * Checking if the given role exist or not
                * */
                $role = Role::where('name', $request["role"])->first();
                /*
                 * Checking if the given role is restaurant-admin or branch-admin
                 * */
                if (is_null($role)) {
                    throw new \Exception();
                }
            } catch (\Exception $ex) {
                return response()->json([
                    'status' => '404',
                    'message' => "Submitted Role Name could not be found"
                ], 404);
            }
            try {
                if (in_array($role->name, ['admin', 'super-admin'])) {
                    throw new \Exception();
                }
            } catch (\Exception $ex) {
                return response()->json([
                    'status' => '403',
                    'message' => "Role name should not be admin or super-admin"
                ], 403);
            }
            $createRestaurantMeta = $request["meta"];
            if ($role->name == "restaurant-admin") {
                /*if role is restaurant-admin then it is the case for restaurant so branch_id is set to null
                 * */
                $createRestaurantMeta['branch_id'] = null;
            }
            if ($role->name == "branch-admin" && !isset($request["meta"]['branch_id'])) {
                return response()->json([
                    "status" => "422",
                    "message" => ["meta" => ["meta.branch_id" => ["meta.branch_id is required for role branch-admin"]]]
                ], 422);
            }
            /*
             * checking is request meta has branch_id or not
             * if branch_id is not present means it is the case of restaurant
             * so is_restaurant_admin is set true
             * else is_restaurant_admin is set false
             * */
            switch (isset($request["meta"]['branch_id'])) {
                case true:
                    try {
                        /*
                         * in case of restaurant branch ,is_restaurant_admin is  always set to false
                         * */
                        $createRestaurantMeta["is_restaurant_admin"] = false;
                        if (!in_array($role->name, ['restaurant-admin', 'branch-admin'])) {
                            if (!in_array($role->parent_role, ['branch-admin'])) {
                                throw new \Exception();
                            }
                        }
                    } catch (\Exception $ex) {
                        return response()->json([
                            'status' => '403',
                            'message' => "Submitted Role parent_role must be  branch-admin "
                        ], 403);
                    }
                    /*
                     * checking if same branch id have one branch admin or not
                     * if not then allowed to create branch admin for that branch id
                     * */
                    if ($this->checkRestaurantBranchAdmin($request["meta"]["restaurant_id"], $request["meta"]['branch_id'], $role->name)) {
                        return response()->json([
                            "status" => "409",
                            "message" => "Restaurant Branch can have only one branch admin"
                        ], 409);
                    }
                    break;
                case false:
                    try {
                        /*
                         * in case of restaurant branch ,is_restaurant_admin is  always set to true
                         * */
                        $createRestaurantMeta["is_restaurant_admin"] = true;
                        if (!in_array($role->name, ['restaurant-admin', 'branch-admin'])) {
                            if (!in_array($role->parent_role, ['restaurant-admin'])) {
                                throw new \Exception();
                            }
                        }
                    } catch (\Exception $ex) {
                        return response()->json([
                            'status' => '403',
                            'message' => "Submitted Role parent_role must be  restaurant-admin "
                        ], 403);
                    }
                    /*
                    * checking if same restaurant id have one restaurant admin or not
                    * if not then allowed to create restaurant admin for that restaurant id
                    * */
                    if ($this->checkRestaurantAdmin($request["meta"]["restaurant_id"], $role->name)) {
                        return response()->json([
                            "status" => "409",
                            "message" => "Restaurant  can have only one restaurant admin"
                        ], 409);
                    }
                    break;
            }

            $req = $request->all();
            if ($request->has('avatar')) {
                $image['avatar'] = $request["avatar"];
                $image["type"] = "avatar";
                $imageUpload = ImageUploader::upload($image);
                if ($imageUpload["status"] != 200) {
                    return response()->json(
                        $imageUpload["message"]
                        , $imageUpload["status"]);
                }
                $request['avatar'] = $imageUpload["message"]["data"];
            }
            /*
             * creating user in user table
             * */
            try {
                $user = User::create($request->all());
            } catch (\Exception $ex) {
                return response()->json([
                    'status' => "400",
                    "message" => " User could not be created"
                ], 400);
            }
            /**
             * Checking if there is role restaurant_admin or not before creating parent_role restaurant_admin
             * And Checking if there is role branch_admin or not before creating parent_role branch_admin
             */
            $flag = false;
            if (!in_array($role->name, ['restaurant-admin', 'branch-admin'])) {
                if ($role->parent_role == 'branch-admin') {
                    $users = RestaurantUserMeta::where('branch_id', $request["meta"]["branch_id"])->get();
                    foreach ($users as $u) {
                        $u = User::findOrFail($u->user_id);

                        if ($u->roles[0]["name"] == "branch-admin") {
                            $flag = true;
                        }
                    }
                    if ($flag == false) {
                        return response()->json([
                            'status' => "404",
                            "message" => "There must be atleast one branch-admin"
                        ], 404);
                    }

                }
            }

            $createRestaurantMeta["user_id"] = $user['id'];
            try {
                /*
                 * creating meta is restaurant user meta table
                 * */
                $restaurantMeta = RestaurantUserMeta::create($createRestaurantMeta);
            } catch (\Exception $ex) {
                return response()->json([
                    'status' => "400",
                    "message" => "User Meta could not be created"
                ], 400);
            }

        } catch (QueryException $ex) {
            DB::rollBack();
            return response()->json([
                'status' => '409',
                'message' => "User could not be created"
            ], 409);
        } catch (\Exception $e) {
            DB::rollBack();
            return response()->json([
                'status' => '422',
                'message' => $e->response->original
            ], 422);
        }
        /*
         * if above all condition satisfies then the role is attached to the user
         * */
        $user->attachRole($role);
        DB::commit();
        return response()->json([
            'status' => '200',
            'data' => $user
        ], 200);
        /**
         * since role in return in array so base [0] is used to get object inside index 0
         **/

    }

    public function storeWithRoleRestaurantCustomer(Request $request)
    {
        DB::beginTransaction();
        try {
            $message = [
                "domain_validation" => "The mail server for domain does not exist."
            ];
            $request["status"] = Input::get("status", 1);
            $rules = [
                'first_name' => 'required|max:50',
                'last_name' => 'required|max:50',
                'email' => 'required|email|domain_validation|unique:users|max:255',
                'gender' => 'required|in:male,female',
                'mobile' => 'required|digits_between:7,20',
                'status' => "integer|min:0|max:1",
                "birthday" => "date_format:Y-m-d",
                'is_normal' => 'integer|min:0|max:1',
                'country' => 'max:255',
                'city' => 'max:255',
                'address_line_1' => 'max:255',
                'address_line_2' => 'sometimes|max:255',
                "meta" => "array",
            ];

            $this->validate($request, $rules,$message);
            try {
                /*
                * Checking if the given role exist or not
                * */
                $role = Role::where('name', "restaurant-customer")->first();
                /*
                 * Checking if the given role is restaurant-admin or branch-admin
                 * */
                if (is_null($role)) {
                    throw new \Exception();
                }
            } catch (\Exception $ex) {
                return response()->json([
                    'status' => '404',
                    'message' => "Submitted Role Name could not be found"
                ], 404);
            }
            try {
                if (in_array($role->name, ['admin', 'super-admin'])) {
                    throw new \Exception();
                }
            } catch (\Exception $ex) {
                return response()->json([
                    'status' => '403',
                    'message' => "Role name should not be admin or super-admin"
                ], 403);
            }
            if ($request->has('avatar')) {
                $image['avatar'] = $request["avatar"];
                $image["type"] = "avatar";
                $imageUpload = ImageUploader::upload($image);
                if ($imageUpload["status"] != 200) {
                    return response()->json(
                        $imageUpload["message"]
                        , $imageUpload["status"]);
                }
                $request['avatar'] = $imageUpload["message"]["data"];
            }
            /*
             * creating user in user table
             * */
            try {
                $request->merge([
                    'is_normal' => $request->get('is_normal',0)
                ]);
                $user = User::create($request->all());
            } catch (\Exception $ex) {
                return response()->json([
                    'status' => "400",
                    "message" => " User could not be created"
                ], 400);
            }

        } catch (QueryException $ex) {
            DB::rollBack();
            return response()->json([
                'status' => '409',
                'message' => "User could not be created"
            ], 409);
        } catch (\Exception $e) {
            DB::rollBack();
            return response()->json([
                'status' => '422',
                'message' => $e->response->original
            ], 422);
        }
        /*
         * if above all condition satisfies then the role is attached to the user
         * */
        $user->attachRole($role);
        DB::commit();
        return response()->json([
            'status' => '200',
            'data' => $user
        ], 200);
        /**
         * since role in return in array so base [0] is used to get object inside index 0
         **/

    }


    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        try {
            $user = User::with('userMeta')->findOrFail($id);
        } catch (QueryException $ex) {
            return response()->json([
                'status' => '404',
                'message' => "Users could not be found"
            ], 404);
        } catch (ModelNotFoundException  $ex) {
            return response()->json([
                'status' => '404',
                'message' => "Users could not be found"
            ], 404);
        } catch (\Exception $ex) {
            return response()->json([
                'status' => '404',
                'message' => "Users could not be found"
            ], 404);
        }
        try {
            $role = $user->roles()->get();
            if (count($role) > 0) {
                $user['role_id'] = $role[0]['id'];
                $user['role_name'] = $role[0]['name'];
            }
        } catch (\Exception $ex) {
            DB::rollBack();
            return response()->json([
                'status' => '403',
                'message' => "Role could not be found"
            ], 403);
        }


        return response()->json([
            'status' => '200',
            'data' => $user
        ]);
    }

    public function getUserDetailForMachineRequest($userId){
        try{
            $getUser = User::select("first_name","last_name","mobile","middle_name",'country','city','address_line_1','address_line_2','email')->findOrFail($userId);

            return response()->json([
                "status" => "200",
                "data" => $getUser
            ]);
        }
        catch (\Exception $ex){
            return response()->json([
                "status" => "404",
                "message" => "User Not found"
            ],404);
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        try {
            $message = [
                'customer_priority.in' => 'The customer_priority field must have value 10 for VIP and 30 for normal user.'
            ];
            $this->validate($request, [
                'first_name' => 'required|max:50',
                'last_name' => 'required|max:50',
                'password' => "string|max:255",
                /*
                 * this validation will not allow other stored email to be used and also allows to use the
                 * current user email which would not be done by unique:users
                 * */
                'email' => 'required|email|max:255|unique:users,email,' . $id,
                'gender' => 'in:male,female',
                'mobile' => 'required|digits_between:9,20',
                "status" => "integer|min:0|max:1",
                "birthday" => "date_format:Y-m-d",
                'country' => 'sometimes|max:255',
                'city' => 'sometimes|max:255',
                'customer_priority' => 'required|in:10,30',
                'is_normal' => 'integer|min:0|max:1',
                "country_code" => "sometimes",
                'address_line_1' => 'sometimes|max:255',
                'address_line_2' => 'sometimes|max:255',
                'role_id' => 'required|integer|min:1',
            ]);
            if($request->role_id == 4){
                $this->validate($request, [
                    'customer_priority' => 'required|in:10,30'
                ],$message);
            }
            try {
                $userData = $users = User::findOrFail($id);
                if(Auth::id() == $userData->id) {
                    return response()->json([
                        "status" => "403",
                        "message" => "You cannot update your own information and role from here"
                    ], 403);
                } elseif (Auth::user()->hasRole("admin") && ($userData->hasRole("admin") ||$userData->hasRole("super-admin"))) {
                    return response()->json([
                        "status" => "403",
                        "message" => "You cannot update data of other admin or super admin"
                    ], 403);
                }

            } catch (ModelNotFoundException $ex) {
                return response()->json([
                    'status' => '404',
                    'message' => "User Not found"
                ], 404);
            }
            DB::beginTransaction();
            try {
                $roles = $users->roles;
                if (count($roles) > 0)
                    foreach ($roles as $role){
                        $users->detachRole($role);
                    }
            } catch (\Exception $ex) {
                DB::rollBack();
                return response()->json([
                    'status' => '403',
                    'message' => "Role could not be detached"
                ], 403);
            }

            try {
                $users->userMetas()->delete();
            } catch (\Exception $ex) {
                DB::rollBack();
                return response()->json([
                    'status' => '403',
                    'message' => "User Meta Could not be deleted"
                ], 403);
            }
            if ($request->has('meta')) {
                $rules = [
                    'meta_key' => 'required',
                    'meta_value' => 'required',
                ];

                foreach ($request['meta'] as $key => $req) {
                    if(!is_array($req)){
                        return response()->json([
                            'status' => '403',
                            'message' => "Meta must contain the object with meta_key and meta_value"
                        ], 403);
                    }
                    try {

                        $validator = Validator::make($req, $rules);

                        if ($validator->fails()) {
                            $error = $validator->errors();
                            DB::rollBack();
                            return response()->json([
                                'status' => '422',
                                "message" => [$key => $error]
                            ]);
                        }
                        $req['user_id'] = $users->id;
                        $req["meta_key"] = strtolower($req["meta_key"]);
                        $userMeta = UserMeta::where([['user_id', $users->id], ['meta_key', $req["meta_key"]]])->first();
                        try {
                            if ($userMeta) {
                                throw new \Exception();
                            }
                        } catch (\Exception $ex) {
                            DB::rollBack();
                            return response()->json([
                                'status' => '409',
                                'message' => "User Meta could not be created due to duplicate entry "
                            ], 409);
                        }
                        $countryMeta = UserMeta::create($req);
                    } catch (\Exception $e) {
                        DB::rollBack();
                        return response()->json([
                            'status' => '422',
                            'message' => $e->getMessage()
                        ], 422);
                    }
                }
//                $request['avatar'] = $imageUpload["message"]["data"];
            }
            if(isset($request["password"]) &&( empty($request["password"]) || is_null($request["password"]))){
                $users->update($request->except( 'password','email'));
            }
            else {
                $users->update($request->except('email'));
            }
            /** create role of user
             * Check if User Already has role.
             */
            try {
                $roles = $users->roles;
                if (count($roles) != 0) {
                    foreach ($roles as $role){
                        $users->detachRole($role);
                    }
                }
                $role = Role::findOrFail($request['role_id']);
                $users->attachRole($role);
                if($role->name ==="admin" || $role->name ==="super-admin"){
                    if(!$this->passAdminInfoToRedis()){
                        return response()->json([
                            "status" => "503",
                            "message" => "Redis server is not running"
                        ],503);
                    }
                }
                $roleName = $role->name;
                $activate = null;
                if($request["status"]=="1"){
                    $activate = true;
                }
                else if($request["status"]=="0"){
                    $activate = false;
                }
            } catch (QueryException $ex) {
                DB::rollBack();
                return response()->json([
                    'status' => '409',
                    'message' => "Role could not be attached to user due to duplicate entry"
                ], 409);
            } catch (ModelNotFoundException $ex) {
                DB::rollBack();
                return response()->json([
                    'status' => '404',
                    'message' => "Submitted Role Name could not be found"
                ], 404);
            }


            if ($request->has('avatar')) {
                $image['avatar'] = $request["avatar"];
                $image["type"] = "avatar";
                $imageUpload = ImageUploader::upload($image);
                if ($imageUpload["status"] != 200) {
                    return response()->json(
                        $imageUpload["message"]
                        , $imageUpload["status"]);
                }
                $request['avatar'] = $imageUpload["message"]["data"];
            }

            $users = $users->update($request->except('password',"mobile"));

            DB::commit();
            $userData= $userData->toArray();
            if($role->name == "customer"){
                $array = [
                    'service' => 'oauth service',
                    'message' => 'customer update by admin',
                    'data' => [
                        "user_details" => $userData,
                        "activate" =>  $activate
                    ]
                ];
                event(new AdminEvent($array));

            }
            else if($role->name == "driver"){
                $array = [
                    'service' => 'oauth service',
                    'message' => 'driver update by admin',
                    'data' =>  [
                        "user_details" => $userData,
                        "activate" => $activate
                        //"confirmation_url" =>  Config::get("config.oauth_base_url")."/user/confirmation/$confirmationCode"
                    ]
                ];
                event(new AdminEvent($array));

            }

            else if($role->name == "restaurant-admin" || $role->parent_role == "restaurant-admin" ){
                $array = [
                    'service' => 'oauth service',
                    'message' => 'restaurant admin update by admin',
                    'data' =>  [
                        "user_details" => $userData,
                        "activate" => $activate,
                        "role" => "Restaurant Admin"

                        //"confirmation_url" =>  Config::get("config.oauth_base_url")."/user/confirmation/$confirmationCode"
                    ]
                ];
                event(new AdminEvent($array));

            }
            else if($role->name == "branch-admin" || $role->parent_role == "branch-admin" ){
                $array = [
                    'service' => 'oauth service',
                    'message' => 'restaurant admin update by admin',
                    'data' =>  [
                        "user_details" => $userData,
                        "activate" => $activate,
                        "role" => "Branch Admin"
                        //"confirmation_url" =>  Config::get("config.oauth_base_url")."/user/confirmation/$confirmationCode"
                    ]
                ];
                event(new AdminEvent($array));

            }
            return response()->json([
                'status' => '200',
                'message' => 'User updated successfully'
            ]);
        } catch (QueryException $ex) {
            DB::rollBack();
            return response()->json([
                'status' => '403',
                'message' => "Users not updated"
            ], 403);
        } catch (ModelNotFoundException  $ex) {
            DB::rollBack();
            return response()->json([
                'status' => '403',
                'message' => "Users not updated"
            ], 403);
        } catch (\Exception $ex) {
            DB::rollBack();
            return response()->json([
                'status' => '422',
                'message' => $ex->response->original
            ], 422);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            $user = User::findOrFail($id);
            $role = $user->roles[0]->name;
            if ($user->hasRole("restaurant-admin") || $user->hasRole("branch-admin")) {
                return response()->json([
                    "status" => "401",
                    "message" => "User With role branch-admin or restaurant-admin cannot be deleted directly"
                ], 401);
            } elseif (!Auth::user()->hasRole("super-admin") && $user->hasRole("super-admin")) {
                return response()->json([
                    "status" => "401",
                    "message" => "User With role super-admin  cannot be deleted "
                ], 401);
            } elseif (Auth::user()->hasRole("admin") && $user->hasRole("admin")) {
                return response()->json([
                    "status" => "401",
                    "message" => "User With role admin  cannot be deleted "
                ], 401);
            }

//            $user->delete();
            if($role =="admin"|| $role =="super-admin"){
                if(!$this->passAdminInfoToRedis()){
                    return response()->json([
                        "status" => "503",
                        "message" => "Redis server is not running"
                    ],503);
                }
            }
            if($user->status == 0){
                $user->delete();
                return response()->json([
                    'status' => '200',
                    'message' => "User deleted successfully"
                ], 200);
            }
            else{
                $user->update([
                    'status' => 0
                ]);
                return response()->json([
                    'status' => '200',
                    'message' => "User deactivated successfully"
                ], 200);
            }

        } catch (QueryException $ex) {
            return response()->json([
                'status' => '403',
                'message' => "User not deleted"
            ], 403);
        } catch (ModelNotFoundException  $ex) {
            return response()->json([
                'status' => '404',
                'message' => "User could not be found"//added
            ], 404);
        } catch (\Exception $ex) {
            return response()->json([
                'status' => '403',
                'message' => "User not deleted"
            ], 403);
        }
    }

    /**
     * update country_id of logged in user
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function updateCountryIdForDriver(Request $request)
    {
        try{
            $this->validate($request,[
                "country_id" => "required|integer|min:1"
            ]);

        }
        catch (\Exception $ex){
            return response()->json([
                "status" => "500",
                "message" => $ex->response->original
            ],500);
        }
        $userId = Auth::id();
        $updateId = User::findOrFail($userId)->update(["country_id" => $request->country_id]);
        return response()->json([
            "status" => '200',
            "message" => "Country updated successfully"
        ],200);

    }





    public function forgetPassword(Request $request){
        try{
            $this->validate($request,[
                "email" => "required|email"
            ]);
        }
        catch (\Exception $ex){
            return response()->json([
                "status" => "422",
                "message" => $ex->response->original
            ],422);
        }
        $lang = Input::get('lang','en');
        try{

            $getSetting = Settings::getSettings('password-reset-link');
            if($getSetting["status"] == "200"){
                $getPasswordResetLink = $getSetting["message"];
            }else{
                return response()->json($getSetting,$getSetting["status"]);
            }
            $checkUserExist = User::where([
                ["email", $request->email],
                ["status",1]
            ])->firstOrFail();
            $now = Carbon::now("UTC");
            $token = password_hash($checkUserExist->id.str_random(32).uniqid(),PASSWORD_DEFAULT );
            PasswordReset::create([
                "email" => $checkUserExist->email,
                "token" => $token,
                "created_at" => $now
            ]);
            $array = [
                'service' => 'oauth service',
                'message' => 'forgot password',
                'data' =>  [
                    "user_details" => $checkUserExist->toArray(),
                    "confirmation_url" =>  $getPasswordResetLink["data"]["value"]."?reset_code=$token"
                ]
            ];
            event(new UserEvent($array));
            return \Settings::getErrorMessageOrDefaultMessage('oauth-check-your-email-to-receive-link-that-reset-your-password',
                "Check your email to receive link that reset your password.", 200, $lang);
//            return response()->json([
//                "status" => "200",
//                "message" => "Check your email to receive link that reset your password."
//            ]);
        }
        catch (ModelNotFoundException $ex){
            return \Settings::getErrorMessageOrDefaultMessage('oauth-user-could-not-be-found-it-may-be-due-to-user-inactive',
                "User could not be found. It may be due to user is inactive or user is not registered.", 404, $lang);
//            return response()->json([
//                "status" => "404",
//                "message" => "User could not be found. It may be due to user is inactive or user is not registered."
//            ],404);

        }
    }

    /**
     * Update  update user information if currently logged in user is equal to passed user id
     * and password matches
     *
     * @param  $id
     * @return \Illuminate\Http\JsonResponse
     */

    public function updateUserDetail($id, Request $request)
    {
        if (Auth::id() != $id) {
            return response()->json([
                "status" => "403",
                "message" => "You are only allowed to update your information only"
            ], 403);
        }
        try {
            $this->validate($request, [
                'first_name' => 'required|max:50',
                'last_name' => 'required|max:50',
                /*
                 * this validation will not allow other stored email to be used and also allows to use the
                 * current user email which would not be done by unique:users
                 * */
                'password' => "max:255",
                "new_password" =>"required_with:password|max:255",
                'gender' => 'in:male,female',
//                'mobile' => 'required|digits_between:9,20',
                "birthday" => "date_format:Y-m-d",
                'country' => 'required|max:255',
                'city' => 'required|max:255',
//            "country_code" =>"required",
//            'district' => 'required|max:255',
                'address_line_1' => 'required|max:255',
                'address_line_2' => 'sometimes|max:255',
            ]);
        } catch (\Exception $ex) {
            return response()->json([
                "status" => "422",
                "message" => $ex->response->original
            ], 422);
        }
        $lang = Input::get('lang','en');
        try {
            $users = User::findOrFail($id);
            $users = $users->makeVisible("password");
        } catch (ModelNotFoundException $ex) {
            return response()->json([
                'status' => '404',
                'message' => "User Not found"
            ], 404);
        }

        try {
            $users->userMetas()->delete();
        } catch (\Exception $ex) {
            DB::rollBack();
            return response()->json([
                'status' => '403',
                'message' => "User Meta Could not be deleted"
            ], 403);
        }
        if ($request->has('meta')) {
            $rules = [
                'meta_key' => 'required',
                'meta_value' => 'required',
            ];

            foreach ($request['meta'] as $key => $req) {
                if(!is_array($req)){
                    return response()->json([
                        'status' => '403',
                        'message' => "Meta must contain the object with meta_key and meta_value"
                    ], 403);
                }
                try {

                    $validator = Validator::make($req, $rules);

                    if ($validator->fails()) {
                        $error = $validator->errors();
                        DB::rollBack();
                        return response()->json([
                            'status' => '422',
                            "message" => [$key => $error]
                        ]);
                    }
                    $req['user_id'] = $users->id;
                    $req["meta_key"] = strtolower($req["meta_key"]);
                    $userMeta = UserMeta::where([['user_id', $users->id], ['meta_key', $req["meta_key"]]])->first();
                    try {
                        if ($userMeta) {
                            throw new \Exception();
                        }
                    } catch (\Exception $ex) {
                        DB::rollBack();
                        return response()->json([
                            'status' => '409',
                            'message' => "User Meta could not be created due to duplicate entry "
                        ], 409);
                    }
                    $countryMeta = UserMeta::create($req);
                } catch (\Exception $e) {
                    DB::rollBack();
                    return response()->json([
                        'status' => '422',
                        'message' => $e->getMessage()
                    ], 422);
                }
            }
        }

        if(!isset($request["password"])) goto here;
        if(!Hash::check($request["password"], $users->password)){
            return \Settings::getErrorMessageOrDefaultMessage('oauth-password-is-incorrect',
                "Password is incorrect.", 403, $lang);
//            return response()->json([
//                "status" => "403",
//                "message" => "Password is incorrect."
//            ],403);
        }
        here:        if ($request->has('avatar')) {
        $image['avatar'] = $request["avatar"];
        $image["type"] = "avatar";
        $imageUpload = ImageUploader::upload($image);
        if ($imageUpload["status"] != 200) {
            return response()->json(
                $imageUpload["message"]
                , $imageUpload["status"]);
        }
        $request['avatar'] = $imageUpload["message"]["data"];
    }
        $password = false;
        if(!isset( $request["new_password"])) $users->update($request->except('email', 'status' , 'password','mobile'));
        else{
            $request["password"] = $request["new_password"];
            $password = true;
            $users->update($request->except('email', 'status','mobile'));
        }

        $userData = User::find($id);
        $array = [
            'service' => 'oauth service',
            'message' => 'update by user',
            'data' =>  [
                "user_details" => $userData->toArray(),
                "password" => $password
                //"confirmation_url" =>  Config::get("config.oauth_base_url")."/user/confirmation/$confirmationCode"
            ]
        ];
        event(new UserEvent($array));
        return \Settings::getErrorMessageOrDefaultMessage('oauth-user-updated-successfully',
            "User updated successfully.", 200, $lang);
//        return response()->json([
//            'status' => '200',
//            'message' => 'User updated successfully'
//        ]);

//        }
//        else{
//            return response()->json([
//                "status" => "403",
//                "message" => "Password is incorrect "
//            ],403);
//        }


    }

    public function changePassword(Request $request)
    {
        try {
            $this->validate($request, [
                'password' => "required|max:255",
                "new_password" => "required|max:255",
            ]);
        } catch (\Exception $ex) {
            return response()->json([
                "status" => "422",
                "message" => $ex->response->original
            ], 422);
        }
        $lang = Input::get('lang','en');
        try {
            $user = User::findOrFail(Auth::id());
            $user = $user->makeVisible("password");
        } catch (ModelNotFoundException $ex) {
            return response()->json([
                'status' => '404',
                'message' => "User Not found"
            ], 404);
        }
        if (Hash::check($request["password"], $user->password)) {
            $request["password"] = $request["new_password"];
            $user->update($request->only('password'));
            return \Settings::getErrorMessageOrDefaultMessage('oauth-password-changed-successfully',
                "Password changed successfully.", 200, $lang);
//            return response()->json([
//                'status' => '200',
//                'message' => 'Password changed successfully.'
//            ]);
        } else {
            return \Settings::getErrorMessageOrDefaultMessage('oauth-password-is-incorrect',
                "Password is incorrect.", 403, $lang);
//            return response()->json([
//                "status" => "403",
//                "message" => "Password is incorrect "
//            ], 403);
        }
    }

    /**
     * return logged in user detail based on bearer token
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */

    public function getUserDetailForPublic()
    {

        try {
            $user = User::with('userMeta')->findorFail(Auth::id());
            $role = $user->roles->first();
            if($role){
                $permission = $role->permissions()->orderBy("name","asc")->get();
                $user["permissions"] = $permission->pluck("name")->all();
                $role = $role->makeHidden(["id","created_at","updated_at"]);
            }
            else{
                $user["permissions"] = [];
            }


//
            $meta =  $user->restaurantUserMeta()->first();
            if($role && $meta && ($role->name == "restaurant-admin" || $role->parent_role =="restaurant-admin")){
                $user->restaurant_id = $meta->restaurant_id;
            }
            elseif($role &&($role->name == "branch-admin" || $role->parent_role =="branch-admin")){

                $user->branch_id = $meta->branch_id;
                $user->restaurant_id = $meta->restaurant_id;
            }
            if(!isset($user["restaurant_id"])) goto skipRestaurantCalling;
            /**
             * calling restaurant service
             */
            $restaurantFullUrl = Config::get("config.restaurant_base_url")."/oauth/restaurant/$user->restaurant_id";

            $restaurantInfo = ImageUploader::getSpecificWithoutToken($restaurantFullUrl);
            if($restaurantInfo["status"] == "200"){
                $user->country_id = $restaurantInfo["message"]["data"];
            }

            skipRestaurantCalling:
            return response()->json([
                "status" => "200",
                "data" => $user
            ], 200);
        }
        catch (ModelNotFoundException $ex) {
            return response()->json([
                "status" => "404",
                "message" => "Requested User Not found"
            ], 404);

        }
        catch (\Exception $ex) {
            return response()->json([
                "status" => "404",
                "message" => "Requested User Role not found"
            ], 404);

        }
    }

    /**
     * Addition  of user/details/{id} route which will display user information if currently logged in user is equal to passed user id
     *
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function getUserDetail($id)
    {
        if (Auth::id() == $id) {
            try {
                $user = User::with('userMeta')->findorFail($id);
                return response()->json([
                    "status" => "200",
                    "data" => $user
                ], 200);
            } catch (ModelNotFoundException $ex) {
                return response()->json([
                    "status" => "404",
                    "message" => "Requested User Not found"
                ], 404);

            }

        } else {
            return response()->json([
                "status" => "403",
                "message" => "You are only allowed to view your information only"
            ], 403);
        }
    }


    public function destroyUserFromRemoteCall(Request $request)
    {
        try {
            $this->validate($request, [
                'is_restaurant_admin' => 'required|boolean',
                'restaurant_id' => 'required|integer|min:1',
                'branch_id' => 'sometimes|integer|min:1',
                'method' => 'required|alpha|in:hard,soft'
            ]);
        } catch (\Exception $ex) {
            return response()->json([
                'status' => '422',
                'message' => $ex->response->original
            ], 422);
        }
        /*
         * if method is hard then type will be true so that all restaurant user will be obtained
         * otherwise only restaurant users will be obtained
         *
         * */
        $type = false;
        if ($request['method'] == 'hard') {
            $type = true;
        }

        /*
         * checking if is_restaurant_admin is true or not if  not  restauranBranchView function will be called
         * otherwise restaurantUserView function will be called
         * */
        if ($request['is_restaurant_admin']) {
            $users = $this->restaurantUserViewPrivate($request['restaurant_id'], $type);
            if ($users->getStatusCode() != 200) {
                return response()->json($users->original, $users->getStatusCode()
                );
            }
        } else {
            $users = $this->restaurantBranchUserViewPrivate($request['restaurant_id'], $request["branch_id"]);
            if ($users->getStatusCode() != 200) {
                return response()->json($users->original, $users->getStatusCode()
                );
            }
        }
        DB::beginTransaction();
        if ($request['method'] == "hard") {
            if (is_null($users->original["data"])) {
                return response()->json([
                    "status" => "200",
                    "message" => "Empty Users"
                ], 200);
            }
            foreach ($users->original["data"] as $user) {
                try {
//                    if ($user['avatar'] != null) {
//                        $filename = base_path() . '/public/' . $user->avatar;
//                        File::delete($filename);
//                    }
                    $user->delete();
                } catch (ModelNotFoundException  $ex) {
                    DB::rollBack();
                    return response()->json([
                        'status' => '403',
                        'message' => "User not found"
                    ], 403);
                } catch (QueryException $ex) {
                    DB::rollBack();
                    return response()->json([
                        'status' => '403',
                        'message' => "User not deleted"
                    ], 403);
                } catch (\Exception $ex) {
                    DB::rollBack();
                    return response()->json([
                        'status' => '403',
                        'message' => "User not deleted"
                    ], 403);
                }
                try {
                    $userMetas = RestaurantUserMeta::where('user_id', $user["id"])->get();
                    foreach ($userMetas as $userMeta)
                        $userMeta->delete();
                } catch (\Exception  $ex) {
                    DB::rollBack();
                    return response()->json([
                        'status' => '403',
                        'message' => "User Meta not deleted"
                    ], 403);
                }
            }
            DB::commit();
            return response()->json([
                "status" => "200",
                "message" => "Users deleted successfully"
            ], 200);
        } elseif ($request['method'] == "soft") {
            if (is_null($users->original["data"])) {
                return response()->json([
                    "status" => "200",
                    "message" => "Empty Users"
                ], 200);
            }
            foreach ($users->original["data"] as $user) {
                try {
                    $user->status = 0;
                    $user->save();
                } catch (ModelNotFoundException  $ex) {
                    DB::rollBack();
                    return response()->json([
                        'status' => '403',
                        'message' => "User not found"
                    ], 403);
                } catch (QueryException $ex) {
                    DB::rollBack();
                    return response()->json([
                        'status' => '403',
                        'message' => "User not deleted"
                    ], 403);
                } catch (\Exception $ex) {
                    DB::rollBack();
                    return response()->json([
                        'status' => '403',
                        'message' => "User not deleted"
                    ], 403);
                }
            }
            DB::commit();
            return response()->json([
                "status" => "200",
                "message" => "Users deleted successfully"
            ], 200);
        } else {
            DB::rollBack();
            return response()->json([
                "status" => "422",
                "message" => ['method' => 'The Method field must be either soft or hard']
            ], 422);
        }

    }

    /**
     * Checks if the passed branch_id has one branch-admin or not, if $role is branch-admin
     *
     *
     * @param $restaurantId
     * @param $branchId
     * @param $role
     * @return bool| true if role is other than branch-admin and branch doesnot have branch admin
     */
    private function checkRestaurantBranchAdmin($restaurantId, $branchId, $role)
    {
        if ($role != "branch-admin") return false;
        $restaurants = RestaurantUserMeta::where([
            ["is_restaurant_admin", false],
            ["restaurant_id", $restaurantId],
            ["branch_id", $branchId]
        ])->get();
        if (count($restaurants) == 0) return false;
        foreach ($restaurants as $restaurant) {
            $user = $restaurant->user;
            if ($user->hasRole("branch-admin")) return true;
        }
        return false;
    }

    /**
     *  Checks if the passed restaurant_id has one restaurant-admin or not, if $role is restaurant-admin
     *
     * @param $restaurantId
     * @param $role
     * @return bool | true if role is other than restaurant-admin and restaurant doesnot have restaurant-admin
     */
    private function checkRestaurantAdmin($restaurantId, $role)
    {
        if ($role != "restaurant-admin") return false;
        $restaurants = RestaurantUserMeta::where([
            ["is_restaurant_admin", true],
            ["restaurant_id", $restaurantId],
            ["branch_id", null]
        ])->get();
        if (count($restaurants) == 0) return false;
        foreach ($restaurants as $restaurant) {
            $user = $restaurant->user;
            if ($user->hasRole("restaurant-admin")) return true;
        }
        return false;

    }

    /**
     * Get all restaurant  user based on restaurant id .
     *if type is false then only restaurant user will be returned
     * else all the associated restaurant users and branch users
     * will be  returned
     * @param $id
     * @param $type
     * @return \Illuminate\Http\JsonResponse
     */
    private function restaurantUserViewPrivate($id, $type)
    {
        if ($type) {
            $restaurantUsers = RestaurantUserMeta::select("user_id")->where([
                ["restaurant_id", $id]
            ])->get();
        } else {
            $restaurantUsers = RestaurantUserMeta::select("user_id")->where([
                ["restaurant_id", $id],
                ["branch_id", null]
            ])->get();
        }

        try {
            if ($restaurantUsers->count() == 0) {
                throw new \Exception();
            }
        } catch (\Exception $ex) {
            return response()->json([
                "status" => "200",
                "data" => null
            ], 200);
        }
        $users = null;
        foreach ($restaurantUsers as $restaurantUser) {
            $restUser = $restaurantUser->user()->get();
            if ($restUser->count() != 0) {
                $users[] = $restUser[0];
            }
        }
        if (is_null($users)) {
            return response()->json([
                "status" => "404",
                "message" => "Users could not be found for requested restaurant id"
            ], 404);
        }
        return response()->json([
            "status" => "200",
            "data" => $users
        ], 200);
    }

    /**
     * Returns requested restaurant branch users
     * @param $id
     * @param $branchId
     * @return \Illuminate\Http\JsonResponse
     */
    private function restaurantBranchUserViewPrivate($id, $branchId)
    {

        $restaurantUsers = RestaurantUserMeta::select("user_id")->where([
            ["is_restaurant_admin", 0],
            ["restaurant_id", $id],
            ["branch_id", $branchId]
        ])->get();
        try {
            if ($restaurantUsers->count() == 0) {
                throw new \Exception();
            }
        } catch (\Exception $ex) {
            return response()->json([
                "status" => "200",
                "data" => null
            ], 200);
        }
        $users = null;
        foreach ($restaurantUsers as $restaurantUser) {
            $restUser = $restaurantUser->user()->get();
            if ($restUser->count() != 0) {
                $users[] = $restUser[0];
            }
        }
        if (is_null($users)) {
            return response()->json([
                "status" => "404",
                "message" => "Users could not be found "
            ], 404);
        }
        return response()->json([
            "status" => "200",
            "data" => $users
        ]);
    }

    private function passAdminInfoToRedis(){
        try{
            Redis::ping();
        }
        catch(\Exception $ex){
            \Log::error("redis error",[$ex->getMessage()]);
            return false;
        }
        $admin = Role::with('users')->where('name', "admin")->get()[0]->users;
        $superAdmin = Role::with('users')->where('name',"super-admin")->get()[0]->users;
        $combileUsers = $admin->merge($superAdmin);
        $count = 0;
        $users ='';
        foreach ($combileUsers as $user){
            if($count ==0){
                $users = $user["email"];
            }
            else{
                $users .= ','.$user["email"];
            }
            $count++;
        }
        \Redis::DEL("admin-list");
        \Redis::SET("admin-list",$users);
        return true;
    }


    public function getTokensByRole($role, Request $request)
    {
        try {

            $this->validate($request, [
                "encoded_string" => "required",
                "country_id" => "required|integer"
            ]);
        } catch (\Exception $ex) {
            return response()->json([
                "status" => "422",
                "message" => $ex->response->original
            ], 422);
        }

        try {
            $decodedString = $this->decodeString($request['encoded_string']);

            if ($decodedString == 'UserTokens-' . $role) {

                $users = User::whereHas('roles', function ($query) use ($role) {
                    $query->where('name', '=', $role);
                })->with(['userToken'])->where('country_id', $request['country_id'])->get()->toArray();
                if(count($users) > 0) {


                    $tokenList = array_filter(collect($users)->map(function ($item, $key) {
                        return array_pluck($item['user_token'], 'user_id', 'token');
                    })->values()->toArray());

                    $tokenList = collect($tokenList)->flatMap(function ($values) {
                        return $values;
                    })->toArray();

                    $tokens = [
                        'tokens' => array_keys($tokenList),
                        'userIds' => array_values($tokenList),
                    ];


                    return response()->json([
                        "status" => "200",
                        "data" => $tokens
                    ], 200);
                }else{
                    return response()->json([
                        "status" => "404",
                        "message" => "Users not found"
                    ], 404);
                }
            }else
            {
                Log::error("role based token list", [
                    'status' => "403",
                    'message' => "unauthorized",
                    'decoded_string' =>$decodedString
                ]);
                return response()->json([
                    "status" => "403",
                    "message" => "Unauthorized"
                ],403);
            }

        } catch (\Exception $ex) {
            Log::error("role based token list", [
                'status' => "500",
                'message' => $ex->getMessage()

            ]);
            return response()->json([
                "status" => "500",
                "message" => $ex->getMessage()
            ],500);
        }


    }

    private function decodeString($data)
    {
        $key = "Alice123$";
        $encryption_key = base64_decode($key);
        // To decrypt, split the encrypted data from our IV - our unique separator used was "::"
        list($encrypted_data, $iv) = explode('::##::', base64_decode($data), 2);
        return openssl_decrypt($encrypted_data, 'aes-256-cbc', $encryption_key, 0, $iv);
    }

    public function userNotificationInfo($id, Request $request)
    {


        try{
            $user = User::findOrFail($id);

            $details = RemoteCall::getSpecificWithoutToken(Config::get("config.location_base_url") . "/notification/country/".$user['country_id']);

            if($details['status'] != 200)
            {
                return response()->json([
                    $details['message']
                ],$details['status']);

            }else{
                return response()->json([
                    "status" => "200",
                    "data" => $details['message']['data']
                ],200);
            }
        }catch(ModelNotFoundException $ex)
        {
            Log::error("User Detail for notification", [
                'status' => "404",
                'message' => $ex->getMessage()

            ]);
            return response()->json([
                "status" => "404",
                "message" => "No record"
            ],404);
        }
        catch(\Exception $ex)
        {
            Log::error("User Detail for notification", [
                'status' => "500",
                'message' => $ex->getMessage()

            ]);
            return response()->json([
                "status" => "500",
                "message" => $ex->getMessage()
            ],500);
        }


    }

}
