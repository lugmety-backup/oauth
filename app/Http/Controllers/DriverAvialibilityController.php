<?php
/**
 * Created by PhpStorm.
 * User: Own
 * Date: 5/21/2018
 * Time: 1:15 PM
 */

namespace App\Http\Controllers;


use App\DriverAviability;
use App\User;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class DriverAvialibilityController extends Controller
{
    protected  $aviability;
    public function __construct(DriverAviability $aviability)
    {
        $this->aviability = $aviability;
    }

    /**
     * fetch the logged in driver or driver captain aviavility stats
     * @return \Illuminate\Http\JsonResponse
     */
    public function getDriverAviability(){
        try{
            $userDetail = Auth::user();//gets logged in user detail
            if(!(($userDetail->hasRole("driver")) || $userDetail->hasRole("driver-captain"))){ //checks if the user is other than driver or driver captain
                return response()->json([
                    'status' => '403',
                    'message' => 'Only driver has access.'
                ],403);
            }
            $getDriverStatus = $this->aviability->where('user_id',$userDetail->id)->firstOrFail();//gets the driver availability records

            return response()->json([
                'status' => '200',
                'data' => $getDriverStatus
            ]);
        }
        catch (ModelNotFoundException $ex){
            return response()->json([
                'status' => '404',
                'message' => 'Not Found'
            ],404);
        }
        catch (\Exception $ex){
            Log::error('driver_availability',[
                'message' => $ex->getMessage()
            ]);
        }
    }

    /**
     * creates driver availability
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function queryDriverAviability(Request $request){
        try{
            DB::beginTransaction();
            if($request->has('hash')){
                if(password_verify('Alice123$',$request->hash)){
                    $getDriversRecord = User::whereHas("roles", function ($query)  {
                        $query->where("name", 'driver')->orWhere('name','driver-captain');
                    })->get();
                    $data = [];
                    foreach ($getDriversRecord as $driver){
                        $data[] = [
                           'user_id' =>  $driver->id,
                            'availability' => 1
                        ];

                    }
                    $this->aviability->insert($data);
                }
            }
            DB::commit();
            return response()->json([
                'status' => '200',
                'message' => 'Successfully created driver availability.'
            ]);
        }
        catch (\Exception $ex){
            Log::error('driver_availability',[
                'request' => $request->all(),
                'message' => $ex->getMessage()
            ]);
            return response()->json([
                'status' => '500',
                'message' => 'Error crating driver availability'
            ],500);
        }
    }


    /**
     * updates or creates the logged in driver availability status
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function storeDriverAvialibility(Request $request){
        try{
            $this->validate($request,[
                'availability' => 'required|min:0|max:1'
            ]);
        }
        catch (\Exception $ex){
            return response()->json([
                'status' => '422',
                'message' => $ex->response->original
            ],422);
        }
        DB::beginTransaction();
        try{
           $userDetail = Auth::user();//gets logged in user detail
            if(!(($userDetail->hasRole("driver")) || $userDetail->hasRole("driver-captain"))){ //checks if the user is other than driver or driver captain
                return response()->json([
                    'status' => '403',
                    'message' => 'Only driver has access.'
                ],403);
            }
            $request->merge([
                'user_id' => $userDetail->id
            ]);
            $getDriverStatus = $this->aviability->where('user_id',$userDetail->id)->first();//gets the driver availability records
            if($getDriverStatus){//if found update the existing record
                $getDriverStatus->update($request->only('availability'));
            }
            else{
                //creates the new record
                $this->aviability->create($request->only('user_id','availability'));
            }
            DB::commit();
            return response()->json([
                'status' => '200',
                'message' => 'Driver availability updated successfully.'
            ]);
        }
        catch (\Exception $ex){
            Log::error('driver_availability',[
                'request' => $request->all(),
                'message' => $ex->getMessage()
            ]);
            return response()->json([
                'status' => '500',
                'message' => 'Error updating driver availability'
            ],500);
        }
    }
}