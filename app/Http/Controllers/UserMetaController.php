<?php

namespace App\Http\Controllers;

use App\User;
use App\UserMeta;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;

class UserMetaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($user_id)
    {
        try {
            $user = User::findOrFail($user_id);
        } catch (ModelNotFoundException $ex) {
            return response()->json([
                'status' => '404',
                'message' => "User Not found"
            ], 404);
        }
        try {
            $meta = $user->userMeta()->get();
            if(count($meta) == 0){
                throw new \Exception();
            }
        } catch (\Exception $ex) {
            return response()->json([
                'status' => '404',
                'message' => "User Meta not found"
            ], 404);
        }
        return response()->json([
            'status' => '200',
            'data' => $meta
        ], 200);

    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request , $user_id)
    {
        try {
            $users = User::findOrFail($user_id);
        } catch (ModelNotFoundException $ex) {
            return response()->json([
                'status' => '404',
                'message' => "User Not found"
            ], 404);
        }

        $rules = [
            'meta' => 'required|array'
        ];
        try {
            $this->validate($request, $rules);
        } catch (\Exception $e) {
            return response()->json([
                'status' => '422',
                'message' => $e->response->original
            ], 422);
        }

      //  DB::beginTransaction();
        $rules = [
            'meta_key' => 'required',
            'meta_value' => 'required'
        ];
        foreach ($request['meta'] as $key => $meta){
            if(!is_array($meta)){
                return response()->json([
                    'status' => '403',
                    'message' => "Meta must contain the object with meta_key and meta_value"
                ], 403);
            }
            $validator = Validator::make($meta, $rules);
            if ($validator->fails()) {
                $error = $validator->errors();
                return response()->json([
                    'status' => '422',
                    "message" => [$key => $error]
                ],422);
            }
        }
        foreach ($request['meta'] as $key => $meta){
            $meta['user_id']=$user_id;
            $usermeta =UserMeta::where([['user_id',$user_id],['meta_key',$meta['meta_key']]])->first();
            if($usermeta == null){
                UserMeta::create($meta);
            }else{
                $usermeta->update($meta);
            }
        }

        return response()->json([
            'status' => '200',
            "message" => "User Meta Updated Successfully"
        ],200);

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function selfUpdate(Request $request)
    {
        try {
            $users = User::findOrFail(Auth::id());
        } catch (ModelNotFoundException $ex) {
            return response()->json([
                'status' => '404',
                'message' => "User Not found"
            ], 404);
        }

        $rules = [
            'meta' => 'required|array'
        ];
        try {
            $this->validate($request, $rules);
        } catch (\Exception $e) {
            return response()->json([
                'status' => '422',
                'message' => $e->response->original
            ], 422);
        }

        //  DB::beginTransaction();
        $rules = [
            'meta_key' => 'required',
            'meta_value' => 'required'
        ];
        foreach ($request['meta'] as $key => $meta){
            if(!is_array($meta)){
                return response()->json([
                    'status' => '403',
                    'message' => "Meta must contain the object with meta_key and meta_value"
                ], 403);
            }
            $validator = Validator::make($meta, $rules);
            if ($validator->fails()) {
                $error = $validator->errors();
                return response()->json([
                    'status' => '422',
                    "message" => [$key => $error]
                ],422);
            }
        }
        foreach ($request['meta'] as $key => $meta){
            $meta['user_id']=$users->id;
            $usermeta =UserMeta::where([['user_id',$meta['user_id']],['meta_key',$meta['meta_key']]])->first();
            if($usermeta == null){
                UserMeta::create($meta);
            }else{
                $usermeta->update($meta);
            }
        }

        return response()->json([
            'status' => '200',
            "message" => "User Meta Updated Successfully"
        ],200);

    }


}
