<?php
/**
 * Created by PhpStorm.
 * User: hazesoft_one
 * Date: 10/5/17
 * Time: 11:18 AM
 */

namespace App\Http\Controllers;


use App\Role;
use App\UserMeta;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use App\User;
use Illuminate\Support\Facades\Request;
use Mockery\CountValidator\Exception;


class ExportController extends Controller
{
    private $db_ext;

    public function __construct()
    {
        $this->db_ext = DB::connection('mysql_external');
    }


    public function exportUser()
    {
        try {


            $oldUser = $this->db_ext->table('users')->where('user_role_id', 3)->get();

            DB::beginTransaction();
            $i = 0;
            foreach ($oldUser as $user) {
                unset($insert);
                if (empty($user->mobile) || empty($user->birthdate)) {
                    continue;
                }
                $addresses = $this->db_ext->table('user_customer_billing_addresses')->where('customer_id', $user->id)->get();
                if (count($addresses) > 0) {

                    foreach ($addresses as $address) {
                        $city = $this->db_ext->table('districts')->join('cities', 'districts.city_id', '=', 'cities.id')->where('districts.id', $address->district_id)->pluck('cities.name');

                        if (count($city) > 0) {
                            $addressLines = unserialize($address->address);
                            $insert['city'] = $city[0];
                            $insert['address_line_1'] = empty($addressLines['address_line1']) ? '' : $addressLines['address_line1'];
                            $insert['address_line_2'] = empty($addressLines['address_line2']) ? '' : $addressLines['address_line2'];

                        }
                    }
                }


                $insert['id'] = $user->id;
                $insert['first_name'] = $user->first_name;
                $insert['last_name'] = $user->last_name;
                $insert['email'] = $user->email;
                $insert['password'] = $user->password;
                if (!empty($user->gender)) {
                    $insert['gender'] = ($user->gender == 'F') ? 'female' : 'male';
                } else {
                    $insert['gender'] = 'male';
                }

                $insert['mobile'] = !empty($user->mobile) ? $user->mobile : '123';
                $insert['birthday'] = $user->birthdate;
                $insert['status'] = $user->status;
                $insert['facebook_id'] = empty($user->facebook_id) ? null : $user->facebook_id;
                $insert['google_id'] = empty($user->google_id) ? null : $user->google_id;
                $insert['country'] = 'Saudi Arabia';
                $insert['country_code'] = '+966';
                $insert['created_at'] = $user->created_at;
                $insert['updated_at'] = $user->updated_at;
                DB::table('users')->insert($insert);
                DB::table('role_user')->insert([
                    'user_id' => $insert['id'],
                    'role_id' => 4

                ]);

                DB::table('user_meta')->insert(
                    [
                        [
                            'user_id' => $user->id,
                            'meta_key' => 'notification',
                            'meta_value' => 0,
                        ],
                        [
                            'user_id' => $user->id,
                            'meta_key' => 'is_subscribed',
                            'meta_value' => 0,
                        ]
                    ]
                );

            }
            DB::commit();

            return response()->json([

                'message' => 'data successfully migrated',
                'status' => '200'

            ], 200);
        } catch (\Exception $exception) {
            return response()->json([
                'message' => $exception->getMessage(),
                'status' => '400'
            ], 400);
        }

    }

    public function exportAdmin()
    {
        try {
            $oldUser = $this->db_ext->table('users')->whereIn('user_role_id', [1, 2])->get();
            // return $oldUser;

            DB::beginTransaction();

            foreach ($oldUser as $user) {
                unset($insert);
                if (empty($user->mobile) || empty($user->birthdate)) {
                    continue;
                }
                $addresses = $this->db_ext->table('user_customer_billing_addresses')->where('customer_id', $user->id)->get();
                if (count($addresses) > 0) {

                    foreach ($addresses as $address) {
                        $city = $this->db_ext->table('districts')->join('cities', 'districts.city_id', '=', 'cities.id')->where('districts.id', $address->district_id)->pluck('cities.name');

                        if (count($city) > 0) {
                            $addressLines = unserialize($address->address);
                            $insert['city'] = $city[0];
                            $insert['address_line_1'] = empty($addressLines['address_line1']) ? '' : $addressLines['address_line1'];
                            $insert['address_line_2'] = empty($addressLines['address_line2']) ? '' : $addressLines['address_line2'];

                        }
                    }
                }


                // $insert['id'] = $user->id;
                $insert['first_name'] = $user->first_name;
                $insert['last_name'] = $user->last_name;
                $insert['email'] = $user->email;
                $insert['password'] = $user->password;
                if (!empty($user->gender)) {
                    $insert['gender'] = ($user->gender == 'F') ? 'female' : 'male';
                } else {
                    $insert['gender'] = 'male';
                }

                $insert['mobile'] = !empty($user->mobile) ? $user->mobile : '123';
                $insert['birthday'] = $user->birthdate;
                $insert['status'] = $user->status;
                $insert['facebook_id'] = empty($user->facebook_id) ? null : $user->facebook_id;
                $insert['google_id'] = empty($user->google_id) ? null : $user->google_id;
                $insert['country'] = 'Saudi Arabia';
                $insert['country_code'] = '+966';

                $insert['created_at'] = $user->created_at;
                $insert['updated_at'] = $user->updated_at;
                $id = DB::table('users')->insertGetId($insert);

                $role_id = ($user->user_role_id == 1) ? 2 : 1;

                DB::table('role_user')->insert([
                    'user_id' => $id,
                    'role_id' => $role_id

                ]);


            }
            DB::commit();

            return response()->json([

                'message' => 'data successfully migrated',
                'status' => '200'

            ], 200);
        } catch (\Exception $exception) {
            return response()->json(['message' => $exception->getMessage(),
                'status' => '400'], 400);
        }

    }

    public function exportDriver(\Illuminate\Http\Request $request)
    {
        try {

            $fileD = fopen($request->file, "r");
            $column = fgetcsv($fileD);
            while (!feof($fileD)) {
                $rowData[] = fgetcsv($fileD);
            }
            DB::beginTransaction();
            foreach ($rowData as $key => $value) {
                try {

                    $insert = [];
                    $insert['first_name'] = $value[9];
                    $insert['middle_name'] = '';
                    $insert['last_name'] = '';
                    $insert['email'] = $value[13];
                    $insert['password'] = app('hash')->make($value[3]);

                    $insert['mobile'] = !empty($value[13]) ? $value[13] : '123';
                    $insert['birthday'] =  explode(" ", Carbon::parse($value[16]))[0];
                    //echo $value[16];


                    $insert['status'] = ($value[19] == "Approved") ? 1 : 0;
                    $insert['facebook_id'] = null;
                    $insert['google_id'] = null;
                    $insert['country'] = 'Saudi Arabia';
                    $insert['country_code'] = '+966';
                    $insert['city'] = $value[14];
                    $insert['created_at'] = $value[18];
                    $insert['updated_at'] = $value[24];

                    $id = DB::table('users')->insertGetId($insert);
                    DB::table('role_user')->insert([
                        'user_id' => $id,
                        'role_id' => 6

                    ]);

                } catch
                (\Exception $ex) {
                    continue;
                }
//                    return response()->json([
//
//                        'message' => $ex->getMessage(),
//                        'status' => '400'
//
//                    ], 400);
//                }
            }

            DB::commit();
        } catch (\Exception $ex) {
            return response()->json([

                'message' => $ex->getMessage(),
                'status' => '400'

            ], 400);
        }
        return response()->json([

            'message' => 'data successfully migrated',
            'status' => '200'

        ], 200);

    }

//#ece4d6
}

