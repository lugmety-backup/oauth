<?php

namespace App\Http\Controllers;
use App\Permission;
use App\Role;
use Cocur\Slugify\Slugify;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Database\QueryException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Illuminate\Support\Facades\DB;
class RoleController extends Controller
{
    private $role;
    private $slugify;
    /**
     * RoleController constructor.
     * @param $role
     */
    public function __construct(Role $role,Slugify $slugify)
    {
        $this->role=$role;
        $this->slugify=$slugify;
    }

    /**
     * Display all the roles present in database table named role
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        try{
            $roles = $this->role->all();//Select id, name from role table
            foreach ($roles as $role){
                $role["permissions"] = $role->permissions;
            }
            return \Datatables::of($roles)->make(true);
//            return response()->json([
//                'status'=>'200',
//                'data'=>$roles
//            ]);
        }
        catch (QueryException $e){

            return response()->json([
                'status'=>'404',
                'message'=>'Roles Could not be found'
            ],404);
        }
        catch (\Exception $e){
            return response()->json([
                'status'=>'404',
                'message'=>'Roles Could not be found'
            ],404);
        }

    }

    public function getChildRole(Request $request){
        $user = Auth::user();
        if($user->hasRole("restaurant-admin") || $user->hasRole("branch-admin") || $user->hasRole("admin") || $user->hasRole("super-admin")){


        $parentRole = $request->get('parent_role');
        if($request->has('parent_role')){
            $childRoles = $this->role->where("parent_role",$parentRole)->get();
            return response()->json([
                "status" => "200",
                "data" => $childRoles
            ]);
        }
        else{
            return response()->json([
                "status" => "200",
                "data" => []
            ]);
        }
        }
        else{
            return response()->json([
                "status" => "403",
                "message" => "Forbidden"
            ],403);
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store the role into database table named role along with passed array of permission
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try{
            /*
             * Validation of request
             * */
            DB::beginTransaction();
            $this->validate($request, [
                'name' => 'required',
                "permissions" =>"required|array"//added
            ]);
            $request['name']=$this->slugify->slugify($request['name'],['regexp' => '/([^A-Za-z]|-)+/']);
            if(empty($request['name'])){
                return response()->json([
                    "status"=>"422",
                    "message"=>["name"=>["The name may not only contain numbers,special characters."]]
                ],422);
            }
            /*
             * Validation of parent role field
             * */
            try {
                $permissions = Permission::select("id")->findOrFail($request["permissions"]);
            }

            catch (\Exception $ex){
                return response()->json([
                    "status" => "404",
                    "message" => "Permission(s) does not exist"
                ],404);
            }
            if($request['parent_role']!=null){
                if($request['parent_role']!='restaurant-admin' && $request['parent_role']!='branch-admin'){
                    return response()->json([
                        "status"=>"422",
                        "message"=>["parent_role"=>["The Parent Role must be either restaurant-admin or branch-admin"]]
                    ],422);
                }
            }
            $role= $this->role->create($request->all());
            try {
                $role->attachPermissions($permissions);
                DB::commit();
                return response()->json([
                    'status'=>'200',
                    'message'=>'Role created successfully'
                ],200);
            }
            catch (\Exception $ex){
                return response()->json([
                    'status' => '409',
                    'message' => "Permission could not be attached to role due to duplicate entry"
                ],409);
            }
        }
        catch (QueryException $ex){
            return response()->json([
                'status'=>'409',
                'message'=>"Role could not be created due to duplicate entry"
            ],409);
        }
        catch (\Exception $e){
            return response()->json([
                'status'=>'422',
                'message'=>$e->response->original
            ],422);
        }
    }

    /**
     * Display the specified role
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        try{
            $role= $this->role->findOrFail($id);
            $permission = $role->permissions;
            return response()->json([
                'status'=>'200',
                'data'=>$role
            ],200);
        }
        catch (QueryException $ex){
            return response()->json([
                'status'=>'404',
                'message'=>"Requested Role could not be found"
            ],404);
        }
        catch (ModelNotFoundException  $ex){
            return response()->json([
                'status'=>'404',
                'message'=>"Requested Role could not be found"
            ],404);
        }
        catch (\Exception $ex){
            return response()->json([
                'status'=>'404',
                'message'=>"Requested Role could not be found"
            ],404);
        }

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified role in role table.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        try{
            /*
             * Validation of parent role field
             * */
            if($request['parent_role']!=null){
                if($request['parent_role'] != 'restaurant-admin' && $request['parent_role'] != 'branch-admin'){
                    return response()->json([
                        "status"=>"422",
                        "message"=>["parent_role"=>"The Parent Role must be either restaurant-admin or branch-admin"]
                    ],422);
                }
            }
            DB::beginTransaction();
            try {
                $this->validate($request, [
                    "permissions" => "required|array"//added
                ]);
            }
            catch (\Exception $ex){
                return response()->json([
                    "status" => "422",
                    "message" => $ex->response->original
                ],422);
            }

            $role = $this->role->findOrFail($id);
            $updateRole = $role->update($request->except('name'));
            $listOfPermission = $role->permissions;
            $detachPermissions = $role->detachPermissions($listOfPermission);
            try {
                $permissions = Permission::select("id")->findOrFail($request["permissions"]);
            }

            catch (\Exception $ex){
                return response()->json([
                    "status" => "404",
                    "message" => "Permission(s) does not exist"
                ],404);
            }
            try {
                $role->attachPermissions($permissions);
                DB::commit();
                return response()->json([
                    'status'=>'200',
                    'message'=>'Role Updated successfully',
                ]);
            }
            catch (\Exception $ex){
                return response()->json([
                    'status' => '409',
                    'message' => "Permission could not be attached to role due to duplicate entry"
                ],409);
            }


        }
        catch (QueryException $ex){
            return response()->json([
                'status'=>'409',
                'message'=>"Role could not be created due to duplicate entry"
            ],409);
        }

        catch (ModelNotFoundException  $ex){
            return response()->json([
                'status'=>'404',
                'message'=>"Requested Role could not be found"
            ],404);
        }
        catch (\Exception $e){
            return response()->json([
                'status'=>'422',
                'message'=>$e->response->original
            ],422);
        }
    }

    /**
     * Remove the specified role from role table.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {

        try{
            /**
            since entrust Role::findOrFail($id)->delete() throws errors .so line 2208 is used to find the id if not exception is thrown
             * if it finds then line 209 is executed which delete the role
             * */
            $role= Role::findOrFail($id);
            /**
            check if the requested role name matched with the array of role that is fixed
             * this is to prevent everyone from deleting fixed role
             */
            $checkRole=in_array($role->name,["admin","super-admin","restaurant-admin","branch-admin","customer","driver"]);
            try{
                if($checkRole){
                    throw new \Exception();
                }
            }
            catch (\Exception $ex){
                return response()->json([
                    "status"=>"403",
                    "message"=>"Requested Role is not allowed to delete"
                ]);
            }
            $role=$this->role->whereId($id)->delete();
            return response()->json([
                'status'=>'200',
                'message'=>"Role deleted successfully"
            ],200);
        }
        catch (QueryException $ex){
            return response()->json([
                'status'=>'404',
                'message'=>"Role could not be found"
            ],404);
        }
        catch (ModelNotFoundException  $ex){
            return response()->json([
                'status'=>'404',
                'message'=>"Requested Role could not be found"
            ],404);
        }
        catch (\Exception $ex){
            return response()->json([
                'status'=>'404',
                'message'=>"Requested Role could not be found"
            ],404);
        }
    }
}
