<?php
/**
 * Created by PhpStorm.
 * User: ashish
 * Date: 5/16/2017
 * Time: 5:01 PM
 */

namespace App\Http\Controllers;

use App\Events\UserEvent;
use App\NonLoggedInUserToken;
use App\PasswordReset;
use App\Tokens;
use App\User;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Log;
use Lcobucci\JWT\Parser;
Use Carbon\Carbon;
use phpDocumentor\Reflection\Types\Integer;
use Settings;

/**
 * Class AuthController
 * @package App\Http\Controllers
 */
class AuthController extends Controller
{
    /**
     * @var
     */
    private $client;
    private $token;

    /**
     * AuthController constructor.
     *
     */
    public function __construct(Tokens $token)
    {
        $this->token = $token;
    }

    /**
     * Calls oauth/token route by attaching client_id ,client_secret,grant_type
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function login(Request $request)
    {

        try {
            $this->validate($request, [
                "username" => "required|email",
                "password" => "required",
                "device_type" => "required|in:web,android,ios"
            ]);
        } catch (\Exception $ex) {
            return response()->json([
                "status" => "422",
                "message" => $ex->response->original
            ], 422);
        }
        $lang = Input::get('lang', 'en');
        DB::beginTransaction();
        try {
            /*
             * checking user exist by searching through email
             * */

            $user = User::where([["email", $request["username"]]])->firstOrFail();
            if ($user["status"] == 0) {
                return response()->json([
                    "status" => "403",
                    "message" => "User is inactive.",
                    "mobile_number" => $user->mobile,
                    "country_code" => $user->country_code
                ], 403);
            }

            $userId = $user->id;
            /**
             * if optional parameter is customer which means its for customer login only so admin and other wont login
             */
            if (isset($request["customer"])) {
                if (!$user->hasRole("customer") && !$user->hasRole("restaurant-customer")) {
                    return \Settings::getErrorMessageOrDefaultMessage('oauth-only-customer-is-allowed-to-login',
                        "Only Customer is allowed to login.", 403, $lang);
//                    return response()->json([
//                        "status" => "403",
//                        "message" => "Only Customer is allowed to login"
//                    ], 403);
                }
            } elseif (isset($request["driver"])) {
                if (!(($user->hasRole("driver")) || $user->hasRole("driver-captain"))) {
                    return \Settings::getErrorMessageOrDefaultMessage('oauth-only-driver-is-allowed-to-login',
                        "Only Driver is allowed to login.", 403, $lang);
//                    return response()->json([
//                        "status" => "403",
//                        "message" => "Only Driver is allowed to login"
//                    ], 403);
                }
            }
            $clientInfo = DB::table("oauth_clients")->where("name", $request->device_type)->first();
            if (!$clientInfo) {
                return response()->json([
                    "status" => "404",
                    "message" => "Client could not be found."
                ], 404);
            }

            $client_id = $clientInfo->id;
            $client_secret = $clientInfo->secret;
            $proxy = Request::create(
                '/oauth/token',
                'post',
                [
                    'grant_type' => "password",
                    'client_id' => $client_id,
                    'client_secret' => $client_secret,
                    'username' => $request["username"],
                    'password' => $request["password"]
                ]
            );
            $data = App::dispatch($proxy);
            if ($data->getStatusCode() == 401) {
//                return response()->json([
//                    "status" => "401",
//                    "message" => "The user credentials were incorrect."
//                ], 401);
                return \Settings::getErrorMessageOrDefaultMessage('oauth-the-user-credentials-are-incorrect',
                    "The user credentials were incorrect.", 401, $lang);
            }
            $user->update(["ip_address" => $request->ip()]);//update ip address of user
            if (!isset($request["token"]) || is_null($request["token"])) goto skipUpdatingToken;
            $testToken = $this->token->where([["user_id", $userId], ["device_type", $request["device_type"]]])->delete();//checks if the token exist or not
            $testToken = $this->token->where([["token", $request['token']], ["device_type", $request["device_type"]]])->delete();//checks if the token exist or not
//            if(count($testToken) !== 0) $testToken[0]->delete(); //if token is present then delete the token
            $this->token->create(["user_id" => $userId, "token" => $request["token"], "device_type" => $request["device_type"]]); //else create new token
            skipUpdatingToken:
            DB::commit();
            return $data;
        } catch (ModelNotFoundException $ex) {
//            return response()->json([
//                "status" => "404",
//                "message" => "User could not be found. It may be due to user is not registered."
//            ], 404);
            return \Settings::getErrorMessageOrDefaultMessage('oauth-user-could-not-be-found-it-may-be-due-to-user-not-registered',
                "User could not be found. It may be due to user is not registered.", 404, $lang);
        } catch (\Exception $ex) {
            return response()->json([
                "status" => "500",
                "message" => "There was some problem logging user"
            ], 500);
        }

    }

    /**
     * logout user
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function logout(Request $request)
    {
        try {
            $this->validate($request, [
                "device_type" => "required|in:web,android,ios"
            ]);
        } catch (\Exception $ex) {
            return response()->json([
                "status" => "422",
                "message" => $ex->response->original
            ], 422);
        }
        $lang = Input::get('lang', 'en');
        try {
            $userInfo = Auth::user();
            $testToken = $this->token->where([["user_id", $userInfo->id], ["device_type", $request["device_type"]]])->delete();
            //revoke access_token
            $userInfo->token()->revoke();
            return \Settings::getErrorMessageOrDefaultMessage('oauth-you-have-been-successfully-logged-out',
                "You have been successfully logged out.", 200, $lang);
//        return response()->json([
//            "status" => "200",
//            "message" => "You have been successfully logged out."
//        ]);
        } catch (\Exception $ex) {
            return response()->json([
                "status" => "500",
                "message" => $ex->getMessage()
            ], 500);
        }
    }
    /**
     * Calls oauth/token route by attaching client_id ,client_secret,grant_type for refreshing token
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
//    public function refreshToken(Request $request){
//
//        try{
//            $this->validate($request,[
//                "access_token"=>"required",
////                "refresh_token"=>"required"
//            ]);}
//        catch (\Exception $ex){
//            return response()->json([
//                "status"=>"422",
//                "message"=>$ex->response->original
//            ],422);
//        }
//    $value= $request["access_token"];
//   $id= (new Parser())->parse($value)->getHeader('jti');
//    $data=  DB::table('oauth_refresh_tokens')
//        ->where('access_token_id', '=', $id)->first();
//$refresh_token =  $data->id;
//return $refresh_token;
//    $currentdate=  Carbon::now();
//    $expiresAt=Carbon::parse($data->expires_at);
//    if($currentdate->gte($expiresAt)){
//        dd("expired");
//
//    }
//    else{
//        dd("not expired");
//    }
//}

    public function confirmCode(Request $request)
    {
        $lang = Input::get('lang', 'en');
        $request['confirmation_code'] = (int)$request['confirmation_code'];
        try {
            try {
                $this->validate($request, [
                    "mobile" => "required",
                    "confirmation_code" => "required|integer|digits:4"
                ]);
            } catch (\Exception $ex) {
                return response()->json([
                    "status" => "422",
                    "message" => $ex->response->original
                ], 422);
            }
            $checkUser = User::where([
                ["confirmation_code", $request->confirmation_code],
                ["mobile", $request->mobile]
            ])->first();
            if ($checkUser) {
                $checkUser->update(["status" => 1, "confirmation_code" => null]);
                $array = [
                    'service' => 'oauth service',
                    'message' => 'customer verification',
                    'data' => [
                        "user_details" => $checkUser->toArray(),
                    ]
                ];
                event(new UserEvent($array));
                return \Settings::getErrorMessageOrDefaultMessage('oauth-user-activated-successfully',
                    "User activated successfully.", 200, $lang);
//                return response()->json([
//                    "status" => "200",
//                    "message" => "User activated successfully."
//                ]);
            } else {
                return \Settings::getErrorMessageOrDefaultMessage('oauth-invalid-confirmation-code',
                    "Invalid confirmation code.", 403, $lang);
//                return response()->json([
//                    "status" => "403",
//                    "message" => "Invalid confirmation code."
//                ], 403);
            }
        } catch (\Exception $ex) {
            return \Settings::getErrorMessageOrDefaultMessage('oauth-error-in-activation-please-try-again-later',
                "Error in activation. Please try again later.", 500, $lang);
//            return response()->json([
//                "status" => "500",
//                "message" => "Error in activation. Please try again later."
//            ], 500);
        }
    }

    public function newPassword(Request $request)
    {
        try {
            $this->validate($request, [
                "reset_code" => "required|max:255",
                "password" => "required|max:255"
            ]);
        } catch (\Exception $ex) {
            return response()->json([
                "status" => "422",
                "message" => $ex->response->original
            ], 422);
        }
        $lang = Input::get('lang', 'en');
        try {
            $checkUserPresentInPasswordReset = PasswordReset::where("token", $request->reset_code)->firstOrFail();
            $currentDate = Carbon::now('utc');
            $expireAt = Carbon::parse($checkUserPresentInPasswordReset->token_created_at)->addDay();
            if ($currentDate->gt($expireAt)) {
                $checkUserPresentInPasswordReset->delete();
                return \Settings::getErrorMessageOrDefaultMessage('oauth-the-provider-reset-code-has-expired',
                    "The provided reset code has expired. Please try again resetting password with new reset code.", 410, $lang);
//                return response()->json([
//                    "status" => "410",
//                    "message" => "The provided reset_code has expired. Please try again resetting password with new reset_code."
//                ], 410);
            }
        } catch (\Exception $ex) {
            return response()->json([
                "status" => "403",
                "message" => "Invalid reset_code"
            ], 403);
        }
        try {
            $getUser = User::where([
                ["email", $checkUserPresentInPasswordReset->email],
                ["status", 1]
            ])->firstOrFail();
            $checkUserPresentInPasswordReset->delete();
            $updatePassword = $getUser->update(["password" => $request->password]);
            return \Settings::getErrorMessageOrDefaultMessage('oauth-password-reset-successfully',
                "Password reset successfully.", 200, $lang);
//            return response()->json([
//                "status" => "200",
//                "message" => "Password reset successfully"
//            ]);
        } catch (ModelNotFoundException $ex) {
            return \Settings::getErrorMessageOrDefaultMessage('oauth-user-could-not-be-found-it-may-be-due-to-user-inactive',
                "User could not be found. It may be due to user is inactive or user is not registered.", 404, $lang);
//            return response()->json([
//                "status" => "404",
//                "message" => "User could not be found. It may be due to user is inactive or user is not registered."
//            ], 404);
        } catch (\Exception $ex) {
            return response()->json([
                "status" => "500",
                "message" => "Error in resetting  password"
            ], 500);
        }
    }

    /**
     * Stores new FCM token based on device and user type
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function storeNonLoggedInDeviceToken(Request $request){
        DB::beginTransaction();
        try{
            $message = [
                'device_type.in' => 'The device type must be android or ios.',
                'user_type.in' => 'The user type must be customer or driver.'
            ];
            $this->validate($request,[
                'device_type' => 'required|in:android,ios',
                'user_type' => 'required|in:customer,driver',
                'token' => 'required|string',
                'country_id' => 'required|integer|min:1'
            ],$message);

        }
        catch (\Exception $ex){
            return response()->json([
                'status' => '422',
                'message' => $ex->response->original
            ],422);
        }
        try{
            //check if the existing record exist
            $getExistingToken = NonLoggedInUserToken::where([
                ['user_type',$request->user_type],
                ['device_type',$request->device_type],
                ['token',$request->token]
            ])->first();

            if(!$getExistingToken){
                $createToken =   NonLoggedInUserToken::create($request->all());
            }
            else{
                $getExistingToken->update($request->all());
            }
            DB::commit();
            return response()->json([
                'status' => '200',
                'message' => 'Token successfully created.'
            ]);
        }
        catch (\Exception $ex){
            Log::error("error creating non logged in user token",[
                'request' => $request->all(),
                'message' => $ex->getMessage()
            ]);
            return response()->json([
                'status' => '200',
                'message' => 'Token successfully created.'
            ]);
        }
    }

    /**
     * gets new FCM token based on device type and user type
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function getNonLoggedInUserToken(Request $request){
        DB::beginTransaction();
        try{
            $message = [
                'device_type.in' => 'The device type must be android or ios.',
                'user_type.in' => 'The user type must be customer or driver.'
            ];
            $this->validate($request,[
                'device_type' => 'required|in:android,ios',
                'user_type' => 'required|in:customer,driver',
                'country_id' => 'required|integer|min:1'
            ],$message);

        }
        catch (\Exception $ex){
            return response()->json([
                'status' => '422',
                'message' => $ex->response->original
            ],422);
        }
        try{
            $getExistingToken = NonLoggedInUserToken::where([
                ['user_type',$request->user_type],
                ['device_type',$request->device_type],
                ['country_id',$request->country_id]
            ])->pluck('token')->values()->all();
            return response()->json([
                'status' => '200',
                'data' => [
                    'tokens' =>$getExistingToken
                ]
            ]);
        }
        catch (\Exception $ex){
            return response()->json([
                'status' => '500',
                'message' => 'Error listing tokens'
            ],500);
        }
    }

}