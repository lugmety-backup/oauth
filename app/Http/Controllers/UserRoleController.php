<?php
/**
 * Created by PhpStorm.
 * User: ashish
 * Date: 3/31/2017
 * Time: 4:30 PM
 */

namespace App\Http\Controllers;

use App\User;
use App\Role;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Database\QueryException;
use Illuminate\Http\Request;
use Symfony\Component\HttpKernel\Exception\MethodNotAllowedHttpException;

class UserRoleController extends Controller
{
    private $user;
    private $role;

    /**
     * RolePermissionController constructor.
     * @param $permission
     */
    public function __construct(User $user,Role $role)
    {
        $this->role = $role;
        $this->user = $user;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($id)
    {
        try {
            $user = $this->user->findOrFail($id);//Select a user from users table based on id
            $role = $user->roles;
            if($role->count()==0){
                throw new \Exception();
            }
            return response()->json([
                'status' => '200',
                'data' => $role
            ],200);
        }
        catch (QueryException $ex){
            return response()->json([
                'status'=>'404',
                'message'=>"Role could not be found"
            ],404);
        }
        catch (ModelNotFoundException $ex){
            return response()->json([
                'status'=>'404',
                'message'=>"Requested User  could not be found"
            ],404);
        }
        catch (\Exception $ex){
            return response()->json([
                'status'=>'404',
                'message'=>"Requested User Roles could not be found"
            ],404);
        }
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request,$id)
    {
        try{
            $user = $this->user->findOrFail($id);//Select * from permission table based on id
        }
        catch (ModelNotFoundException $ex) {
            return response()->json([
                'status' => '404',
                'message' => "Requested User  could not be found"
            ], 404);
        }
        /*
         * Validation of Request
         * */

        try{
            $this->validate($request, [
                'role' => 'required|alpha-dash'
            ]);
        }
        catch (\Exception $ex){
            return response()->json([
                'status'=>'422',
                'message'=>$ex->response->original
            ],422);
        }

        /*
         * Check if User Already has role.
         * */
        try{
           $roles = $user->roles;
           if(count($roles)!=0){
               throw new \Exception();
           }
        }
        catch (\Exception $ex){
            return response()->json([
                'status' => '403',
                'message' => "User already has role"
            ], 403);
        }
        try{
            $role = $this->role->where('name',strtolower($request['role']))->get();
            if($role->count()==0)
            {
                throw new \Exception();
            }
            $user->attachRole($role[0]);
            return response()->json([
                'status' => '200',
                'message' => 'Role is successfully assigned to User'
            ],200);
        }
        catch (QueryException $ex){
            return response()->json([
                'status' => '409',
                'message' => "Role could not be attached to user due to duplicate entry"
            ], 409);
        }
        catch (\Exception $ex){
            return response()->json([
                'status' => '404',
                'message' => "Submitted Role Name could not be found"
            ], 404);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id, Request $request)
    {
        try {
            $user = $this->user->findOrFail($id);//Select * from permission table based on id

            $this->validate($request, [
                'role' => 'required|alpha-dash'
            ]);
            try {
                $role = $this->role->where('name', strtolower($request['role']))->get();

                if ($role->count() == 0) {
                    throw new \Exception();
                }
                $user->detachRole($role[0]);
                return response()->json([
                    'status' => '200',
                    'message' => 'Role is successfully detached from User'
                ], 200);
                /**
                 * since role in return in array so base [0] is used to get object inside index 0
                 **/
            }
            catch (\Exception $ex) {
                return response()->json([
                    'status' => '404',
                    'message' => "Submitted Role Name could not be found"
                ], 404);
            }
        }
        catch (ModelNotFoundException $ex) {
            return response()->json([
                'status' => '404',
                'message' => "Requested User not be found"
            ], 404);
        }
    }

}