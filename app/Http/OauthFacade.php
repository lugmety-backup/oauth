<?php
/**
 * Created by PhpStorm.
 * User: ashish
 * Date: 4/25/2017
 * Time: 10:51 PM
 */

namespace App\Http;


use Laravel\Passport\Bridge\AccessTokenRepository;
use Laravel\Passport\Passport;

use Lcobucci\JWT\Token;
use Lcobucci\JWT\ValidationData;

class OauthFacade
{
    private $accessTokenRepository;

    /**
     * constructing IntrospectionController
     *
     * @param \Lcobucci\JWT\Parser $jwt
     * @param \League\OAuth2\Server\ResourceServer $resourceServer
     * @param \Laravel\Passport\Bridge\AccessTokenRepository $accessTokenRepository
     */
    public function __construct(

        AccessTokenRepository $accessTokenRepository
    ) {

        $this->accessTokenRepository = $accessTokenRepository;
    }
    public function verifyToken(Token $token): bool
    {
        $signer = new \Lcobucci\JWT\Signer\Rsa\Sha256();
        $publicKey = 'file://' . Passport::keyPath('oauth-public.key');

        try {

            if ( ! $token->verify($signer, $publicKey)) {
                return false;
            }

            $data = new ValidationData();
            $data->setCurrentTime(time());

            if ( ! $token->validate($data)) {
                return false;
            }

            //  is token revoked?
            if ($this->accessTokenRepository->isAccessTokenRevoked($token->getClaim('jti'))) {
                return false;
            }

            return true;
        } catch (\Exception $exception) {
        }

        return false;
    }

//    public function introspectToken($request){
//        try {
//            $this->resourceServer->validateAuthenticatedRequest($request);
//
//            if (array_get($request->getParsedBody(), 'token_type_hint', 'access_token') !== 'access_token') {
//                //  unsupported introspection
//                return $this->notActiveResponse();
//            }
//
//            $accessToken = array_get($request->getParsedBody(), 'token');
//            if ($accessToken === null) {
//                return $this->notActiveResponse();
//            }
//
//            $token = $this->jwt->parse($accessToken);
//
//            if ( !$this->verifyToken($token)) {
//
//                return $this->errorResponse([
//                    'status'=>'401',
//                    'message'=>"Invalid Token"
//                ],401);
//                //error if given token is invalid
//            }
//
//            /** @var string $userModel */
//            $userModel = config('auth.providers.users.model');
//            $user = (new $userModel)->findOrFail($token->getClaim('sub'));
//
//            if ( array_get( $request->getParsedBody(), 'permission', '') !== '' ) {
//                $permission = array_get($request->getParsedBody(), 'permission');
//
//
//                try{
//                    if(!$user->can($permission)){
//                        throw new Exception();
//                    }
//                }
//                catch (Exception $ex){
//                    return response()->json([
//                            'status'=> '403',
//                            "message"=> "Forbidden" ]
//                        ,403 );
//                }
//
//            }
//
//            // get user roles
//            $roles = array();
//
//            if( count( $user->roles ) ) {
//                foreach ($user->roles as $role) {
//                    $roles[] = $role->name;
//                }
//            }
//
//
//            return $this->jsonResponse([
//                'status'        =>'200',
//                'active'        => true,
//                'scope'         => trim(implode(' ', (array)$token->getClaim('scopes', []))),
//                'client_id'     => intval($token->getClaim('aud')),
//                'user_id'       => $user->id,
//                'username'      => $user->email,
//                'token_type'    => 'access_token',
//                'exp'           => intval($token->getClaim('exp')),
//                'roles'         => $roles,
////                'iat' => intval($token->getClaim('iat')),
////                'nbf' => intval($token->getClaim('nbf')),
////                'sub' => intval($token->getClaim('sub')),
////                'aud' => intval($token->getClaim('aud')),
////                'jti' => $token->getClaim('jti'),
//            ], 200 );
//
//        } catch (OAuthServerException $oAuthServerException) {
//
//            return $oAuthServerException->generateHttpResponse(new Psr7Response);
//        }
//        catch ( ModelNotFoundException $ex){
//
//            return response()->json([
//                'status'=>'404',
//                'message'=>"Requested User could not be found"
//            ],404);
//
//        }
//        catch (\Exception $exception) {
//            //this error occurs when the  decoder could not decode token
//            return response()->json([
//                'status'=>'401',
//                'message'=>"Invalid Token"
//            ],401); ;
//        }
//    }
}