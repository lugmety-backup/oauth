<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Facades\Mail;

class CountactUsMail extends Mailable
{
    use Queueable, SerializesModels;
    public $request = [];

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($request = [])
    {
        $this->request = $request;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('emails.welcome')
            ->from("ascc.2023@gmail.com", $this->request['username'])
            ->subject('Tourism User Message');
    }
}

