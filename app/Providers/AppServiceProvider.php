<?php

namespace App\Providers;

use Carbon\Carbon;
use Illuminate\Support\ServiceProvider;
use Validator;
use Illuminate\Support\Facades\Config;
class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function boot(){

        /**
         * takes the google cloud credential
         */
        $array = [
            "type" =>env('STACK_TYPE') ,
            "project_id" => env('STACK_PROJECT_ID'),
            "private_key_id" => env('STACK_PRIVATE_KEY_ID'),
            "private_key" => str_replace('\n', "\n", env('STACK_PRIVATE_KEY')),
            "client_email" => env('STACK_CLIENT_EMAIL'),
            "client_id" => env('STACK_CLIENT_ID'),
            "auth_uri" => env('STACK_AUTH_URI'),
            "token_uri" => env('STACK_TOKEN_URL'),
            "auth_provider_x509_cert_url" => env('STACK_AUTH_PROVIDER_x509_CERT_URL'),
            "client_x509_cert_url" => env('STACK_CLIENT_X509_CERT_URL')
        ];
        /**
         * the path to create and store the json
         */
        $path = storage_path().'/stackdriver.json';
        /**
         * open the file
         */
        $fp = fopen($path, 'w');
        /**
         * store array in json with pretty print
         */
        fwrite($fp, json_encode($array,JSON_PRETTY_PRINT ));
        fclose($fp);

//        date_default_timezone_set('utc');
        Validator::extend('domain_validation', function($attribute, $value, $parameters, $validator) {
            $domains = explode("@",$value);
            if(checkdnsrr($domains[1],"MX")){
                return true;
            }
            else{
                return false;
            }
        });

        Validator::extendImplicit('birthday_validation', function($attribute, $value, $parameters, $validator) {
           $now = Carbon::now();
           $date = Carbon::parse($value);
           $diff = $date->diffInYears($now);
           $ageLimit = Config::get("config.age_limit");
           if($now->gt($date) && ($diff >= $ageLimit)) return true;
           else return false;
        });
    }
    public function register()
    {
        $this->app->bind('oauth','App\Http\OauthFacade');
        $this->app->bind("imageUploader",'App\Http\ImageUploadFacade');
        $this->app->bind('settings', 'App\Http\SettingsFacade');
        $this->app->bind("remoteCall",'App\Http\RemoteCallFacade');
    }
}
