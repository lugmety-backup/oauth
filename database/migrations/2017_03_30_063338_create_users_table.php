<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->string('first_name',50);
            $table->string('middle_name',50)->nullable();
            $table->string('last_name',50);
            $table->string('email')->unique();
            $table->string('password');
            $table->enum('gender',['male','female']);
            $table->string('mobile');
            $table->string("avatar");
            $table->date('birthday');
            $table->string('facebook_id')->nullable();
            $table->string('google_id')->nullable();
            $table->smallInteger("status");
            $table->string("ip_address");
            $table->tinyInteger('is_normal')->default(1);
            $table->unsignedInteger("country_id")->nullable();
            $table->integer('customer_priority')->defaule(30);
            $table->rememberToken();
            $table->timestamps();
            $table->engine = "InnoDB";
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
