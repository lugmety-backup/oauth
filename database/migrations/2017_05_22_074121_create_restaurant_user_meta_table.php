<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRestaurantUserMetaTable extends Migration
{
    /**
     * Run the migrations.
     * this table is created to handle restaurant tasks like
     * when restaurant user or branch user is created information will be stored in this table
     * @return void
     */
    public function up()
    {
        Schema::create('restaurant_user_meta', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('user_id');
            $table->foreign('user_id')
                ->references('id')
                ->on('users')
                ->onDelete('cascade');
            $table->boolean('is_restaurant_admin');
            $table->unsignedInteger('restaurant_id');
            $table->unsignedInteger('branch_id')->nullable();
            $table->engine = "InnoDB";
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('restaurant_user_meta');
    }
}
