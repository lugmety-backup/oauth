<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateNonLoggedUserTokenTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('non_logged_tokens', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();
            $table->string("token");
            $table->string("device_type",10);
            $table->string("user_type",20);
            $table->integer("country_id");
            $table->engine = "InnoDB";

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('non_logged_tokens');
    }
}
