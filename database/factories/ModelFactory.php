<?php

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/
use Carbon\Carbon;
$factory->define(App\User::class, function (Faker\Generator $faker) {
    return [
        'first_name' =>$faker->firstName ,
        'email' => $faker->email,
        'last_name'=>$faker->lastName,
        'gender'=>'male',
        'mobile'=>'12345667',
        'birthday'=>\Carbon\Carbon::now()->toDateString(),
        'password'=>$faker->name,
        'remember_token'=>"token",
    ];
});
$factory->define(App\Permission::class, function (Faker\Generator $faker) {
    return [
        'name' => $faker->name,
        'display_name' => $faker->name,
        'description'=>$faker->name,

    ];
});

$factory->define(App\Role::class, function (Faker\Generator $faker) {
    return [
        'name' => $faker->firstName,
        'display_name' => $faker->name,
        'description'=>$faker->name,

    ];
});



