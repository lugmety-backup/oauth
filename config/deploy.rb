# config valid only for current version of Capistrano
lock "3.10.1"

set :application, "lugmety_oauth"
#set :repo_url, "git@example.com:me/my_repo.git"
set :repo_url, 'git@gitlab.com:lugmety/oauth.git'

# Default branch is :master
# ask :branch, `git rev-parse --abbrev-ref HEAD`.chomp