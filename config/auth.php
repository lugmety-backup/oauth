<?php
/**
 * Created by PhpStorm.
 * User: ashish
 * Date: 3/30/2017
 * Time: 11:23 AM
 */
return [
    'defaults' => [
        'guard' => 'api',
        'passwords' => 'users',
    ],

    'guards' => [
        'api' => [
            'driver' => 'passport',
            'provider' => 'users',
        ],
    ],

    'providers' => [
        'users' => [
            'driver' => 'eloquent',
            'model' => App\User::class
        ]
    ]
];