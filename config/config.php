<?php
/**
 * Created by PhpStorm.
 * User: ashish
 * Date: 6/29/2017
 * Time: 3:49 PM
 */

if(env("APP_ENV") == "production"){
    return array(
        'image_service_base_url' => 'https://api.lugmety.com/cdn/v1',//dont add / after v1

        "image_service_base_url_cdn" => 'https://cdn.lugmety.com',//cdn base url for image display

        "oauth_base_url" => "https://api.lugmety.com/auth/v1",

        "order_base_url" => "https://api.lugmety.com/order/v1",

        "restaurant_base_url" => "https://api.lugmety.com/restaurant/v1",

//        "password_reset_link" => "https://lugmety.com",

        "location_base_url" => "https://api.lugmety.com/location/v1",
        "age_limit" => 18,
        "settings_url" => "https://api.lugmety.com/general/v1/public/settings/",
//    "oauth_base_url" => "http://localhost:8888"


//    'image_service_base_url'=>'http://localhost:5555',
    );
}
else
{
    return array(
        'image_service_base_url'=>'http://api.stagingapp.io/cdn/v1',//dont add / after v1

        "image_service_base_url_cdn" => 'http://api.stagingapp.io/cdn/v1',//cdn base url for image display

        "oauth_base_url" => "http://api.stagingapp.io/auth/v1",

        "restaurant_base_url" => "http://api.stagingapp.io/restaurant/v1",

        "order_base_url" => "http://api.stagingapp.io/order/v1",
//        "order_base_url" => "http://localhost:2222",

//        "password_reset_link" => "http://localhost:8080/en/ksa/forgot-password/recover",
        "age_limit" => 18,
        "settings_url" => "http://api.stagingapp.io/general/v1/public/settings/",
//    "oauth_base_url" => "http://localhost:8888"
       "location_base_url" => "http://api.stagingapp.io/location/v1",
        //"location_base_url" => "http://localhost:5555",


//    'image_service_base_url'=>'http://localhost:5555',
    );
}

