server "5.39.69.20",
   user: "deployer",
   roles: %w{web app},
   ssh_options: {
     user: "deployer", # overrides user setting above
     keys: %w(C:\ssh\lugmety_deployment_key),
     forward_agent: true,
     auth_methods: %w(publickey password),
   }

set :deploy_to, "/home/docker/oauth/app"

# Default value for :format is :airbrussh.
# set :format, :airbrussh

# You can configure the Airbrussh format using :format_options.
# These are the defaults.
# set :format_options, command_output: true, log_file: "log/capistrano.log", color: :auto, truncate: :auto

# Default value for :pty is false
# set :pty, true

# Default value for :linked_files is []
# append :linked_files, "config/database.yml", "config/secrets.yml"

# Default value for linked_dirs is []
# append :linked_dirs, "log", "tmp/pids", "tmp/cache", "tmp/sockets", "public/system"

# Default value for default_env is {}
# set :default_env, { path: "/opt/ruby/bin:$PATH" }

# Default value for keep_releases is 5
# set :keep_releases, 5


set  :tmp_dir, '/home/deployer/tmp'

# Share files/directories between releases
set :linked_files, [".env"]
# set :linked_dirs, ["storage/logs"]


set :composer_install_flags, '--no-dev --no-interaction --quiet --optimize-autoloader'
set :composer_roles, :all
set :composer_working_dir, '/home/docker/oauth/app/deploy/'
set :composer_dump_autoload_flags, '--optimize'
set :composer_download_url, "https://getcomposer.org/installer"
set :composer_version, '1.0.0-alpha8' #(default: not set)

Rake::Task['deploy:updated'].prerequisites.delete('composer:install')



#namespace :deploy do

task :sync_files do
    puts "==================sync files for docker======================"
    on roles(:all) do
        execute :rm, "-rf /home/docker/oauth/app/deploy/ !vendor"
        execute :rsync, "-avzh /home/docker/oauth/app/current/ /home/docker/oauth/app/deploy/"
        execute :chmod, "-R 777 /home/docker/oauth/app/deploy/storage/logs"
        execute :chmod, "-R 777 /home/docker/oauth/app/deploy/public/"
        puts "==================stopping docker======================"
        execute :docker, "stop lugmety-oauth"
    end
end

after "deploy:published", "sync_files"

task :update_composer do
  invoke "composer:run", :update, "--dev --prefer-dist"
end

after "deploy:published", "update_composer"

task :start_docker do
    on roles(:all) do
      	sleep 10
      	puts "==================starting docker======================"
        execute :docker, "start lugmety-oauth"
    end
end

after "deploy:cleanup", "start_docker"

#end