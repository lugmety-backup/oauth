server '103.58.145.110',
user: 'deployer',
roles: %w{web app},
port:2222,
ssh_options: {
 forward_agent: true,
 auth_methods: %w(password),
 password: "Alice123$"
}

# Default deploy_to directory is /var/www/my_app_name
set :deploy_to, "/mnt/vol2/docker/oauth"

set  :tmp_dir, '/home/deployer/tmp'

# Share files/directories between releases
set :linked_files, [".env"]
# set :linked_dirs, ["storage/logs"]


set :composer_install_flags, '--no-dev --no-interaction --quiet --optimize-autoloader'
set :composer_roles, :all
set :composer_working_dir, '/mnt/vol2/docker/oauth/deploy/'
set :composer_dump_autoload_flags, '--optimize'
set :composer_download_url, "https://getcomposer.org/installer"
set :composer_version, '1.0.0-alpha8' #(default: not set)

Rake::Task['deploy:updated'].prerequisites.delete('composer:install')



#namespace :deploy do

task :sync_files do
    puts "==================sync files for docker======================"
    on roles(:all) do
        execute :rm, "-rf /mnt/vol2/docker/oauth/deploy/ !vendor"
        execute :rsync, "-avzh /mnt/vol2/docker/oauth/current/ /mnt/vol2/docker/oauth/deploy/"
        execute :chmod, "777 /mnt/vol2/docker/oauth/deploy/storage/stackdriver.json"
        execute :chmod, "-R 777 /mnt/vol2/docker/oauth/deploy/storage/logs"
        execute :chmod, "-R 777 /mnt/vol2/docker/oauth/deploy/public/"
        puts "==================stopping docker======================"
        execute :docker, "stop lugmety-oauth"
    end
end

after "deploy:published", "sync_files"

task :update_composer do
  invoke "composer:run", :update, "--dev --prefer-dist"
end

after "deploy:published", "update_composer"
#after "deploy:published", "copy_vendor"


task :copy_vendor do
    puts "==================copy vendor======================"
    on roles(:all) do
        execute :rm, "-rf /mnt/vol2/docker/oauth/deploy/vendor"
        execute :cp, "~/vendor.zip /mnt/vol2/docker/oauth/deploy/"
        execute :unzip, "/mnt/vol2/docker/oauth/deploy/vendor.zip -d /mnt/vol2/docker/oauth/deploy/"
    end
end

task :start_docker do
    on roles(:all) do
      	sleep 10
      	puts "==================starting docker======================"
        execute :docker, "start lugmety-oauth"
    end
end

after "deploy:cleanup", "start_docker"

#end
