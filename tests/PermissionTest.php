<?php
/*
/**
* Created by PhpStorm.
* User: ashish
* Date: 4/7/2017
* Time: 11:10 AM
*/
Use App\Permission;
use Laravel\Lumen\Testing\DatabaseTransactions;
class PermissionTest extends TestCase
{
    use DatabaseTransactions;
    public function __construct(){
        $access_token=$this->getAccessToken();
        $this->access_token1=$access_token[0];//having permission and roles access
        $this->access_token2=$access_token[1];//not having access

    }

    public function testGetCorrectSpecificPermission(){
        $permission=factory(Permission::class)->create();


        $res=$this->get("permission/$permission->id",[
            'Authorization'=>'Bearer '.$this->access_token1
        ]);
        $this->seeJson([
            'status'=>'200',
            'permission'=>[
                'id'=>$permission->id,
                'name'=>$permission->name,
                'display_name'=>$permission->display_name,
                'description'=>$permission->description,
                'created_at'=>(string)$permission->created_at,
                'updated_at'=>(string)$permission->updated_at

            ]
        ])->assertResponseStatus(200);
    }
    public function testSpecificWrongForUserWithNoAccessToViewPermission(){//testing for user with no access
        $permission=factory(Permission::class)->create();
        $this->get("permission/$permission->id",[
            'Authorization'=>'Bearer '.$this->access_token2
        ]);
        $this->assertResponseStatus(403);
    }
    public function testCreatePermission(){//testing for creating permission for valid input
        $this->post('permission',['name'=>'ashishhh'],[
            'Authorization'=>'Bearer '.$this->access_token1
        ])->assertResponseStatus(200);
    }
    public function testCreatePermissionForInvalidUser(){//testing for creating permission for unauthorized user
        $this->post('permission',['name'=>'ashish'],[
            'Authorization'=>'Bearer '.$this->access_token2
        ])->assertResponseStatus(403);
    }
    public function testWrongCreatePermission(){//validation for form data
        $this->post('permission',['name'=>''],[
            'Authorization'=>'Bearer '.$this->access_token1
        ])->assertResponseStatus(422);

    }
    public function testForDuplicateEntryWhileCreatingPermission(){
        $permission=Permission::create(['name'=>'ashishh']);
        $name=$permission->name;

        $this->post('permission',["name"=>$name],[
            'Authorization'=>'Bearer '.$this->access_token1
        ])->assertResponseStatus(409);

    }
    public function testForDuplicateEntryWhileUpdatingPermission(){
        $permission=factory(Permission::class)->create();
        $data=Permission::create(['name'=>'xyz']);
        $this->put("permission/$permission->id",["name"=>$data->name],[
            'Authorization'=>'Bearer '.$this->access_token1
        ])->assertResponseStatus(409);

    }

    public function testDeleteSpecificPermission(){//for user having access
        $permission=factory(Permission::class)->create();
        $res= $this->delete("permission/$permission->id",[],//delete parameter 1 url,2->form data,3 =>headers
            [
                'Authorization'=>'Bearer '.$this->access_token1
            ]
        );
        $this->assertResponseStatus(200);
    }

    public function testDeleteSpecificPermissionUserNotAccess(){//for user having access
        $permission=factory(Permission::class)->create();
        $res= $this->delete("permission/$permission->id",[],//delete parameter 1 url,2->form data,3 =>headers
            [
                'Authorization'=>'Bearer '.$this->access_token2
            ]
        );
        $this->assertResponseStatus(403);
    }


}
