<?php

/**
 * Created by PhpStorm.
 * User: ashish
 * Date: 4/9/2017
 * Time: 10:56 AM
 */
use App\User;
use App\Role;
use Laravel\Lumen\Testing\DatabaseTransactions;

class UserRoleTest extends TestCase
{
    use DatabaseTransactions;
    public function __construct(){
        $access_token=$this->getAccessToken();
        $this->access_token1=$access_token[0];//having user access
        $this->access_token2=$access_token[1];//not having access

    }

    public function testGetCorrectRolesOfAUser(){

        $user=factory(User::class)->create();
        $role=factory(Role::class)->create();
        $test=DB::table('role_user')->insert(
            [
                'user_id' => $user->id,
                'role_id' => $role->id,
            ]
        );
        $res=$this->get("/user/$user->id/role",[
            'Authorization'=>'Bearer '.$this->access_token1
        ]);

        $this->seeJson([
        'status'=>'200',
        'role'=>[[//userrole index returns array of role
                'id'=>$role->id,
                'name'=>$role->name,
                'display_name'=>$role->display_name,
                'description'=>$role->description,
                'created_at'=>(string)$role->created_at,
                'updated_at'=>(string)$role->updated_at,
                'pivot'=>['role_id'=>$role->id,'user_id'=>$user->id]]

            ]
        ])->assertResponseStatus(200);
    }
    public function testGetRolesOfAUserWithInvalidToken(){

        $user=factory(User::class)->create();
        $role=factory(Role::class)->create();
        $test=DB::table('role_user')->insert(
            [
                'user_id' => $user->id,
                'role_id' => $role->id,
            ]
        );
        $res=$this->get("/user/$user->id/role",[
            'Authorization'=>'Bearer '.$this->access_token2
        ]);
        $this->assertResponseStatus(403);
    }
    public function testCorrectAttachUserToRole(){

        $user=factory(User::class)->create();
        $role=factory(Role::class)->create();

        $res=$this->post("/user/$user->id/role",['role'=>$role->name],[
            'Authorization'=>'Bearer '.$this->access_token1
        ])->assertResponseStatus(200);

    }
    public function testCorrectAttachUserToRolewithInvalidToken(){

        $user=factory(User::class)->create();
        $role=factory(Role::class)->create();

        $res=$this->post("/user/$user->id/role",['role'=>$role->name],[
            'Authorization'=>'Bearer '.$this->access_token2
        ])->assertResponseStatus(403);

    }
    public function testAttachUserToRoleWithNoUser(){
        $user=factory(User::class)->create();
        $role=factory(Role::class)->create();
        User::whereId($user->id)->delete();
        $res=$this->post("/user/$user->id/role",['role'=>$role->name],[
            'Authorization'=>'Bearer '.$this->access_token1
        ])->assertResponseStatus(404);

    }
    public function testAttachUserToRoleWithNoRole(){
        $user=factory(User::class)->create();
        $role=factory(Role::class)->create();
        Role::whereId($role->id)->delete();
        $res=$this->post("/user/$user->id/role",['role'=>$role->name],[
            'Authorization'=>'Bearer '.$this->access_token1
        ])->assertResponseStatus(404);
    }
    public function testCorrectDettachUserToRole(){

        $user=factory(User::class)->create();
        $role=factory(Role::class)->create();
        $test=DB::table('role_user')->insert(
            [
                'user_id' => $user->id,
                'role_id' => $role->id,
            ]
        );
        $res=$this->delete("/user/$user->id/role",['role'=>$role->name],[
            'Authorization'=>'Bearer '.$this->access_token1
        ])->assertResponseStatus(200);
    }
    public function testCorrectDettachUserToRoleWithInvalidToken(){

        $user=factory(User::class)->create();
        $role=factory(Role::class)->create();
        $test=DB::table('role_user')->insert(
            [
                'user_id' => $user->id,
                'role_id' => $role->id,
            ]
        );
        $res=$this->delete("/user/$user->id/role",['role'=>$role->name],[
            'Authorization'=>'Bearer '.$this->access_token2
        ])->assertResponseStatus(403);

    }
    public function testDettachUserToRoleWithNoUser(){
        $user=factory(User::class)->create();
        $role=factory(Role::class)->create();
        $test=DB::table('role_user')->insert(
            [
                'user_id' => $user->id,
                'role_id' => $role->id,
            ]
        );
        User::whereId($user->id)->delete();
        $res=$this->delete("/user/$user->id/role",['role'=>$role->name],[
            'Authorization'=>'Bearer '.$this->access_token1
        ])->assertResponseStatus(404);

    }
    public function testDettachUserToRoleWithNoRole(){
        $user=factory(User::class)->create();
        $role=factory(Role::class)->create();
        $test=DB::table('role_user')->insert(
            [
                'user_id' => $user->id,
                'role_id' => $role->id,
            ]
        );
        Role::whereId($role->id)->delete();
        $res=$this->delete("/user/$user->id/role",['role'=>$role->name],[
            'Authorization'=>'Bearer '.$this->access_token1
        ])->assertResponseStatus(404);
    }


}