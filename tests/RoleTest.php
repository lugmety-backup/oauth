<?php

/**
 * Created by PhpStorm.
 * User: ashish
 * Date: 4/9/2017
 * Time: 10:56 AM
 */
use App\Role;
use Laravel\Lumen\Testing\DatabaseTransactions;
class RoleTest extends TestCase
{
    use DatabaseTransactions;
    public function __construct(){
        $access_token=$this->getAccessToken();
        $this->access_token1=$access_token[0];//having role and roles access
        $this->access_token2=$access_token[1];//not having access

    }

    public function testGetCorrectSpecificRole(){
        $role=factory(Role::class)->create();

        $res=$this->get("/role/$role->id",[
            'Authorization'=>'Bearer '.$this->access_token1
        ]);

        $this->seeJson([
            'status'=>'200',
            'role'=>[
                'id'=>$role->id,
                'name'=>$role->name,
                'display_name'=>$role->display_name,
                'description'=>$role->description,
                'created_at'=>(string)$role->created_at,
                'updated_at'=>(string)$role->updated_at

            ]
        ])->assertResponseStatus(200);
    }
    public function testSpecificWrongForUserWithNoAccessToViewrole(){//testing for user with no access
        $role=factory(Role::class)->create();
        $this->get("/role/$role->id",[
            'Authorization'=>'Bearer '.$this->access_token2
        ]);
        $this->assertResponseStatus(403);
    }
    public function testCreaterole(){//testing for creating role for valid input
        $this->post('/role',['name'=>'helper'],[
            'Authorization'=>'Bearer '.$this->access_token1
        ])->assertResponseStatus(200);
    }
    public function testCreateroleForInvalidUser(){//testing for creating role for unauthorized user
        $this->post('/role',['name'=>'helper'],[
            'Authorization'=>'Bearer '.$this->access_token2
        ])->assertResponseStatus(403);
    }
    public function testWrongCreaterole(){//validation for form data
        $this->post('/role',['name'=>''],[
            'Authorization'=>'Bearer '.$this->access_token1
        ])->assertResponseStatus(422);

    }
    public function testForDuplicateEntryWhileCreatingrole(){
        $role=Role::create(['name'=>'helper']);
        $name=$role->name;

        $this->post('/role',["name"=>$name],[
            'Authorization'=>'Bearer '.$this->access_token1
        ])->assertResponseStatus(409);

    }
    public function testForDuplicateEntryWhileUpdatingrole(){
        $role=factory(Role::class)->create();
        $data=Role::create(['name'=>'xyz']);
        $this->put("/role/$role->id",["name"=>$data->name],[
            'Authorization'=>'Bearer '.$this->access_token1
        ])->assertResponseStatus(409);

    }

    public function testDeleteSpecificrole(){//for user having access
        $role=factory(Role::class)->create();
        $res= $this->delete("/role/$role->id",[],//delete parameter 1 url,2->form data,3 =>headers
            [
                'Authorization'=>'Bearer '.$this->access_token1
            ]
        );
        $this->assertResponseStatus(200);
    }

    public function testDeleteSpecificroleUserNotAccess(){//for user having access
        $role=factory(Role::class)->create();
        $res= $this->delete("/role/$role->id",[],//delete parameter 1 url,2->form data,3 =>headers
            [
                'Authorization'=>'Bearer '.$this->access_token2
            ]
        );
        $this->assertResponseStatus(403);
    }

}