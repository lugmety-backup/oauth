<?php

/**
 * Created by PhpStorm.
 * User: ashish
 * Date: 4/9/2017
 * Time: 10:56 AM
 */
use App\User;
use Laravel\Lumen\Testing\DatabaseTransactions;
class UserTest extends TestCase
{
    use DatabaseTransactions;
    public function __construct(){
        $access_token=$this->getAccessToken();
        $this->access_token1=$access_token[0];//having user access
        $this->access_token2=$access_token[1];//not having access

    }

    public function testGetCorrectSpecificUser(){
        $user=factory(User::class)->create();

        $res=$this->get("/user/$user->id",[
            'Authorization'=>'Bearer '.$this->access_token1
        ]);

        $this->seeJson([
            'status'=>'200',
            'user'=>[
                'id'=>$user->id,
                'first_name'=>$user->first_name,
                'last_name'=>$user->last_name,
                'email'=>$user->email,
                'gender'=>$user->gender,
                'middle_name'=>$user->middle_name,
                'mobile'=>$user->mobile,
                'birthday'=>$user->birthday,
                'google_id'=>$user->google_id,
                'facebook_id'=>$user->facebook_id,
                'remember_token'=>$user->remember_token,
                'created_at'=>(string)$user->created_at,
                'updated_at'=>(string)$user->updated_at

            ]
        ])->assertResponseStatus(200);
    }
    public function testSpecificWrongForUserWithNoAccessToViewUser(){//testing for user with no access
        $user=factory(User::class)->create();
        $this->get("/user/$user->id",[
            'Authorization'=>'Bearer '.$this->access_token2
        ]);
        $this->assertResponseStatus(403);
    }
    public function testCreateUser(){//testing for creating user for valid input
        $this->post('/user',['first_name'=>'ashishhh','last_name'=>'kafle','password'=>'123456','email'=>'asis@gmail.com','gender'=>'male','mobile'=>'9876543210'],[
            'Authorization'=>'Bearer '.$this->access_token1
        ])->assertResponseStatus(200);
    }
    public function testCreateUserForInvalidToken(){//testing for creating user for unauthorized user
        $this->post('/role',['first_name'=>'ashishhh','last_name'=>'kafle','password'=>'123456','email'=>'asis@gmail.com','gender'=>'male','mobile'=>'9876543210'],[
            'Authorization'=>'Bearer '.$this->access_token2
        ])->assertResponseStatus(403);
    }
    public function testWrongCreateUser(){//validation for form data
        $this->post('/user',['name'=>''],[
            'Authorization'=>'Bearer '.$this->access_token1
        ])->assertResponseStatus(422);
    }
    public function testForDuplicateEntryWhileCreatingUser(){
        $user=User::create(['first_name'=>'ashishhh','last_name'=>'kafle','password'=>'123456','email'=>'asis@gmail.com','gender'=>'male','mobile'=>'9876543210']);
        $email=$user->email;

        $abc=$this->post('/user',['email'=>$email,'first_name'=>'Ashish','last_name'=>'kafle','password'=>'123456','gender'=>'male','mobile'=>'9876543210'],[
            'Authorization'=>'Bearer '.$this->access_token1
        ])->assertResponseStatus(422);


    }
    public function testForDuplicateEntryWhileUpdatingUser(){
        $user=factory(User::class)->create();
        $data=User::create(['first_name'=>'ashishhh','last_name'=>'kafle','password'=>'123456','email'=>'asis@gmail.com','gender'=>'male','mobile'=>'9876543210']);
        $this->put("/role/$user->id",["name"=>$data->email],[
            'Authorization'=>'Bearer '.$this->access_token1
        ])->assertResponseStatus(422);

    }

    public function testDeleteSpecificUser(){//for user having access
        $user=factory(User::class)->create();
        $res= $this->delete("/user/$user->id",[],//delete parameter 1 url,2->form data,3 =>headers
            [
                'Authorization'=>'Bearer '.$this->access_token1
            ]
        );
        $this->assertResponseStatus(200);
    }

    public function testDeleteSpecificroleUserNotAccess(){//for user having access
        $user=factory(User::class)->create();
        $res= $this->delete("/user/$user->id",[],//delete parameter 1 url,2->form data,3 =>headers
            [
                'Authorization'=>'Bearer '.$this->access_token2
            ]
        );
        $this->assertResponseStatus(403);
    }

}