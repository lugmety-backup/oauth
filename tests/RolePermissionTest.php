<?php

/**
 * Created by PhpStorm.
 * User: ashish
 * Date: 4/9/2017
 * Time: 10:56 AM
 */
use App\Role;
use App\Permission;
use Laravel\Lumen\Testing\DatabaseTransactions;

class RolePermissionTest extends TestCase
{
    use DatabaseTransactions;
    public function __construct(){
        $access_token=$this->getAccessToken();
        $this->access_token1=$access_token[0];//having user access
        $this->access_token2=$access_token[1];//not having access

    }

    public function testGetCorrectPermissionsOfRole(){

        $role=factory(Role::class)->create();
        $permission=factory(Permission::class)->create();
        $test=DB::table('permission_role')->insert(
            [
                'permission_id' => $permission->id,
                'role_id' => $role->id,
            ]
        );
        $res=$this->get("/role/$role->id/permission",[
            'Authorization'=>'Bearer '.$this->access_token1
        ]);

        $this->seeJson([
        'status'=>'200',
            'permission'=>[[
                'id'=>$permission->id,
                'name'=>$permission->name,
                'display_name'=>$permission->display_name,
                'description'=>$permission->description,
                'created_at'=>(string)$permission->created_at,
                'updated_at'=>(string)$permission->updated_at,
                'pivot'=>['role_id'=>$role->id,'permission_id'=>$permission->id]
            ]]
        ])->assertResponseStatus(200);
    }
    public function testGetCorrectPermissionsOfRoleWithInvalidToken(){


        $role=factory(Role::class)->create();
        $permission=factory(Permission::class)->create();
        $test=DB::table('permission_role')->insert(
            [
                'permission_id' => $permission->id,
                'role_id' => $role->id,
            ]
        );
        $res=$this->get("/role/$role->id/permission",[
            'Authorization'=>'Bearer '.$this->access_token2
        ]);
        $this->assertResponseStatus(403);
    }
    public function testCorrectAttachRoleToPermission(){

        $role=factory(Role::class)->create();
        $permission=factory(Permission::class)->create();
        $permissionArray=array($permission->name);
        $res=$this->post("/role/$role->id/permission",['permission'=>$permissionArray],[
            'Authorization'=>'Bearer '.$this->access_token1
        ])->assertResponseStatus(200);

    }
    public function testCorrectAttachRoleToPermissionwithInvalidToken(){

        $role=factory(Role::class)->create();
        $permission=factory(Permission::class)->create();
        $permissionArray=array($permission->name);
        $res=$this->post("/role/$role->id/permission",['permission'=>$permissionArray],[
            'Authorization'=>'Bearer '.$this->access_token2
        ])->assertResponseStatus(403);
    }
    public function testCorrectAttachRoleToPermissionWithNoRole(){

        $role=factory(Role::class)->create();
        Role::whereId($role->id)->delete();
        $permission=factory(Permission::class)->create();
        $permissionArray=array($permission->name);
        $res=$this->post("/role/$role->id/permission",['permission'=>$permissionArray],[
            'Authorization'=>'Bearer '.$this->access_token1
        ])->assertResponseStatus(404);

    }
    public function testCorrectAttachRoleToPermissionWithNoPermission(){

        $role=factory(Role::class)->create();
        $permission=factory(Permission::class)->create();
        Permission::whereId($permission->id)->delete();
        $permissionArray=array($permission->name);
        $res=$this->post("/role/$role->id/permission",['permission'=>$permissionArray],[
            'Authorization'=>'Bearer '.$this->access_token1
        ])->assertResponseStatus(404);

    }
    public function testCorrectDettachRoleToPermission(){

        $role=factory(Role::class)->create();
        $permission=factory(Permission::class)->create();
        $test=DB::table('permission_role')->insert(
            [
                'permission_id' => $permission->id,
                'role_id' => $role->id,
            ]
        );
        $permissionArray=array($permission->name);
        $res=$this->delete("/role/$role->id/permission",['permission'=>$permissionArray],[
            'Authorization'=>'Bearer '.$this->access_token1
        ])->assertResponseStatus(200);

    }
    public function testCorrectDettachRoleToPermissionwithInvalidToken(){

        $role=factory(Role::class)->create();
        $permission=factory(Permission::class)->create();
        $test=DB::table('permission_role')->insert(
            [
                'permission_id' => $permission->id,
                'role_id' => $role->id,
            ]
        );
        $permissionArray=array($permission->name);
        $res=$this->delete("/role/$role->id/permission",['permission'=>$permissionArray],[
            'Authorization'=>'Bearer '.$this->access_token2
        ])->assertResponseStatus(403);
    }
    public function testCorrectDettachRoleToPermissionWithNoRole(){

        $role=factory(Role::class)->create();
        $permission=factory(Permission::class)->create();
        $test=DB::table('permission_role')->insert(
            [
                'permission_id' => $permission->id,
                'role_id' => $role->id,
            ]
        );
        Role::whereId($role->id)->delete();
        $permissionArray=array($permission->name);
        $res=$this->delete("/role/$role->id/permission",['permission'=>$permissionArray],[
            'Authorization'=>'Bearer '.$this->access_token1
        ])->assertResponseStatus(404);

    }
    public function testCorrectDettachRoleToPermissionWithNoPermission(){

        $role=factory(Role::class)->create();
        $permission=factory(Permission::class)->create();
        $test=DB::table('permission_role')->insert(
            [
                'permission_id' => $permission->id,
                'role_id' => $role->id,
            ]
        );
        Permission::whereId($permission->id)->delete();
        $permissionArray=array($permission->name);
        $res=$this->delete("/role/$role->id/permission",['permission'=>$permissionArray],[
            'Authorization'=>'Bearer '.$this->access_token1
        ])->assertResponseStatus(404);

    }
    public function testCorrectDettachRoleToPermissionWithNoAttach(){

        $role=factory(Role::class)->create();
        $permission=factory(Permission::class)->create();
        $permissionArray=array($permission->name);
        $res=$this->delete("/role/$role->id/permission",['permission'=>$permissionArray],[
            'Authorization'=>'Bearer '.$this->access_token1
        ])->assertResponseStatus(404);

    }

}