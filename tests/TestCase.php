<?php
Use Illuminate\Support\Facades\DB;
Use Laravel\Lumen\Testing\DatabaseTransactions;
abstract class TestCase extends Laravel\Lumen\Testing\TestCase
{
    use DatabaseTransactions;
    /**
     * Creates the application.
     *
     * @return \Laravel\Lumen\Application
     */
    public function createApplication()
    {
        return require __DIR__.'/../bootstrap/app.php';
    }
    public function getAccessToken(){
        return ['eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiIsImp0aSI6IjA0YjM4YmRmZDRlNWJlMDJjZGE3MjU4YjFkYjFmOGExOTViYjkzNjMwODQzYjZhNTg5ZTU2YWViYTc2NDgxNDNkNDRmZTUzZmFiZjI0Njg5In0.eyJhdWQiOiI1IiwianRpIjoiMDRiMzhiZGZkNGU1YmUwMmNkYTcyNThiMWRiMWY4YTE5NWJiOTM2MzA4NDNiNmE1ODllNTZhZWJhNzY0ODE0M2Q0NGZlNTNmYWJmMjQ2ODkiLCJpYXQiOjE0OTE1NDA2NzEsIm5iZiI6MTQ5MTU0MDY3MSwiZXhwIjoxNTIzMDc2NjcxLCJzdWIiOiIxIiwic2NvcGVzIjpbXX0.bL9_FaUiC7qYRuWkG4Bz4foWaWhFTxZs4cKZYRBNIQEdyg8XN6qG_XHuGe38J8FmD4MWPBlQ2MF7kqt7c9tg1bghH7l8uaaFCYdb5tidmeG_texh_GpekmEirkyLwMGkfbv_hXPb5RyXO6F4wNO0Z9xlUgC451vy6gE0W2E8swQb_-Oih6O9rC7S6ahnA0P3NR-npwL6_8YrGD0A_qvlGfWcg1zCveW7nzNp4WQAdI898NWDModP7yRJgCasHLLnGtI8KlKuiyZC_Owitjjj36F3SVid61v0i5PN_Ux3GTv9NqHpK8Ktxw6Yg5Q-w7dBwQK0IAMuejaN_uMM3eZrLTTPBRDBTAmSH-b-5INu7qDvOqv-0F0mjlXvgKvUPLwDRP1uNe8UT1xUx_5JWLJJzn5Ca-yQUT76s7arPdY6jFfezg8pi8_t-6csBYi_FcXGQnsW5pb12aNK46I5Kzuh1TL1VXtG38WICm27fa-Hj12rHJr66N8kUIb1_txtZOSUghABf1rvE0z9CYZw5JbKQkC0zQtL_YjJlRUOXFV8urzkrPLcrtmLQffnoJe0ol4inPM_863xoWyptZJU6QbTvAqplCfTjd_boxEcw-7K-cCqJEHVb9tUyajgk5E-fQMZax6Mm3p18TNucP8RVBiAl1WL9qUJvocKw9GeLVqG0jM',
            "eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiIsImp0aSI6ImQxM2MzODNkMDVhMWQ5ODU0ZjQ5NmRiMDEwNDhkODEzNWJhZjRmNGY0ZjM0Mjc5NDE3NjllMDg4MjQ3ZWMxODMwMGNlNzZkNTdlY2ZmNzc1In0.eyJhdWQiOiI1IiwianRpIjoiZDEzYzM4M2QwNWExZDk4NTRmNDk2ZGIwMTA0OGQ4MTM1YmFmNGY0ZjRmMzQyNzk0MTc2OWUwODgyNDdlYzE4MzAwY2U3NmQ1N2VjZmY3NzUiLCJpYXQiOjE0OTE1NDYzNDUsIm5iZiI6MTQ5MTU0NjM0NSwiZXhwIjoxNTIzMDgyMzQ1LCJzdWIiOiIyIiwic2NvcGVzIjpbXX0.cDdXkMwxjd1NVJ8wKQJ7MhLVhWcwHH-_ZvxH_Zg-JVGFmWVvqyu9erimAbchSlrQ7A0v8xtbUYd2BrD91XSP8snkwo3t9NfJ-aoP2X1o5JFddYMERTi24WSS6n10zqsaEqbCcifhI1cwBMNoW_R-XtG-S05-q_smhmk_IP1-Mevb6PjnArzKXbYwfORAmICc18JHzRxxXTyUL9OxERB0MOBc0eK3W50CcnBs2ngCUrms2Z4ho_s23iSyYji7XAjibFzxSOGeXFBuiSgKkrVlupTvNPW89H8BauNC680NR6FABmnRQ7JKxD_TefxC4sYvRWxEOnOksPZIu09LPjDe5jBAuNEUTwHWTMXM3Myd_78UcjQwute5lu-uk--Y8HA06qW4qV7ITy45KKMt1HQQhPlUM5ngs3xFThHm_OwUfOQ4UNxxExDV8Xyu2wCbC2nDEqb9H7SVzCq5Ugq3x9RchDIdj8whXgbwNqO43WgVWpQbpJyB2IQIchV7lvPwYKQfZSkjWz6BTB6KIwtzNvOBq9srwGJyg3VgSisXuO_VTXWTjZ31pMlOGdlTNhiLPCE6o6xGRuak3X5QiM_tld3pijMquktSe_eSCW0CrlEAIukNmwxfNQgM8PmcyYfPf-CCxbakgm0_QCS5EM0OPd1CWFp0z1k5tKk8vrYC53O0Eew"];
    }
    public function getToken(){
        $data=DB::table('oauth_clients')->insertGetId([
            'id'=>'6',
            'secret'=>'Bko3s6jLymmxp3YZrc2N1s3mLkS3BiEXWRUk6adu',
            'name'=>'tests',
            'redirect'=>'http://localhost',
            'password_client'=>1

        ]);
        $data=DB::table('oauth_clients')->find($data);
        $res= $this->call('post',"oauth/token",[
            'grant_type'=>'password',
            'client_id'=>$data->id,
            'client_secret'=>$data->secret,
            'username'=>'ascc.2023@gmail.com',
            'password'=>'123456'
        ]);
        return $res->getContent();
    }
}
